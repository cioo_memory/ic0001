#####################################################
# Import the library
#####################################################
import os, sys, re
import argparse





#####################################################
# Setup
#####################################################
script_path = os.path.dirname(os.path.abspath(__file__))
sys.path.append(script_path)


#----------------------------------------------------
# import class
#----------------------------------------------------
import common
from sv_common_flow import sv_common_flow





#####################################################
# Function
#####################################################







#####################################################
# Class
#####################################################


class vcs_flow(sv_common_flow):

    parser  = None
    logger = None

    def __init__(self):
        sv_common_flow.__init__(self)
        self.comp_log = 'vcs_comp.log'
        self.sim_log = 'vcs_sim.log'
        self.simulator = 'vcs'
        self.compile_mode = '-full64'
        self.logger = common.get_logger()


    def gen_compile_cmd(self, args):
        command = self.simulator
        command = common.str_combine(command, '-full64')
        command = common.str_combine(command, '-timescale=1ns/100ps')
        command = common.str_combine(command, '-sverilog -ntb_opts uvm')
        #command = common.str_combine(command, '-debug_access+pp -debug_access+all -debug -Mupdate -LDFLAGS -Wl, -E -lca')
        command = common.str_combine(command, '-debug_access+pp -debug_access+all -debug -Mupdate -lca')
        command = common.str_combine(command, '-top testbench')

        if(args.gui):
            command = common.str_combine(command, 'gui')
        
        if(args.user_comp_args):
            command = common.str_combine(command, args.user_comp_args)

        if(args.dump_en):
            command = common.str_combine(command, '-kdb -cm_tgl mda -cm line+cond+tgl+branch+fsm')
            command = common.str_combine(command, '-lca')
            command = common.str_combine(command, '+vcs+lic+wait')

        if(args.coverage):
            command = common.str_combine(command, '-cm_tgl mda -cm line+cond+tgl+branch+fsm')

        


        print('gen vcs final compile cmd : ', command)
        return command

    def gen_sim_cmd(self, args):
        command = os.path.join(args.work_dir, './simv')

        if(args.tcname):
            command = common.str_combine(command, '+UVM_TESTNAME=')
            command = common.str_combine(command, args.tcname, '')

        if(args.dump_en):
            command = common.str_combine(command, '-ucli -i $PROJ_ROOT/temp/ucli.tcl')

        if(args.coverage):
            command = common.str_combine(command, '-cm line+cond+tgl+branch+fsm')

        if(args.verbosity):
            command = common.str_combine(command, '+UVM_VERBOSITY=')
            command = common.str_combine(command, args.verbosity, '')

        command = common.str_combine(command, '-l vcs_sim.log')

        print('gen vcs sim cmd : ', command)

        return command

    def gen_cmd(self, args):
        cmd = {}

        cmd['comp'] = self.gen_compile_cmd(args)

        cmd['simulator'] = self.simulator

        if(args.gui):
            cmd['gui_mode'] = True


        cmd['sim'] = self.gen_sim_cmd(args)

        self.logger.info('gen command done : ', cmd)

        print('gen cmd done *********************')
        print(cmd)

        return cmd
