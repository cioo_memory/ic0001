#####################################################
# Import the library
#####################################################
import os, sys, re
import argparse





#####################################################
# Setup
#####################################################
script_path = os.path.dirname(os.path.abspath(__file__))
sys.path.append(script_path)


#----------------------------------------------------
# import class
#----------------------------------------------------
import common





#####################################################
# Function
#####################################################







#####################################################
# Class
#####################################################


global_params = {}
global_params['seed'] = 0

def get_glb_param(name = None):
    if(not name or type(name) != str):
        return global_params
    elif(type(name) is str and name in global_params):
        return global_params[name]
    else:
        return None

def set_glb_param(name:str, value):
    if(name and type(name) is str):
        global_params[name] = value




class args:

    parser  = None
    logger = None

    def __init__(self):
        self.parser = argparse.ArgumentParser(description='run simulation script')
        self.logger = common.get_logger()


    def setup(self):
        self.parser.add_argument("-tb",         metavar='TB_NAME',          dest="tbname",          default=None, type=str, help='The testbench name : tb_top and tb_uvc_xx can be used')
        self.parser.add_argument("-tc",         metavar='TC_NAME',          dest="tcname",          default=None, type=str, help='The testcase name')
        self.parser.add_argument("-simulator",  metavar='SIMULATOR',        dest="simulator",       default='vcs',type=str, help='Specify the simulator VCS|IRUN')
        self.parser.add_argument("-comp_args",  metavar='COMPILE_ARGS',     dest="user_comp_args",  default=None, type=str, help='User compile options input')
        self.parser.add_argument("-sim_args",   metavar='SIMULATION_ARGS',  dest="user_sim_args",   default=None, type=str, help='User simulate options input')
        self.parser.add_argument("-gui",        metavar='SIM_GUI',          dest="gui",             default=None, type=str, help='Set the GUI')
        self.parser.add_argument("-dp",                                     dest="dump_en",         action='store_true',    help='enable the fsdb dump')
        self.parser.add_argument("-cov",                                    dest="coverage",        action='store_true',    help='enable the coverage collect')
        self.parser.add_argument("-block",                                  dest="block_level_sim", action='store_true',    help='enable the coverage collect')
        self.parser.add_argument("-verbosity",  metavar='SIM_VERBOSITY',    dest="verbosity",       default=None, type=str, help='Set the UVM verbosity')

    def get(self):
        args = self.parser.parse_args()
        return args
