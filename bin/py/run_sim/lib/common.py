#####################################################
# Import the library
#####################################################
import os, sys, re
import argparse
import logging
#import yaml
#import calendar




#####################################################
# Setup the logger
#####################################################
proj_root = os.getenv('PROJ_ROOT')
log_path = os.path.join(proj_root, 'bin/py/run_sim/log/runsim.log')
log = open(log_path,'w')
log.close()
logger = logging.getLogger()


#file handle and stream handle
log_file_handler = logging.FileHandler(log_path)
log_stream_handler = logging.StreamHandler()

#file handler
#print('log path = ', log_path)
log_file_handler.setLevel(logging.INFO)
log_stream_handler.setLevel(logging.INFO)

# formatter
log_file_show_fmt = "PY_%(levelname)s %(filename)s(%(lineno)d) @%(asctime)-15s : %(message)s"
log_stream_show_fmt = "PY_%(levelname)s %(filename)s(%(lineno)d) @ %(asctime)-s : %(message)s"
#log_date_fmt = "%a %d %b %Y %H:%M:%S"
log_file_date_fmt = "%a-%d-%b-%Y %H:%M:%S"
log_stream_date_fmt = "%H:%M:%S"

log_file_formatter = logging.Formatter(log_file_show_fmt, log_file_date_fmt)
log_stream_formatter = logging.Formatter(log_stream_show_fmt, log_stream_date_fmt)

#
log_file_handler.setFormatter(log_file_formatter)
log_stream_handler.setFormatter(log_stream_formatter)
#logger.addHandler(log_file_handler)
logger.addHandler(log_stream_handler)



#####################################################
# Function
#####################################################
def get_logger():
    return logger

def set_logger(level = 'INFO'):
    if(level == 'DEBUG'):
        logger.setLevel(logging.DEBUG)
    elif(level == 'INFO'):
        logger.setLevel(logging.INFO)




def file_exists(file_path):
    if(file_path):
        _file = Path(os.path.expandvars(file_path))
        if _file.is_file():
            return True
        else:
            return False
    else:
        return False


def grep(keywords, file_path, after_lines = 1):
    grep_cmd = 'egrep -A ' + str(after_lines)
    grep_cmd = grep_cmd + ' "' + keywords + '" ' + file_path
    #print('grep cmd',grep_cmd)
    #logger.info('do the grep : ', grep_cmd)
    grep_fd = os.popen(grep_cmd)
    #print(grep_fd)
    grep_result = grep_fd.readlines()
    #print('grep result',grep_result)
    return grep_result

def str_combine(str1, str2, delimite = ' '):
    result = None
    if(type(str1) != str):
        logger.fatal('ERROR, str combine dest must be string')
    else:
        result = str1 + delimite + str(str2)

    return result

def run_command(cmd):
    exec_cmd = ''
    i = 0
    if(type(cmd) == str):
        cmd = os.path.expandvars(cmd)
        exec_cmd = exec_cmd + 'echo \'>' +cmd + '\'\n'
        print(cmd)
        exit(1)
    elif(type(cmd) == list):
        for cmd_item in cmd:
            cmd_item = os.path.expandvars(cmd_item)
            i = i+1
            if i != len(cmd):
                exec_cmd = exec_cmd + cmd_item + ' && '
            else:
                exec_cmd = exec_cmd + cmd_item
            #exec_cmd = exec_cmd + 'echo \'>' + cmd_item + '\'\n'
            #exec_cmd = exec_cmd + cmd_item + '\n'
            #exec_cmd = exec_cmd + 'echo "" \n'
            #exec_cmd = exec_cmd + 'echo "" \n'

            print('cmd : ', cmd_item)
        
        print('exec cmd = ', exec_cmd)

    # multi cmd
    else:
        logger.fatal('ERROR, not support cmd type' + str(type(cmd)))
    os.system(exec_cmd)
