#####################################################
# Import the library
#####################################################
import os, sys, re
import argparse





#####################################################
# Setup
#####################################################
script_path = os.path.dirname(os.path.abspath(__file__))
sys.path.append(script_path)


#----------------------------------------------------
# import class
#----------------------------------------------------
import common
import vcs_flow
import args_config
from args_config import args



#####################################################
# Function
#####################################################







#####################################################
# Class
#####################################################


class main_flow:

    parser  = None
    #logging = None
    logger= common.get_logger()
    args_cfg    = None
    work_dir = None

    def __init__(self):
        # setup and get args
        self.setup_args()
        self.args = self.get_args();

    def setup_args(self):
        if(not self.args_cfg):
            self.args_cfg = args()
            self.logger.debug('get args from args_config.args')
        self.args_cfg.setup()

    def get_args(self):
        args = self.args_cfg.get()
        return args

    def run(self):
        simulator = self.args.simulator.lower()
        #self.logger.info('simulator is set to ', simulator)
        self.logger.info('simulator is set to vcs')
        flist_opt = ''
        if(simulator == 'vcs'):
            sv_flow = vcs_flow.vcs_flow()


        # gen the command dict
        if(sv_flow):
            self.args.work_dir = ''
            command = sv_flow.gen_cmd(self.args)


        if(command['comp']):
            if(args_config.get_glb_param('flist')):
                flist_opt = common.str_combine('-f', args_config.get_glb_param('flist'))
                command['comp'] = common.str_combine(command['comp'], flist_opt)
                print('set the file list to',flist_opt)
            else:
                self.logger.error('can not find the file list')
                exit(1)

            command['comp'] = common.str_combine(command['comp'], '-l vcs_compile.log')

        if(self.args.tcname):
            command['tests'] = self.args.tcname

        self.work_dir = args_config.get_glb_param('tempdir')

        compile_cmd = []
        if(self.work_dir):
            compile_cmd.append('cd ' + self.work_dir)

        compile_cmd.append(command['comp'])



        print(compile_cmd)
        # run the command
        common.run_command(compile_cmd)

        sim_cmd = []
        if(self.work_dir):
            sim_cmd.append('cd ' + self.work_dir)
        sim_cmd.append(command['sim'])

        print('sim cmd', sim_cmd)
        common.run_command(sim_cmd)

        
        print("\033[32m")
        print("\n-------------------------------------------------------------------------------------------------------------------")
        #self.logger.info('Simulation done')
        print('Simulation done')
        print("\033[0m")
        fsdb_file = os.path.join(self.work_dir, 'testbench.fsdb')
        comp_log_file  = os.path.join(self.work_dir, 'vcs_compile.log')
        sim_log_file  = os.path.join(self.work_dir, 'vcs_sim.log')

        if(os.path.exists(comp_log_file)):
            has_error = 1
            has_error = sv_flow.check_comp_log(comp_log_file)
            print("\033[32m")
            print("Compile log file :")
            print("    {comp_log_file}".format(comp_log_file=comp_log_file))
            print("\033[0m")
            if(has_error == 1):
                self.print_result(has_error)
                exit(1)
        else:
            print("\033[31m")
            self.logger.fatal('the compile file log is not exists, please check!')
            print("\033[0m")
            exit(1)

        if(os.path.exists(sim_log_file)):
            print("\033[32m")
            print("Sim log file :")
            print("    {sim_log_file}".format(sim_log_file=sim_log_file))
            print("\033[0m")
        else:
            self.logger.warning('the simile file log is not exists, please check!')

        if(os.path.exists(fsdb_file)):
            print("\033[32m")
            print("Waveform fsdb file exists, you can tape the follow cmd to open it :")
            print("    verdi -ssf", fsdb_file, "&")
            print("\033[0m")
        else:
            self.logger.info('the fsdb dump is disable, there is no one a valid fsdb file.')
            
        print("\033[32m")
        print("\n-------------------------------------------------------------------------------------------------------------------")
        print("\033[0m")

        if(os.path.exists(sim_log_file)):
            has_error = 1
            has_error = sv_flow.check_sim_log(sim_log_file)
            self.print_result(has_error)

        exit(1)

    def print_result(self, has_error):
        if(has_error == 1):
                print("\033[31m                                                             \n\
                                                                                                    \n\
*******************************************************************************************************************\n\
*                    _____   _       ___  ___      _____      ___   _   _                                         *\n\
*                   /  ___/ | |     /   |/   |    |  ___|    /   | | | | |                                        *\n\
*                   | |___  | |    / /|   /| |    | |__     / /| | | | | |                                        *\n\
*                   |___  | | |   / / |__/ | |    |  __|   / /_| | | | | |                                        *\n\
*                    ___| | | |  / /       | |    | |     / /  | | | | | |___                                     *\n\
*                   /_____/ |_| /_/        |_|    |_|    /_/   |_| |_| |_____|                                    *\n\
*                                                                                                                 *\n\
*******************************************************************************************************************\n\033[0m")
        else:
                print("\033[32m                                                             \n\
                                                                                                    \n\
*******************************************************************************************************************\n\
*                    _____   _       ___  ___      _____      ___   _____   _____                                 *\n\
*                   /  ___/ | |     /   |/   |    |  _  |    /   | /  ___/ /  ___/                                *\n\
*                   | |___  | |    / /|   /| |    | |_| |   / /| | | |___  | |___                                 *\n\
*                   |___  | | |   / / |__/ | |    |  ___/  / /_| | |___  | |___  |                                *\n\
*                    ___| | | |  / /       | |    | |     / /  | |  ___| |  ___| |                                *\n\
*                   /_____/ |_| /_/        |_|    |_|    /_/   |_| /_____/ /_____/                                *\n\
*                                                                                                                 *\n\
*******************************************************************************************************************\n\033[0m")

        #command
        


