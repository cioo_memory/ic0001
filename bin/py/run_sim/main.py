#####################################################
# Import the library
#####################################################
import os, sys, re
#import logging
import argparse





#####################################################
# Setup
#####################################################
script_path = os.path.dirname(os.path.abspath(__file__))
sys.path.append(script_path)
sys.path.append(os.path.join(script_path, 'lib'))


#----------------------------------------------------
# import class
#----------------------------------------------------
import common
from main_flow import main_flow

import args_config
from args_config import args



class run_sim_args(args):
    def setup(self):
        super().setup()
        self.parser.add_argument("-search",     metavar='SEARCH_PATH',      dest="tb_search_path",  default=None, type=str, help='The testbench search path')

class run_sim_flow(main_flow):
    tb_path = None
    proj_root = None
    logger = None
    
    def __init__(self):
        common.set_logger('INFO')
        self.logger= common.get_logger()
        self.logger.info('run main flow init') 
        super().__init__()

    def create_temp_dir(self):
        print('create the temp dir')
        tbname = self.args.tbname

        self.proj_root = os.getenv('PROJ_ROOT')
        cur_dir = os.getcwd()
        if(self.args.block_level_sim):
            self.logger.info('block level simulation, set the root to verif/block')
            verif_path = os.path.join(self.proj_root, 'design/digital/verif/block')
        else:
            self.logger.info('chip level simulation, set the root to verif/chip')
            verif_path = os.path.join(self.proj_root, 'design/digital/verif/chip')

        self.tb_path = os.path.join(verif_path, os.path.join(tbname, 'tb'))

        if(not tbname):
            self.logger.fatal('-tb option must be set')

        find_tb = False
        print('the tb path is : ', self.tb_path)

        if(os.path.isdir(self.tb_path)):
            find_tb = True
            flist_path = os.path.join(self.tb_path, os.path.join('flist', 'tb_'+tbname+'.fl'))
            if(not os.path.isfile(flist_path)):
                self.logger.fatal('ERROR, file list can not be found')
                print('Error, can not find the file list : ', file)
                exit(1)
            else:
                print('success find the file list : ', flist_path)
                args_config.set_glb_param('flist', flist_path)

        else:
            # search the tb for(uvc tb)
            print('start to search the uvc tb in ', verif_path)
            tb_search_path = verif_path
            for root, dirs, files in os.walk(tb_search_path):
                for file in files:
                    if(file == tbname + '.fl'):
                        self.logger.info('success find the file list')
                        print('success find the uvc file list : ', file, 'in', root)
                        self.tb_path = root.replace('/flist', '')
                        print('the tb path is set to : ', self.tb_path)
                        find_tb = True
                        break
        if(not find_tb):
            self.logger.fatal('ERROR, testbench can not be found')
        else:
            self.logger.info('Find the testbench in' + self.tb_path)
            print('Find the testbench in' + self.tb_path)


            
        temp_root = os.path.join(self.proj_root, 'temp')
        temp_tb_dir = os.path.join(temp_root, tbname)

        if(not os.path.isdir(temp_root)):
            self.logger.info('The temp dir is not exists, we will create it')
            os.mkdir(temp_root)

        if(not os.path.isdir(temp_tb_dir)):
            self.logger.info('The tb temp dir is not exists, we will create it')
            os.mkdir(temp_tb_dir)

        return temp_tb_dir

    #def proj_root_setup(self):
    #    self.proj_root = os.getenv('PROJ_ROOT')

    #    if(self.proj_root):
    #logger.info('Get the project root from env : ' + self.proj_root)
    #    else:
    #        cur_dir = Path(os.getcwd())
    #        parent = cur_dir.parent
    #        while(parent.as_posix() != '/'):
    #logger.info('PROJ_ROOT is', + curdir.as_posix())
    #            self.proj_root = cur_dit.as_posix()


    def setup_check(self):
        print('setup check')
        if(not os.getenv('PROJ_ROOT')):
            self.logger.fatal("ERROR, can not find the PROJ_ROOT, please check your env_set first!")
            exit(1)
        if(not self.args.tbname):
            self.logger.fatal("ERROR, can not find the tb name, please check your sim cmd first!")
            exit(1)

    def run(self):
        self.setup_check()
        temp_dir = self.create_temp_dir()
        args_config.set_glb_param('tempdir', temp_dir)
        
        return super().run()


#####################################################
# Main
#####################################################

if __name__ == '__main__':
    sim_flow = run_sim_flow()
    try:
        sim_flow.run()
    except SystemExit as e:
        if(str(e) == '1'):
            print("exit because of 1")
            exit(1)
