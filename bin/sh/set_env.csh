#!/bin/tcsh

# project location
echo "set the project env in `pwd`"

set setenv_file="setenv.csh"

set project_name="ic0001"

if(! $?PROJ_LOC) then
    if(-e bin/sh/$setenv_file) then
        setenv PROJ_LOC `pwd`
        echo "$setenv_file will set the PROJ_LOC to $PROJ_LOC"
    else
        echo "ERROR : not source the $setenv_file at project root"
        exit 1
    endif
endif




# setenv
setenv PROJ_ROOT "$PROJ_LOC"

setenv RTL_ROOT  "$PROJ_ROOT/design/digital/rtl"
setenv VRF_ROOT  "$PROJ_ROOT/design/digital/verif"
setenv DUMP_ROOT "$PROJ_ROOT/design/digital/dump"





# alias

# ---- cmd
#alias runsim 'autoenv; python3 $PROJ_ROOT/bin/py/run_sim/main.py'
alias runsim 'python3 $PROJ_ROOT/bin/py/run_sim/main.py'

# ---- chdir
alias cdprj "cd $PROJ_ROOT"
alias cdbin "cd $PROJ_ROOT/bin"
alias cdvrf "cd $VRF_ROOT"
alias cdrtl "cd $RTL_ROOT"
alias cdtb "cd $VRF_ROOT/${project_name}"
