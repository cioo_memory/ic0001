#!/usr/bin/python
#****************************************************************************************************
#---------------Copyright (c) 2019 FPGA1988 | wangboworks@126.com All rights reserved----------------
#****************************************************************************************************
#File Information
#****************************************************************************************************
#File Name      : run_env[.py]
#Product Type   : All
#Description    : The project's environment set.
#License        : MuLanPSL-1.0
#****************************************************************************************************
#Version Information
#****************************************************************************************************
#Create Date    : 2019-10-14 16:50
#First Author   : FPGA1988
#Modify Date    : 2016-10-14 17:00
#Last Author    : FPGA1988
#Version Number : 001
#****************************************************************************************************
#Revison History
#****************************************************************************************************
#2019.10.14 - FPGA1988 - The initial version.
#----------------------------------------------------------------------------------------------------


#****************************************************************************************************
#1. The package import
#****************************************************************************************************
import os
import sys
import re
from pathlib import Path
#from export_win import *
prj_path = ""


#****************************************************************************************************
#2. The path gen
#****************************************************************************************************

def auto_env_set():

    print("auto env set")
    #Get the father directory
    parent_path = os.path.abspath(os.path.join(os.path.dirname("__file__"),os.path.pardir))
    prj_name_match = re.match(r'.*(ic\d\d\d\d).*logic.*',parent_path,re.M|re.I)
    match_text = ".*ic\d\d\d\d.*logic"
    prj_path_match = re.match(r'({match_text}).*'.format(match_text=match_text),parent_path,re.M|re.I)
    prj_path = prj_path_match.group(1)
    if prj_name_match:
        print('project name = ',prj_name_match.group(1))
        prj_name = prj_name_match.group(1)
    else:
        print('No project name is match!')

    print("project path = ",prj_path)

    #level 1 : project/xx
    path_name_level1 = [
        'apr_path'  ,   #cdapr
        'dig_path'  ,   #cddig
        'env_path'  ,   #cdenv
        'fpga_path' ,   #cdfpga
        'fcp_path'  ,   #cdfcp
        'lib_path'  ,   #cdlib
        'bin_path'      #cdbin
    ]
    path_alias_level1 = {
        path_name_level1[0] : 'cdapr',  #cdapr
        path_name_level1[1] : 'cddig',  #cddig
        path_name_level1[2] : 'cdenv',  #cdenv
        path_name_level1[3] : 'cdfpga',  #cdfpga
        path_name_level1[4] : 'cdfcp',  #cdfcp
        path_name_level1[5] : 'cdlib',  #cdlib
        path_name_level1[6] : 'cdbin'   #cdbin
    }
    path_value_level1 = {
        path_name_level1[0] : prj_path + '/apr'     ,  #cdapr
        path_name_level1[1] : prj_path + '/digital' ,  #cddig
        path_name_level1[2] : os.getcwd()           ,  #cdenv
        path_name_level1[3] : prj_path + '/fpga'    ,  #cdfpga
        path_name_level1[4] : prj_path + '/fullchip',  #cdfcp
        path_name_level1[5] : prj_path + '/lib'     ,  #cdlib
        path_name_level1[6] : prj_path + '/bin'        #cdbin
    }

    #level 2 : project/digital/xx

    path_name_level2 = [
        'fml_path'  ,   #cdfml
        'lint_path' ,   #cdlint
        'rtl_path'  ,   #cdrtl
        'pwr_path'  ,   #cdpwr
        'sta_path'  ,   #cdsta
        'syn_path'  ,   #cdsyn
        'vrf_path'      #cdvrf
    ]

    path_alias_level2 = {
        path_name_level2[0]  : 'cdfml' ,   #cdfml
        path_name_level2[1]  : 'cdlint' ,   #cdlint
        path_name_level2[2]  : 'cdrtl' ,   #cdrtl
        path_name_level2[3]  : 'cdpwr' ,   #cdpwr
        path_name_level2[4]  : 'cdsta' ,   #cdsta
        path_name_level2[5]  : 'cdsyn' ,   #cdsyn
        path_name_level2[6]  : 'cdvrf'     #cdvrf
    }

    path_value_level2 = {
        #path_name_level2[0]  : path_value_level1['dig_path'] + '/bin'    ,   #cdbin
        path_name_level2[0]  : path_value_level1['dig_path'] + '/formal' ,   #cdfml
        path_name_level2[1]  : path_value_level1['dig_path'] + '/nlint'  ,   #cdlint
        path_name_level2[2]  : path_value_level1['dig_path'] + '/rtl'    ,   #cdrtl
        path_name_level2[3]  : path_value_level1['dig_path'] + '/power'  ,   #cdpwr
        path_name_level2[4]  : path_value_level1['dig_path'] + '/sta'    ,   #cdsta
        path_name_level2[5]  : path_value_level1['dig_path'] + '/syn'    ,   #cdsyn
        path_name_level2[6]  : path_value_level1['dig_path'] + '/verify/uvm'      #cdvrf
    }

    #level 3 : project/digital/verif
    # verification path setting
    path_name_level3 = [
        'cov_path'   , #cdcov
        'list_path'  , #cdlist
        'hsim_path'  , #cdhsim
        'inc_path'   , #cdinc
        'log_path'   , #cdlog
        'mdl_path'   , #cdmdl
        'run_path'   , #cdrun
        'tb_path'    , #cdtb
        'tc_path'    , #cdtc
        'wave_path'    #cdwave
    ]
    path_alias_level3 = {
        path_name_level3[0]  : 'cdcov' 	, #cdcov
        path_name_level3[1]  : 'cdlist' , #cdlist
        path_name_level3[2]  : 'cdhsim' , #cdhsim
        path_name_level3[3]  : 'cdinc' 	, #cdinc
        path_name_level3[4]  : 'cdlog' 	, #cdlog
        path_name_level3[5]  : 'cdmdl' 	, #cdmdl
        path_name_level3[6]  : 'cdrun' 	, #cdrun
        path_name_level3[7]  : 'cdtb' 	, #cdtb
        path_name_level3[8]  : 'cdtc' 	, #cdtc
        path_name_level3[9]  : 'cdwave'   #cdwave
    }
    path_value_level3 = {
        path_name_level3[0]  : path_value_level2['vrf_path'] + '/coverage' , #cdcov
        path_name_level3[1]  : path_value_level2['vrf_path'] + '/flist'    , #cdlist
        path_name_level3[2]  : path_value_level2['vrf_path'] + '/hsim'     , #cdhsim
        path_name_level3[3]  : path_value_level2['vrf_path'] + '/include'  , #cdinc
        path_name_level3[4]  : path_value_level2['vrf_path'] + '/log'      , #cdlog
        path_name_level3[5]  : path_value_level2['vrf_path'] + '/model'    , #cdmdl
        path_name_level3[6]  : path_value_level2['vrf_path'] + '/run'      , #cdrun
        path_name_level3[7]  : path_value_level2['vrf_path'] + '/tb'       , #cdtb
        path_name_level3[8]  : path_value_level2['vrf_path'] + '/tc'       , #cdtc
        path_name_level3[9]  : path_value_level2['vrf_path'] + '/wave'       #cdwave
    }


    def path_dict_create(path_name_list,path_value_list,path_alias_list):
        i = 0
        path_dict = []
        for path_name in path_name_list:
            path_new = {'path_name' : path_name,'path_value' : path_value_list[path_name],'path_alias' : path_alias_list[path_name]}
            path_dict.append(path_new)
            i = i + 1

        return path_dict

    path_dict_level1 = path_dict_create(path_name_level1,path_value_level1,path_alias_level1)
    path_dict_level2 = path_dict_create(path_name_level2,path_value_level2,path_alias_level2)
    path_dict_level3 = path_dict_create(path_name_level3,path_value_level3,path_alias_level3)

    #test
    '''
    for path in path_dict_level2:
        if path['path_name'] == 'pwr_path':
            print ("power path = " + path['path_value'])
    '''

#****************************************************************************************************
#3. The env file generate
#****************************************************************************************************

    #use the format to use a vars in a text.
    if os.name == 'nt' :
        # set a variable in the windows os
        env_obj = Win32Environment(scope="SYSTEM")
        #print set_env_path(env_obj, 'JAVA_HOME', java_home,refresh=True)
        #print set_env_path(env_obj, 'Path', '%JAVA_HOME%\\bin;%JAVA_HOME%\\jre\\bin')
        #print set_env_path(env_obj, 'CLASSPATH',
        #                   '.;%JAVA_HOME%\\lib\\dt.jar;%JAVA_HOME%\\lib\\tools.jar')

        #environ can not change the register table
        #os.environ['RTL']='digital/rtl'
        #putenv has the same problem

        for env_path in path_dict_level1:
            #os.environ(["{env_path}['path_name']".format(env_path=env_path)]) =  env_path['path_value']
            #set_env_path(env_obj,'{env_path}['path_name']'.format(env_path=env_path),env_path['path_value'])
            set_env_path(env_obj,env_path['path_name'],env_path['path_value'],refresh=True)

        for env_path in path_dict_level2:
            set_env_path(env_obj,env_path['path_name'],env_path['path_value'],refresh=True)
        for env_path in path_dict_level3:
            set_env_path(env_obj,env_path['path_name'],env_path['path_value'],refresh=True)
    else :
        print('current path = ' + os.getcwd())
        env_file = '{project_name}.env'.format(project_name=prj_name)
        #print(env_file)
        if os.path.exists(env_file):
            os.remove(env_file)
        fp = open(env_file,'w')
        fp.write("#! /bin/csh/ -f\n")
        #3.1 environment variable set
        fp.write("export PRJ_NAME=" + prj_name + "\n")
        fp.write("export " + prj_name + "=" + prj_path + "\n")
        for env_path in path_dict_level1:
            fp.write("export " + env_path['path_name'] + "=" + env_path['path_value']+ "\n")
        for env_path in path_dict_level2:
            fp.write("export " + env_path['path_name'] + "=" + env_path['path_value']+ "\n")
        for env_path in path_dict_level3:
            fp.write("export " + env_path['path_name'] + "=" + env_path['path_value']+ "\n")
        #3.2 alias set
        for env_path in path_dict_level1:
            fp.write('alias ' + env_path['path_alias'] + '=\"cd ' + env_path['path_value'] + "\"" + "\n")
        for env_path in path_dict_level2:
            fp.write('alias ' + env_path['path_alias'] + '=\"cd ' + env_path['path_value'] + "\"" + "\n")
        for env_path in path_dict_level3:
            fp.write('alias ' + env_path['path_alias'] + '=\"cd ' + env_path['path_value'] + "\"" + "\n")
        #3.3 bin path
        fp.write("export PATH=$PATH:" + prj_path + "/bin " + "\n")


        #fp.write("source /tool/sge/default/common/settings.csh\n")

        fp.close()


#os.system('source ' + env_file)

#****************************************************************************************************
#4. Add source statement in .bashrc_my : auto source
#****************************************************************************************************

        home_path = os.getenv('HOME')
        fp_new = open(home_path + '/bashrc_my.new','w')
        fp_old = open(home_path + '/bashrc_my','r')

# re.I : match the lower-case and capital

        match_flag = 0
        lines = fp_old.readlines()

        for line in lines:
            if(re.match(r'source .*(ic\d\d\d\d)\.env',line,re.M|re.I)):
                #print(line)
                line = line.replace(line,'source ' + path_value_level1['env_path'] + "/" + env_file + "\n")
                #print(line)
            fp_new.write(line)

        fp_new.close()
        fp_old.close()

        os.system('cp ' + home_path + '/bashrc_my.new' + " " + home_path + '/bashrc_my -f')
        os.system('rm ' + home_path + '/bashrc_my.new')



def dir_gen(project_path):
    dir_names = [
        "apr/dataout",
        "digital/verify/coverage/cov_merged",
        "digital/verify/coverage/cov_results",
        "digital/verify/coverage/scripts",
        "digital/verify/coverage/reports",
        "digital/verify/flist",
        "digital/verify/hsim",
        "digital/verify/include",
        "digital/verify/lib",
        "digital/verify/log",
        "digital/verify/model/gt_analog_model/analog_osc",
        "digital/verify/model/gt_analog_model/analog_pad",
        "digital/verify/model/gt_analog_model/analog_power",
        "digital/verify/model/gt_interface/if_model",
        "digital/verify/run",
        "digital/verify/tb",
        "digital/verify/tc",
        "digital/verify/wave",
        "digital/bin",
        "digital/formal/log",
        "digital/formal/reports",
        "digital/formal/run_dir",
        "digital/formal/scripts",
        "digital/nlint/result",
        "digital/nlint/run",
        "digital/rtl",
        "digital/sta/log",
        "digital/sta/reports",
        "digital/sta/results",
        "digital/sta/run_dir",
        "digital/sta/scripts",
        "digital/syn/log",
        "digital/syn/reports",
        "digital/syn/results",
        "digital/syn/run_dir",
        "digital/syn/scripts",
        "fullchip/spice_netlist_gen/lib",
        "fullchip/spice_netlist_gen/results",
        "fullchip/spice_netlist_gen/scripts",
        "lib",
        "env",
        "fpga/doc",
        "fpga/lib",
        "fpga/project/cfg/bit",
        "fpga/project/cfg/mcs",
        "fpga/project/cfg/temp",
        "fpga/project/prj",
        "fpga/project/rar",
        "fpga/project/sim",
        "fpga/project/src_asic",
        "fpga/project/src_fpga",
        "fpga/project/ucf"
    ]
    for dir_name in dir_names:
        os.makedirs(project_path+dir_name,0o777,True)

def dir_get(project_path):
    dir_list = os.listdir(project_path)
    return dir_list

def prj_new(project_path):
    dir_gen(project_path)
    dir_list = dir_get("d:/gt1234")
    return dir_list

'''
module run as main
'''

if __name__ == '__main__':
    auto_env_set()
