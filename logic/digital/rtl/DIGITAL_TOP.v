module DIGITAL_TOP(
input          osc_clk,
output         osc_en,
input          por_rst_n,
output reg     por_cfg_done,
input          pad_sclk_in,
input          pad_cs_in,
input          pad_si_in,
input          pad_so_in,
input          pad_wp_in,
input          pad_hold_in,
output         pad_si_out,
output         pad_so_out,
output         pad_wp_out,
output         pad_hold_out,
output         pad_out_en,//1wire output
output         pad_dual_en,
output         pad_quad_en,
output         flh_pump_01_en,
output         flh_pump_02_en,
output         flh_pump_03_en,
output         flh_pump_04_en,
input          flh_pump_01_rdy,
input          flh_pump_02_rdy,
input          flh_pump_03_rdy,
input          flh_pump_04_rdy,
output         flh_sab_en,//sa bias
output         flh_rd_clk,
output         flh_sa_en,//sa_en
output[18:0]   flh_addr,
output         flh_rd_en,
output[31:0]   flh_data_in,//to flash,flash master
input[31:0]    flh_data_out,//from flash,flash master
output         flh_pgm_start,
output         flh_pgm_en,
output         flh_pgm_pulse,
output         flh_pv_en,//pgm_verify
input[1:0]     flh_pgm_st,
output         flh_ers_start,
output         flh_ers_en,
output         flh_ers_pulse,
output         flh_ev1_en,
output         flh_ev2_en,
input[1:0]     flh_ers_st,
output         flh_ers_tune1_en,   //when st='b01
output         flh_ers_tune1_pulse,
output         flh_ers_tune2_en,   //when st='b10
output         flh_ers_tune2_pulse,
output         flh_blk_ers_en,
output         flh_sec_ers_en,
output         flh_ers_64k_en,//0:for 32k blk    1:for 64k  blk
output         flh_busy,//flash busy, in program/erase
output         sram_clk,
output[7:0]    sram_addr,
output         sram_wr_en,
output         sram_rd_en,
output[7:0]    sram_data_in,//to sram,sram master
input[7:0]     sram_data_out,//from sram,sram master
output         test_mode_en,
output         dpd_mode_en,//deep power mode
output         stb_mode_en,//standby mode
output         tm_flag_01,
output         tm_flag_02,
output         tm_flag_03,
output         tm_flag_04,
output         tm_flag_05,
output         tm_flag_06
);
assign sck = pad_sclk_in;
assign csn = pad_cs_in;


//TODO pad bhv
assign s0_in = pad_si_in;
assign s1_in = pad_so_in;
assign s2_in = pad_wp_in;
assign s3_in = pad_hold_in;

wire s0_out;
wire s1_out;
wire s2_out;
wire s3_out;
assign pad_si_out   = pad_out_en&(pad_dual_en|pad_quad_en) ? s0_out : 1'b0;
assign pad_so_out   = pad_out_en                           ? s1_out : 1'b0;
assign pad_wp_out   = pad_out_en&pad_quad_en               ? s2_out : 1'b0;
assign pad_hold_out = pad_out_en&pad_quad_en               ? s3_out : 1'b0;

assign holdn = pad_hold_in;
assign wpn   = pad_wp_in;


assign rst_n = por_rst_n;

wire[7:0]  flh_rdataout;
wire[7:0]  fe_rdataout;
wire[7:0]  par_rdataout;  
wire[7:0]  flh_dataout;
wire[7:0]  fe_dataout;
wire[7:0]  par_dataout;  
wire[7:0]  cmd_latch;
wire[7:0] par_tx_data;

FELOGIC FELOGIC(
     .sck    (sck    )
    ,.s0_in  (s0_in  )
    ,.s1_in  (s1_in  )
    ,.s2_in  (s2_in  )
    ,.s3_in  (s3_in  )
    ,.s0_out (s0_out )
    ,.s1_out (s1_out )
    ,.s2_out (s2_out )
    ,.s3_out (s3_out )
    ,.csn    (csn    )
    ,.rst_n  (rst_n  )

    //sapi
    ,.fe_por_rdy     (flh_por_rdy)
    //,.datain      ();//fe_datain     )
    ,.fe_rdataout    (fe_rdataout)
    //,.clkin       ();//fe_clkin      )
    //,.addrenin    ();//fe_addrenin   )
    //,.dataoutenin ();//fe_dataoutenin)
    
    ,.fe_dataout     (fe_dataout      )
    ,.fe_clkout      (fe_clkout       ) 
    ,.fe_rdatain     (flh_rdataout    )
    ,.fe_addrenout   (fe_addrenout    )
    ,.fe_dataoutenout(fe_dataoutenout )

    ,.cmde7_l     (1'b0            )
    ,.cmdbb_l     (1'b0            )
    ,.cmdeb_l     (1'b0            )
    ,.out_en      (pad_out_en      )
    ,.dual_en     (pad_dual_en     )
    ,.quad_en     (pad_quad_en     )

    ,.sram_clk      (sram_clk      )
    ,.sram_addr     (sram_addr     )
    ,.sram_wr_en    (sram_wr_en    )
    ,.sram_rd_en    (sram_rd_en    )
    ,.sram_data_in  (sram_data_in  )
    ,.sram_data_out (sram_data_out )
    ,.cmd_latch     (cmd_latch     )
    ,.par_tx_data   (par_tx_data   )
    ,.fe_busy       (fe_busy       )

);

FLH_CONTROL FLH_CONTROL(
 .rst_n        (rst_n      )
//,.clk_20M      (clk_20M_out)

//sapi stop,rdata_in start
,.datain       (fe_dataout           )
,.rdataout     (flh_rdataout         ) 
,.clkin        (fe_clkout            ) 
,.addrenin     (fe_addrenout         )
,.dataoutenin  (fe_dataoutenout      )

,.dataout      (flh_dataout                     )//to next sapi
,.clkout       (flh_clkout                      )
,.rdatain      (par_rdataout                    )//start rdata point
,.addrenout    (flh_addrenout                   )
,.dataoutenout (flh_dataoutenout                )
,.por_rdy      (flh_por_rdy                     )
);


PAR PAR(
 .iclk        (osc_clk)
,.rst_n       (rst_n  )

,.par_tx_data (par_tx_data)

,.par_datain      (flh_dataout       )  //address+data
,.par_rdataout    (par_rdataout      )//to back_sapi for read data
,.par_clkin       (flh_clkout        )
,.par_addrenin    (flh_addrenout     )
,.par_dataoutenin (flh_dataoutenout  )

,.par_dataout     (par_dataout       )//to next_sapi
,.par_clkout      (par_clkout        ) //to next_sapi
,.par_rdatain     ('h0)//flh_rdataout      )
,.par_addrenout   (par_addrenout     )//to next_sapi
,.par_dataoutenout(par_dataoutenout  )

,.par_por_rdy     (flh_por_rdy       ) //parameter ok


,.cmd_latch     (cmd_latch     )
,.fe_busy       (fe_busy       )
);
//TODO auto_load parameter and other operation
initial begin
    por_cfg_done = 1'b0;
    #10us;
    por_cfg_done = 1'b1;
end
//IOPAD(
//
//);
endmodule
