module FELOGIC(
input             sck,
input             s0_in,
input             s1_in,
input             s2_in,
input             s3_in,
input             csn,
input             rst_n,
output reg        s0_out,
output reg        s1_out,
output reg        s2_out,
output reg        s3_out,
input             cmde7_l,//cmde7_mode
input             cmdbb_l,
input             cmdeb_l,
output reg        out_en,//output spi 1wire
output reg        dual_en,
output reg        quad_en,
//sram control
output            sram_clk,
output reg[7:0]   sram_addr,
output            sram_wr_en,
output            sram_rd_en,
output[7:0]       sram_data_in,
input[7:0]        sram_data_out,
//sapi
//to next sapi
input             fe_por_rdy,
//input[7:0]        fe_datain,  //start point
output[7:0]       fe_rdataout,//end point
//input             fe_clkin,   //start point
//input             fe_addrenin,//start point
//input             fe_dataoutenin,//start point

output logic[7:0] fe_dataout,//to next_sapi
output            fe_clkout, //to next_sapi
input[7:0]        fe_rdatain,
output logic      fe_addrenout,//to next_sapi
output            fe_dataoutenout,
output[23:0]      addr24,
output reg[7:0]   cmd_latch,
input[7:0]        par_tx_data,
output            fe_busy

);
wire[7:0] fe_datain;
reg[7:0] din;
assign holdn = s3_in;
assign wpn   = s2_in;
enum{
IDLE,
ADDR,
RX_DATA,
TX_DATA,
DMY,
WAIT,
M7,
AMC,       //testmode
DATAIN,    //test_mode
DATAOUT,   //test_mode
NUM_STATES
}FE_STATES;
reg[NUM_STATES-1:0] state,next_state;
reg[5:0]            lat_en,next_lat_en;
reg[1:0]            cnt_byte,next_cnt_byte;
reg[3:0]            cnt_bit,next_cnt_bit;
assign fe_busy = !(state[IDLE]);
wire cmd01 = cmd_latch=='h01;
wire cmd02 = cmd_latch=='h02;
wire cmd03 = cmd_latch=='h03;
wire cmd04 = cmd_latch=='h04;
wire cmd05 = cmd_latch=='h05;
wire cmd06 = cmd_latch=='h06;
wire cmd0b = cmd_latch=='h0b;
wire cmd20 = cmd_latch=='h20;
wire cmd32 = cmd_latch=='h32;
wire cmd35 = cmd_latch=='h35;
wire cmd3b = cmd_latch=='h3b;
wire cmd42 = cmd_latch=='h42;
wire cmd44 = cmd_latch=='h44;
wire cmd48 = cmd_latch=='h48;
wire cmd50 = cmd_latch=='h50;
wire cmd52 = cmd_latch=='h52;
wire cmd5a = cmd_latch=='h5a;
wire cmd60 = cmd_latch=='h60;
wire cmd66 = cmd_latch=='h66;
wire cmd6b = cmd_latch=='h6b;
wire cmd70 = cmd_latch=='h70;
wire cmd75 = cmd_latch=='h75;
wire cmd77 = cmd_latch=='h77;
wire cmd7a = cmd_latch=='h7a;
wire cmd80 = cmd_latch=='h80;
wire cmd90 = cmd_latch=='h90;
wire cmd92 = cmd_latch=='h92;
wire cmd94 = cmd_latch=='h94;
wire cmd99 = cmd_latch=='h99;
wire cmd9f = cmd_latch=='h9f;
wire cmdab = cmd_latch=='hab;
wire cmdb9 = cmd_latch=='hb9;
wire cmdbb = cmd_latch=='hbb;
wire cmdc7 = cmd_latch=='hc7;
wire cmdd8 = cmd_latch=='hd8;
wire cmde7 = cmd_latch=='he7;
wire cmdeb = cmd_latch=='heb;

//testmode command
wire cmdff = cmd_latch==`WRP; //write parameter
wire cmdfe = cmd_latch==`RDP; //read parameter
wire cmdfd = cmd_latch==`WPB; //write pagebuff(sram)
wire cmdfc = cmd_latch==`RPB; //read pagebuff(sram)
reg   mode4,mode2;
reg   tx_en,next_tx_en;
assign rst = !csn&rst_n;
reg g_clk;
always@(*)begin
    if(!rst)begin
        g_clk = 1'b0;
    end
    else if(!sck)begin
        g_clk = !csn;
    end
end
assign clk = sck&g_clk;
always@(negedge clk or negedge rst)begin
    if(!rst)begin
        state        <= 'h1;
        lat_en       <= 'h1;
        cnt_bit      <= 'h7;
        cnt_byte     <= 'h0;
        tx_en        <= 1'b0;
    end
    else begin
        state        <= next_state;
        lat_en       <= next_lat_en;
        cnt_bit      <= next_cnt_bit;
        cnt_byte     <= next_cnt_byte;
        tx_en        <= next_tx_en;
    end
end
wire amc_dmy;
//cmdab have 2mode
assign cmd_addr    = cmd03|cmd0b|cmd3b|cmd6b|cmdeb|cmde7|cmd02|cmd32|cmd20|cmd52|cmdd8
                     |cmd90|cmd92|cmd94|cmd5a|cmd44|cmd42|cmd48|cmdab|cmdbb|cmdfd|cmdfc;

assign cmd_tx      = cmd05|cmd35|cmd9f;
assign cmd_wait    = cmd06|cmd04|cmd50|cmdc7|cmd60|cmd66|cmd99|cmd75|cmd7a|cmd70|cmd80
                     |cmdb9;
assign cmd_rx      = cmd01|cmd77;

assign addr_tx     = cmd03|cmd90|cmdab|cmdfc;
assign addr_rx     = cmd02|cmd32|cmd42|cmdfd;
assign addr_dmy    = cmd0b|cmd3b|cmd5a|cmd48|cmd6b;


assign dmy_tx      = cmd0b|cmdeb|cmde7|cmd94|cmd5a|cmd48|cmd3b|cmd6b|cmdfe;

assign hold_cmd    = cmdbb_l|cmdeb_l|cmde7_l;


assign addr_m7     = cmd92|cmd94|cmde7|cmdeb|cmdbb;
assign m7_tx       = cmd92|cmde7|cmdbb;
assign m7_dmy      = cmd94|cmdeb;
assign cmd_amc     = cmdff|cmdfe;
assign amc_dmy     = cmdfe;
assign amc_datain  = cmdff;
wire[7:0] cnt_bit_mode;
assign cnt_bit_mode = mode4 ? 'h1 :
                      mode2 ? 'h3 :
                      'h7;
wire [7:0] p2_datadef;

//byte clock
assign byte_clk   = cnt_bit==0&!csn&sck;
assign byte_clk_1 = cnt_bit==1&!csn&sck;//pre_clk

assign test_cmd = cmdff|cmdfe|cmdfd|cmdfc;
assign fe_clkin       = test_cmd       ? byte_clk :
                        cmd01|cmd04    ? byte_clk_1|byte_clk :
                        'h0;
wire[7:0] st_data = cnt_byte==1&cnt_bit==1 ? `P3_ADDR :
                    cnt_byte==1&cnt_bit==0 ? din :
                    cnt_byte==0&cnt_bit==1 ? `P4_ADDR :
                    cnt_byte==0&cnt_bit==0 ? din :
                    'h0;              

assign fe_datain      = test_cmd ? din     : 
                        cmd01    ? st_data :
                        'h0;
assign fe_addrenin    = cmd01|state[AMC];
assign fe_dataoutenin = tx_en;

SAPI #(
 .ADDRP( `P2_ADDR)
,.DATAD( `P2_DATA)
)
P2_MODE(
 .rst_n        (rst_n             )
,.por_ok       (fe_por_rdy        )
,.datain       (fe_datain         )
,.rdataout     (fe_rdataout       )
,.clkin        (fe_clkin          )
,.addrenin     (fe_addrenin       )
,.dataoutenin  (fe_dataoutenin    )

,.dataout      (fe_dataout     )
,.clkout       (fe_clkout      )
,.rdatain      (fe_rdatain     )
,.addrenout    (fe_addrenout   )
,.dataoutenout (fe_dataoutenout)

,.datadef      (p2_datadef     )
);
assign mode2_p2 = p2_datadef[0];
assign mode4_p2 = p2_datadef[1];
//vvvvvvvvvv latch
wire g_din_latch0;
wire g_din_latch1;
wire g_din_latch2;
wire g_din_latch3;
wire g_din_latch4;
wire g_din_latch5;
wire g_din_latch6;
wire g_din_latch7;
assign g_byte_en    = g_din_latch0;

//mode2_in/mode4_in enable ==>mode2_out/mode4_out must enable
always@(*)begin
    if(!rst)begin
        mode2 = 1'b0;
    end
    else if(g_byte_en)begin
        if(mode2_p2)begin
            mode2 =1'b1;
        end
        else if(state[IDLE])begin
            mode2 = cmdbb|cmd92;
        end
        else if(state[DMY])begin
            mode2 = cmd3b&cnt_byte==0;
        end
    end
end
always@(*)begin
    if(!rst)begin
        mode4 = 1'b0;
    end
    else if(g_byte_en)begin
        if(mode4_p2)begin
            mode4 =1'b1;
        end
        else if(state[IDLE])begin
            mode4 = cmdeb|cmde7|cmd77|cmd94;
        end
        else if(state[DMY])begin
            mode4 = cmd6b&cnt_byte==0;
        end
    end
end

//negedge output data
reg mode2_flag;
reg mode4_flag;
always@(negedge byte_clk or negedge rst)begin
    if(!rst)begin
        mode2_flag <= 1'b0;
        mode4_flag <= 1'b0;
    end
    else begin
        mode2_flag <= mode2;
        mode4_flag <= mode4;
    end   
end
assign out_en  = state[TX_DATA];
assign dual_en = mode2_flag;
assign quad_en = mode4_flag;

always@(*)begin
    next_state      = '0;
    next_lat_en     = lat_en;
    next_cnt_bit    = |cnt_bit ? cnt_bit-1 : cnt_bit_mode;
    next_cnt_byte   = (|cnt_byte)&(cnt_bit==0) ? cnt_byte - 1 : cnt_byte;
    next_tx_en      = tx_en;
    case(1)
        state[IDLE]:begin//CMD
            if(cmd_tx&cnt_bit==1)begin
                next_tx_en = 1'b1;
            end

            if(|cnt_bit)begin
                next_state[IDLE] = 1;
            end
            else if(cmd_addr)begin
                next_state[ADDR] = 1;
                next_cnt_byte    = 2;
            end
            else if(cmd_tx)begin
                next_state[TX_DATA] = 1;
            end
            else if(cmd_rx)begin
                next_state[RX_DATA] = 1;
                if(cmd01)begin
                    next_cnt_byte   = 1;
                end
            end
            else if(hold_cmd)begin//data as addr
                next_state[ADDR] = 1;
                next_cnt_byte    = 1;
            end
            else if(cmd_amc)begin
                next_state[AMC]  = 1;
            end
            else begin//other command cmd_wait
                next_state[WAIT] = 1'b1;
            end
        end
        state[ADDR]:begin//full if
            if(addr_tx&cnt_bit==1&cnt_byte==0)begin
                next_tx_en = 1'b1;
            end
            if((|cnt_byte)|(|cnt_bit))begin
                next_state[ADDR] = 1;
            end
            else if(addr_tx)begin
                next_state[TX_DATA] = 1;//always TX
            end
            else if(addr_rx)begin
                next_state[RX_DATA] = 1;//always RX
            end
            else if(addr_dmy)begin
                next_state[DMY]     = 1;
            end
            else if(addr_m7)begin
                next_state[M7]      = 1;
            end
            else begin//other command or not recognise
                next_state[WAIT]    = 1;
            end
        end
        state[RX_DATA]:begin
            if((|cnt_byte)|(|cnt_bit))begin
                next_state[RX_DATA] = 1;
            end
            else begin
                next_state[RX_DATA] = 1;
            end
        end
        state[TX_DATA]:begin
            next_state[TX_DATA] = 1;
            if(cnt_bit==0)begin
            end
        end
        state[DMY]:begin
            if(dmy_tx&cnt_byte==0&cnt_bit==1)begin
                next_tx_en     = 1;
            end
            if((|cnt_bit)|(|cnt_byte))begin
                next_state[DMY]     = 1;
            end
            else if(dmy_tx)begin
                next_state[TX_DATA] = 1;
            end
            else begin//cmd not recognise
                next_state[WAIT]    = 1;
            end
        end
        state[M7]:begin
            if(m7_tx&cnt_byte==0&cnt_bit==1)begin
                next_tx_en = 1;
            end
            if((|cnt_bit)|(|cnt_byte))begin
                next_state[M7]      = 1;
            end
            else if(m7_dmy)begin
                next_state[DMY]     = 1;
            end
            else if(m7_tx)begin
                next_state[TX_DATA] = 1;
            end
        end
        state[AMC]:begin
            if((|cnt_bit)|(|cnt_byte))begin
                next_state[AMC]     = 1;
            end
            else if(amc_dmy)begin
                next_state[DMY]  = 1;
            end
            else if(amc_datain)begin
                next_state[DATAIN] = 1;
            end
            //no other 
        end
        state[DATAIN]:begin
            next_state[DATAIN] = 1;
        end
        state[WAIT]:begin
            next_state[WAIT] = 1;
        end
        default:next_state='x;
    endcase
end
//7
//
reg[7:0] din_latch;
assign g_en = !csn&!sck;
//7 3 1
//6 3 1
//5 2 1
//4 2 1
//3 1 0
//2 1 0
//1 0 0
//0 0 0
assign g_latch7 = mode4_flag ? cnt_bit==1 :
                  mode2_flag ? cnt_bit==3 :
                  cnt_bit==7;
assign g_latch6 = mode4 ? cnt_bit==1 :
                  mode2_flag ? cnt_bit==3 :
                  cnt_bit==6;
assign g_latch5 = mode4_flag ? cnt_bit==1 :
                  mode2_flag ? cnt_bit==2 :
                  cnt_bit==5;
assign g_latch4 = mode4_flag ? cnt_bit==1 :
                  mode2_flag ? cnt_bit==2 :
                  cnt_bit==4;
assign g_latch3 = mode4_flag ? cnt_bit==0 :
                  mode2_flag ? cnt_bit==1 :
                  cnt_bit==3;
assign g_latch2 = mode4_flag ? cnt_bit==0 :
                  mode2_flag ? cnt_bit==1 :
                  cnt_bit==2;
assign g_latch1 = mode4_flag ? cnt_bit==0 :
                  mode2_flag ? cnt_bit==0 :
                  cnt_bit==1;
assign g_latch0 = cnt_bit==0;

assign g_din_latch7 = g_latch7&g_en;
assign g_din_latch6 = g_latch6&g_en;
assign g_din_latch5 = g_latch5&g_en;
assign g_din_latch4 = g_latch4&g_en;
assign g_din_latch3 = g_latch3&g_en;
assign g_din_latch2 = g_latch2&g_en;
assign g_din_latch1 = g_latch1&g_en;
assign g_din_latch0 = g_latch0&g_en;

always@(*)begin
    if(!rst)begin
        din_latch[7] = 0;
    end
    else if(g_din_latch7)begin
        din_latch[7] = mode4_flag ? s3_in :
                       mode2_flag ? s1_in :
                       s0_in;
    end
end
always@(*)begin
    if(!rst)begin
        din_latch[6] = 0;
    end
    else if(g_din_latch6)begin
        din_latch[6] = mode4_flag ? s2_in :
                       s0_in;
    end
end
always@(*)begin
    if(!rst)begin
        din_latch[5] = 0;
    end
    else if(g_din_latch5)begin
        din_latch[5] = mode4_flag|mode2_flag ? s1_in :
                       s0_in;
    end
end
always@(*)begin
    if(!rst)begin
        din_latch[4] = 0;
    end
    else if(g_din_latch4)begin
        din_latch[4] = s0_in;
    end
end
always@(*)begin
    if(!rst)begin
        din_latch[3] = 0;
    end
    else if(g_din_latch3)begin
        din_latch[3] = mode4_flag ? s3_in :
                       mode2_flag ? s1_in :
                       s0_in;
    end
end
always@(*)begin
    if(!rst)begin
        din_latch[2] = 0;
    end
    else if(g_din_latch2)begin
        din_latch[2] = mode4_flag ? s2_in :
                       s0_in;
    end
end
always@(*)begin
    if(!rst)begin
        din_latch[1] = 0;
    end
    else if(g_din_latch1)begin
        din_latch[1] = mode2_flag|mode4_flag ? s1_in :
                       s0_in;
    end
end
always@(*)begin
    if(!rst)begin
        din_latch[0] = 0;
    end
    else if(g_din_latch0)begin
        din_latch[0] = s0_in;
    end
end

//update to din,receive 1byte data
always@(*)begin
    if(!rst_n)begin
        din = 'h0;
    end
    else if(g_byte_en)begin
        din = din_latch;
    end
end

//latch command
assign g_cmd_latch = !hold_cmd&state[IDLE]&g_byte_en;
always@(*)begin
    if(!rst)begin
        cmd_latch = 0;
    end
    else if(g_cmd_latch)begin
        cmd_latch = din;
    end
end

//latch_addr
//byte3
reg[23:0] addr_latch;
assign addr24 = addr_latch;
assign g_addr_byte2 = (state[IDLE]&hold_cmd | state[ADDR]&cnt_byte==2)&g_byte_en;
always@(*)begin
    if(!rst)begin//!csn
        addr_latch[23:16] = 'h0;
    end
    else if(g_addr_byte2)begin
        addr_latch[23:16] = din;
    end
end

assign g_addr_byte1 = state[ADDR]&cnt_byte==1&g_byte_en;
always@(*)begin
    if(!rst)begin
        addr_latch[15:8] = 'h0;
    end
    else if(g_addr_byte1)begin
        addr_latch[15:8] = din;
    end
end

assign g_addr_byte0 = state[ADDR]&cnt_byte==0&g_byte_en;
always@(*)begin
    if(!rst)begin//!csn
        addr_latch[7:0] = 'h0;
    end
    else if(g_addr_byte0)begin
        addr_latch[7:0] = din;
    end
end

logic wel_latch;
//TODO FLH_CONTROL set/reset wel_latch
//set:cmd06|cmd02|cmd20|cmd32|cmd44|cmd60|cmdc7|cmd42
//reset:cmd06
//sapi

//TODO FLH_CONTROL
//cmd66 enable reset,cmd99 go to reset

//TODO FLH_CONTROL
//volatile_status register enable
logic vwel_latch;
//set: cmd50
//reset:any other command

logic[15:0] st_latch;
//TODO S15/S10/S1/S0
always@(*)begin
    if(wel_latch&g_byte_en&state[RX_DATA]&cmd01&cnt_byte==1)begin
        st_latch[7:2] = din[7:2];
        st_latch[1:0] = 'h0;//TODO
    end
end
always@(*)begin
    if(wel_latch&g_byte_en&state[RX_DATA]&cmd01&cnt_byte==0)begin
        st_latch[15]    = 'h0;//TODO
        st_latch[14:11] = din[7:4];
        st_latch[10]    = 'h0;//TODO
        st_latch[9:8]   = din[1:0];
    end
end

//TODO RYBY outptu by so cmd70/cmd80
//TODO cmd75/cmd7a sus/exit_sus
//
//



reg[7:0] tx_data;
wire[7:0] sram_out;
always@(negedge byte_clk or rst)begin
    if(!rst)begin
        tx_data <= 'h0;
    end
    else if(tx_en)begin
        tx_data <= cmdfe ? fe_rdataout :
                   cmdfc ? sram_out    :
                   'h0;//other
    end
end
reg[2:0] cnt_bit_out;
always@(*)begin
    case(cnt_bit)
        3'b000: cnt_bit_out = 'h0;//mode4 ? 1 :
                                  //mode2 ? 3 :
                                  //'h7;
        3'b001: cnt_bit_out = 'h1;//'h0;
        3'b010: cnt_bit_out = 'h2;//'h1;
        3'b011: cnt_bit_out = 'h3;//'h2;
        3'b100: cnt_bit_out = 'h4;//'h3;
        3'b101: cnt_bit_out = 'h5;//'h4;
        3'b110: cnt_bit_out = 'h6;//'h5;
        3'b111: cnt_bit_out = 'h7;//'h6;
        default:cnt_bit_out = 'h0;//'h0;
    endcase
end

wire[7:0] spi_odata;
assign spi_odata =par_tx_data|tx_data;
always@(*)begin
    if(!out_en)begin
        s0_out = 1'b0;
    end
    else if(!sck)begin
        s0_out = mode4_flag ? spi_odata >>(cnt_bit_out*4) :
                 mode2_flag ? spi_odata >>(cnt_bit*2 )    :
                 1'b0;//
    end
end
always@(*)begin
    if(!out_en)begin
        s1_out = 1'b0;
    end
    else if(!sck)begin
        s1_out = mode4_flag ? spi_odata >>((cnt_bit_out*4)+1) :
                 mode2_flag ? spi_odata >>((cnt_bit*2) + 1)   :
                 tx_data >> cnt_bit_out;
    end
end
always@(*)begin
    if(!out_en)begin
        s2_out = 1'b0;
    end
    else if(!sck)begin
        s2_out = mode4_flag ? spi_odata >>((cnt_bit_out*4)+2) :
                 //mode2 ? tx_data >>((cnt_bit*2)+2 )    :
                 1'b0;//
    end
end
always@(*)begin
    if(!out_en)begin
        s3_out = 1'b0;
    end
    else if(!sck)begin
        s3_out = mode4 ? spi_odata >>((cnt_bit_out*4)+3) :
                 //mode2 ? tx_data >>((cnt_bit*2)+3 )    :
                 1'b0;//
    end
end

reg[7:0] sram_ptr;
reg g_sram_ptr_clk;
always@(*)begin
    if(!byte_clk)begin
        g_sram_ptr_clk = tx_en|state[RX_DATA];//TODO sram_en
    end
end
assign sram_ptr_clk = g_sram_ptr_clk&byte_clk;
always@(posedge sram_ptr_clk or negedge rst)begin
    if(!rst)begin
        sram_ptr <= 0;
    end
    else begin
        sram_ptr <= sram_ptr + 1;
    end
end

//update sram_addr
always@(*)begin
    if(!rst_n)begin
        sram_addr = 'h0;
    end
    else if(g_byte_en)begin
        sram_addr = addr24[7:0] + sram_ptr;
    end
end

assign sram_clk     = sram_ptr_clk;      //update wdata
//assign sram_rst     = g_addr_byte0;
//assign sram_addr    = addr24[7:0];    //256 byte address
assign sram_wr_en   = state[RX_DATA]; //write to sram
assign sram_rd_en   = tx_en;//state[TX_DATA]; //read from sram
assign sram_data_in = din;          //data to write to sram
assign sram_out     = sram_data_out;//data to read from sram
endmodule

