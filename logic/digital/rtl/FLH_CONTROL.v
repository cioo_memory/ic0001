module FLH_CONTROL(
input       rst_n,          //power reset
input       clk_20M,
output      flh_pump_01_en,//read_en
input       flh_pump_01_rdy,//read_rdy
output      flh_sab_en,     //sa bias
input[18:0] flh_addr,
output      flh_rd_clk,
output      flh_sa_en,      //sa en
input[31:0] flh_data_out,


//sapi
input[7:0]  datain,  //address+data
output[7:0] rdataout,//to back_sapi for read data
input       clkin,
input       addrenin,
input       dataoutenin,

output[7:0] dataout,//to next_sapi
output      clkout, //to next_sapi
input[7:0]  rdatain,
output      addrenout,//to next_sapi
output      dataoutenout,

output      por_rdy //parameter ok
);
assign clk = clk_20M;

wire[7:0] p1_datadef;
SAPI #(
 .ADDRP( `P1_ADDR)
,.DATAD( `P1_DATA)
)
P1_RDY_INST(
 .rst_n        (rst_n        )
,.por_ok       (por_rdy      )
,.datain       (datain       )
,.rdataout     (rdataout     )
,.clkin        (clkin        )
,.addrenin     (addrenin     )
,.dataoutenin  (dataoutenin  )
,.dataout      (dataout      )
,.clkout       (clkout       )
,.rdatain      (rdatain      )
,.addrenout    (addrenout    )
,.dataoutenout (dataoutenout )
,.datadef      (p1_datadef   )
);
assign por_rdy = p1_datadef[0];
enum{
IDLE,
EN_RDY,
NUM_STATES
}FLH_CONTROL_STATES;

reg[7:0]            cnt,next_cnt;
reg[NUM_STATES-1:0] state,next_state;
wire[1:0]           mode;
always@(*)begin
    if(!rst_n)begin
        state <= IDLE;
    end
    else begin
        state <= next_state;
    end
end
always@(*)begin
    next_state = '0;
    case(1)
        state[IDLE]:begin
            if(mode==`FLH_READ)begin
                next_state = state[EN_RDY];
            end
        end
        state[EN_RDY]:begin
            if(|cnt)begin
                next_state[EN_RDY] = 1'b1;
            end
            else begin
                //next_state = 
            end
        end
    endcase
end
endmodule
