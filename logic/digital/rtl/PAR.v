module PAR(
input       iclk,
input       rst_n,

output[7:0] par_tx_data,
//SAPI
input[7:0]  par_datain,  //address+data
output[7:0] par_rdataout,//to back_sapi for read data
input       par_clkin,
input       par_addrenin,
input       par_dataoutenin,

output[7:0] par_dataout,//to next_sapi
output      par_clkout, //to next_sapi
input[7:0]  par_rdatain,
output      par_addrenout,//to next_sapi
output      par_dataoutenout,

output      par_por_rdy, //parameter ok


input[7:0]  cmd_latch,
input       fe_busy
);
wire[7:0]   p3_rdatain;
enum{
IDLE,
SET_DATA,
NUM_STATES
}FE_STATES;

wire[7:0] p3_datadef;
wire[7:0] p4_datadef;
wire[7:0] st0,st1;
wire[7:0] p3_rdataout;
wire[7:0] p3_datain;
wire[7:0] p3_dataout;
SAPI #(
 .ADDRP( `P3_ADDR)
,.DATAD( `P3_DATA)
)
P3_INST(
 .rst_n        (rst_n         )
,.por_ok       (par_por_rdy   )
,.datain       (p3_datain     )
,.rdataout     (p3_rdataout   )
,.clkin        (p3_clkin      )
,.addrenin     (p3_addrenin   )
,.dataoutenin  (p3_dataoutenin)
,.dataout      (p3_dataout    )
,.clkout       (p3_clkout     )
,.rdatain      (p3_rdatain    )
,.addrenout    (p3_addrenout  )
,.dataoutenout (p3_dataoutenout)
,.datadef      (p3_datadef )
);
SAPI #(
 .ADDRP( `P4_ADDR)
,.DATAD( `P4_DATA)
)
P4_INST(
 .rst_n        (rst_n         )
,.por_ok       (par_por_rdy   )
,.datain       (p3_dataout     )
,.rdataout     (par_rdataout   )
,.clkin        (p3_clkout      )
,.addrenin     (p3_addrenout   )
,.dataoutenin  (p3_dataoutenout)
,.dataout      (par_dataout    )
,.clkout       (par_clkout     )
,.rdatain      (p3_rdataout    )
,.addrenout    (par_addrenout  )
,.dataoutenout (par_dataoutenout)
,.datadef      (p4_datadef )
);
assign st0   = p3_datadef;
assign st1   = p4_datadef;
assign cmd06 = cmd_latch=='h06;
assign cmd04 = cmd_latch=='h04;
assign cmd05 = cmd_latch=='h05;
assign cmd35 = cmd_latch=='h35;
assign cmd01 = cmd_latch=='h01;
reg[NUM_STATES-1:0] state,next_state;
reg[3:0]            cnt,next_cnt;
reg                 clkin,next_clkin;
reg                 dataoutenin,next_dataoutenin;
reg[7:0]            datain,next_datain;
reg[7:0]            rdata,next_rdata;
reg                 addrenin,next_addrenin;
reg                 flag,next_flag;
//TODO change mode_sel
reg                 cmd_04,next_cmd_04;
reg                 cmd_06,next_cmd_06;
reg                 cmd_01,next_cmd_01;
reg  g_clk;
assign busy = !(state[IDLE])|(cmd06|cmd04|cmd01);
always@(*)begin
    if(!iclk)begin
        g_clk = busy;
    end
end
assign clk = g_clk&iclk;
always@(posedge clk or negedge rst_n)begin
    if(!rst_n)begin
        state       <= 'h1;
        cnt         <= 'h0;
        clkin       <= 'h0;
        dataoutenin <= 1'b0;
        datain      <= 'h0;
        rdata       <= 'h0;
        addrenin    <= 'h0;
        flag        <= 1'b0;
        cmd_06      <= 1'b0;
        cmd_04      <= 1'b0;
        cmd_01      <= 1'b0;
    end
    else begin
        state       <= next_state;
        cnt         <= next_cnt;
        clkin       <= next_clkin;
        dataoutenin <= next_dataoutenin;
        datain      <= next_datain;
        rdata       <= next_rdata;
        addrenin    <= next_addrenin;
        flag        <= next_flag;
        cmd_06      <= next_cmd_06;
        cmd_04      <= next_cmd_04;
        cmd_01      <= next_cmd_01;
    end
end
always@(*)begin
    next_state       = 'h0;
    next_cnt         = |cnt ? cnt - 1 : cnt;
    next_clkin       = 1'b0;
    next_dataoutenin = dataoutenin;
    next_datain      = datain;
    next_rdata       = rdata;
    next_flag        = flag;
    next_addrenin    = 1'b0;
    next_cmd_06      = cmd_06;
    next_cmd_04      = cmd_04;
    next_cmd_01      = cmd_01;
    case(1'b1)
        state[IDLE]:begin
            if(fe_busy&(cmd06|cmd04))begin
                next_cnt             = 4;
                next_state[SET_DATA] = 1'b1;
                next_flag            = 1'b1;
                next_cmd_06          = cmd06;
                next_cmd_04          = cmd04;
                next_cmd_01          = cmd01;
            end
            else begin
              next_state[IDLE]     = 1'b1;
            end
        end
        state[SET_DATA]:begin
            case(cnt)
                4:begin//address
                    next_datain          = `P3_ADDR;
                    next_state[SET_DATA] = 1'b1;
                end
                3:begin
                    next_addrenin        = 1'b1;
                    next_clkin           = 1'b1;
                    next_state[SET_DATA] = 1'b1;
                end
                2:begin//wdata
                    next_addrenin        = 1'b1;
                    next_datain     = cmd_06 ? {p3_datadef[7:2],1'b1,p3_datadef[0]} ://set_wel
                                      cmd_04 ? {p3_datadef[7:2],1'b0,p3_datadef[0]} ://reset wel
                                      'h0;
                    next_state[SET_DATA] = 1'b1;
                end
                1:begin
                    next_clkin      =1'b1;
                    next_state[SET_DATA] = 1'b1;
                end
                0:begin
                    next_state[IDLE] = 1'b1;
                    next_datain      = 'h0;
                    next_flag        = 1'b0;
                end
            endcase
        end
    endcase
end
assign p3_datain      = flag ? datain : par_datain;//flag ? datain : par_datain;  //address+data
assign p3_clkin       = clkin|par_clkin;
assign p3_addrenin    = addrenin|par_addrenin;
assign p3_dataoutenin = dataoutenin|par_dataoutenin;
assign p3_rdatain     = par_rdatain;

assign par_tx_data = cmd05 ? st0 : 
                     cmd35 ? st1 :
                     'h0;
endmodule
