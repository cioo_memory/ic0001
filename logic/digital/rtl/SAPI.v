
module SAPI
#(
parameter ADDRP = 0, //parameter address
parameter DATAD = 0  //parameter default value
)
(
input       rst_n,
input       por_ok,
input[7:0]  datain,  //address+data
output[7:0] rdataout,//to back_sapi for read data
input       clkin,
input       addrenin,
input       dataoutenin,

output[7:0] dataout,//to next_sapi
output      clkout, //to next_sapi
input[7:0]  rdatain,
output      addrenout,//to next_sapi
output      dataoutenout,
output[7:0] datadef
);
wire[7:0] data;
assign addren    = addrenin;
assign dataouten = dataoutenin;
assign clk       = clkin;
assign data      = datain;
assign es        = addren&(datain==ADDRP);

reg sel;
always@(negedge clk or negedge rst_n)begin
    if(!rst_n)begin
        sel <= 1'b0;
    end
    else begin
        sel <= addren ? es : 1'b0;
    end
end

assign wr_clk = !dataouten & sel & clk;
reg[7:0] datal;
always@(*)begin
    if(!rst_n)begin
        datal <= DATAD;
    end
    else if(wr_clk)begin
        datal <= data;
    end
end
assign datadef      = datal;
assign dataout      = data;                 //---> to next sapi,even write latch
assign rdataout     = dataouten&sel ? (por_ok ? datadef : DATAD) : rdatain;//<---- get latch data when selcted
assign clkout       = clk;                  //to next
assign addrenout    = addren;               //to next
assign dataoutenout = dataouten;
endmodule
