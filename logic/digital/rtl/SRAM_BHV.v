module SRAM_BHV
(
input            sram_clk,
input[7:0]       sram_addr,
input            sram_wr_en,
input            sram_rd_en,
input[7:0]       sram_data_in,
output reg[7:0]  sram_data_out
);
reg[7:0] sram[255:0];//256 byte data
//initial begin
//sram[0] = 'h55;
//sram[1] = 'h66;
//sram[2] = 'h77;
//sram[3] = 'h88;
//sram[4] = 'h99;
//sram[5] = 'haa;
//sram[6] = 'hbb;
//end
always@(posedge sram_clk)begin
    if(sram_wr_en)begin
        sram[sram_addr] <= sram_data_in;
    end
end
always@(posedge sram_clk)begin
    if(sram_rd_en)begin
        sram_data_out <= sram[sram_addr];
    end
end
//assign sram_data_out = sram[sram_addr];
endmodule
