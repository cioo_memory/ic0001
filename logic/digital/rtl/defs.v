`define TCYCLE 20
`define TDVCH  1

`define WRSR 'h01

//ADDR
`define P1_ADDR 'h01
`define P2_ADDR 'h02
`define P3_ADDR 'h03
`define P4_ADDR 'h04
`define P5_ADDR 'h05





//default data
`define P1_DATA  'h00  //[0]:por_rdy   
`define P2_DATA  'h00  //[1:0]:mode
                       //2'b00:1spi,2'b01:2spi,2'b10:4spi
`define P3_DATA  'h00  //s7-s0 for cmd05
`define P4_DATA  'haa  //s15-s8 for cmd35



//FLH operation
`define FLH_READ 'h01


//test command
`define  WRP 'hff //write parameter
`define  RDP 'hfe //read parameter
`define  WPB 'hfd //write pagebuffer
`define  RPB 'hfc //write pagebuffer
