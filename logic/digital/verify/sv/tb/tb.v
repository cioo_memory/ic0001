`timescale 1ns/100ps
module tb;
reg cs;
reg s0_in;
reg s1_in;
reg s2_in;
reg s3_in;
wire s0_out;
wire s1_out;
wire s2_out;
wire s3_out;
reg  S0_ERROR;
reg  S1_ERROR;
reg  S2_ERROR;
reg  S3_ERROR;
reg  BYTE_ERROR;
reg csn;
reg sck;
reg rst_n;
reg[1:0] mode;//for 1/2/4 wire
reg[1:0] spi_mode;//for spi mode
initial begin
    `ifdef DUMPVCD
        $dumpfile("tb.vcd");
        $dumpvars(0,tb);
    `elsif FSDB
        $dumpfile("tb.fsdb");
        $dumpvars(0,tb);
    `endif
end
initial begin
    spi_mode   = 0;
    mode       = 0;
    S0_ERROR   = 0;
    S1_ERROR   = 0;
    S2_ERROR   = 0;
    S3_ERROR   = 0;
    BYTE_ERROR = 0;
end
initial begin
    s0_in = 'b0;
    s1_in = 'b0;
    s2_in = 'b0;
    s3_in = 'b0;
    csn   = 'b1;
    sck   = 'b0;
    rst_n = 1'b0;
    #100ns;
    rst_n = 1;
end
reg osc_clk;
initial begin
    osc_clk = 1'b0;
    forever begin//20M
        #25ns osc_clk = !osc_clk;
    end
end
task ADD_IN3(
input[23:0] addr24=0
);
    SDR_IN(addr24[23:16]);
    SDR_IN(addr24[15:8] );
    SDR_IN(addr24[7 :0] );
endtask
task CMD_IN(//always 1wire
    input[7:0] data
);
    reg[1:0] tmp_mode;
    tmp_mode = mode;
    mode = 0;
    SDR_IN(data);
    mode = tmp_mode;
endtask
task DMY;
    SDR_IN('h00);
endtask
initial begin
    if($test$plusargs("FELOGIC_test"))begin
        `include "./stimulus.v"
    end
    else if($test$plusargs("PARAM_test"))begin
        `include "./stimulus.v"
    end
    else begin
        `include "./stimulus.v"
    end
end
initial begin
    #10ms;
    $finish;
end
task START;
    if(spi_mode==2'b11)begin
        sck = 1'b1;
    end
    else if(spi_mode==2'b00)begin
        sck = 1'b0;
    end
    #2ns;
    csn = 1'b0;
    #(`TCYCLE);
endtask
//TODO auto check
task SDR_OUT(
input[7:0] exp_data='h0
);
    reg[3:0] i;
    reg[7:0] data=0;
    sck = 1'b0;
    #(`TDVCH);
    case(mode)
        0:begin//1wire
            for(i=0;i<8;i++)begin
                #(`TCYCLE);
                sck   = 1'b1;
                #(`TDVCH);
                //sample
                data = {data[6:0],s1_out};
                #(`TCYCLE-`TDVCH);
                sck   = 1'b0;
                S1_ERROR = !(s1_out== exp_data[7-i]);
            end
        end
        1:begin//2wire
            for(i=0;i<8;i=i+2)begin
                #(`TCYCLE);
                sck   = 1'b1;
                #(`TDVCH);
                data = {data[5:0],s1_out,s0_out};
                #(`TCYCLE-`TDVCH);
                sck   = 1'b0;
                S0_ERROR = !(s0_out== exp_data[6-i]);
                S1_ERROR = !(s1_out== exp_data[7-i]);
            end
        end
        2:begin//4wire
            for(i=0;i<8;i=i+4)begin
                #(`TCYCLE);
                sck   = 1'b1;
                #(`TDVCH);
                data = {data[3:0],s3_out,s2_out,s1_out,s0_out};
                #(`TCYCLE-`TDVCH);
                sck   = 1'b0;
                S0_ERROR = !(s0_out== exp_data[4-i]);
                S1_ERROR = !(s1_out== exp_data[5-i]);
                S2_ERROR = !(s2_out== exp_data[6-i]);
                S3_ERROR = !(s3_out== exp_data[7-i]);
            end
        end
        default:begin
        end
    endcase
    BYTE_ERROR = !(data==exp_data);
    if(BYTE_ERROR) $display("\t time :%0t,exp vs real:'h%02h vs 'h%02h",$time,exp_data,data);
endtask
task SDR_IN(
input[7:0] data='h0
);
//mode='h0   //0:1wire 1:2wire 2:4wire
    reg[3:0] i;
    sck = 1'b0;
    #(`TDVCH);
    case(mode)
        0:begin//1wire
            for(i=0;i<8;i++)begin
                #(`TCYCLE);
                s0_in = data>>(7-i);
                #(`TDVCH);
                sck   = 1'b1;
                #(`TCYCLE-`TDVCH);
                sck   = 1'b0;
            end
        end
        1:begin//2wire
            for(i=0;i<8;i=i+2)begin
                #(`TCYCLE);
                s1_in = data>>(7-i);
                s0_in = data>>(6-i);
                #(`TDVCH);
                sck   = 1'b1;
                #(`TCYCLE-`TDVCH);
                sck   = 1'b0;
            end
        end
        2:begin//4wire
            for(i=0;i<8;i=i+4)begin
                #(`TCYCLE);
                s0_in = data>>(4-i);
                s1_in = data>>(5-i);
                s2_in = data>>(6-i);
                s3_in = data>>(7-i);
                #(`TDVCH);
                sck   = 1'b1;
                #(`TCYCLE-`TDVCH);
                sck   = 1'b0;
            end
        end
        default:begin
        end
    endcase
endtask
task STOP;
    if(spi_mode==2'b00)begin
        sck = 1'b0;
    end
    else if(spi_mode==2'b11)begin
        sck = 1'b1;
    end
    #(`TCYCLE);
    csn = 1'b1;
    #20ns;
endtask
//logic clk_20M_in;
//initial begin
//    clk_20M_in = 1'b0;
//    forever begin
//        #25ns;
//        clk_20M_in = ~clk_20M_in;
//    end
//end
////bhv
//CLK_GEN(
// .clk_20M_in (clk_20M_in)
//,.clk_20M_out(clk_20M_out)
//);
wire[7:0]       sram_addr;
wire[7:0]       sram_data_in;
wire[7:0]       sram_data_out;
DIGITAL_TOP DIGITAL_TOP(
 .osc_clk       (osc_clk       )
,.pad_sclk_in   (sck           )
,.pad_cs_in     (csn           )
,.pad_si_in     (s0_in         )
,.pad_so_in     (s1_in         )
,.pad_wp_in     (s2_in         )
,.pad_hold_in   (s3_in         )
,.por_rst_n     (rst_n         )
,.sram_clk      (sram_clk      )
,.sram_addr     (sram_addr     )
,.sram_wr_en    (sram_wr_en    )
,.sram_rd_en    (sram_rd_en    )
,.sram_data_in  (sram_data_in  )
,.sram_data_out (sram_data_out )
,.pad_si_out    (s0_out        )
,.pad_so_out    (s1_out        )
,.pad_wp_out    (s2_out        )
,.pad_hold_out  (s3_out        )

);

//bhv
SRAM_BHV SRAM_BHV(
 .sram_clk      (sram_clk      )
,.sram_addr     (sram_addr     )
,.sram_wr_en    (sram_wr_en    )
,.sram_rd_en    (sram_rd_en    )
,.sram_data_in  (sram_data_in  )
,.sram_data_out (sram_data_out )

);
endmodule
