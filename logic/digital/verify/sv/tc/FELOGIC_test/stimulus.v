#10us;
////WREN--cmd06
////WEL latch active
//START;
//SDR_IN('h06);
//STOP;
//
////WRDI--cmd04
////disable wel_latch
//START;
//SDR_IN('h04);
//STOP;
//START;
//SDR_IN('h06);
//STOP;
////WRST---cmd01
////+8/16 bit
//START;
//SDR_IN('h01);
//SDR_IN('ha5);
//SDR_IN('h33);
//STOP;


////PP---page program cmd02
//START;
//SDR_IN('h02);
//ADD_IN3('h010203);
//SDR_IN('haa);
//SDR_IN('hbb);
//SDR_IN('hcc);
//SDR_IN('hdd);
//STOP;
//
//
//
//START;
//SDR_IN('h03);
//ADD_IN3('h010203,1);
//SDR_OUT('h88,1);
//SDR_OUT('h9,1);
//STOP;

//START;
//SDR_IN('h05);
//SDR_OUT('haa);
//SDR_OUT('hbb);
//STOP;


//START;
//SDR_IN('h35);
//SDR_OUT('haa);
//SDR_OUT('hbb);
//STOP;


//START;
//SDR_IN('h0b);
//ADD_IN3('h010203);
//SDR_IN('h00);
//SDR_OUT('h0a);
//SDR_OUT('h0b);
//STOP;

////wel
//START;
//SDR_IN('h06);
//STOP;
//
////sector erase
//START;
//SDR_IN('h20);
//ADD_IN3('h010203);
//STOP;
//
//QE PP

////wel
//START;
//SDR_IN('h06);
//STOP;
//
//START;
//SDR_IN('h32);
//ADD_IN3('h010203);
//SDR_IN('h32,2);
//SDR_IN('hab,2);
//SDR_IN('hcc,2);
//STOP;

//START;
//SDR_IN('h3b);
//ADD_IN3('h010203);
//SDR_IN('h00);
//SDR_OUT('h0a);
//SDR_OUT('h0b);
//STOP;


//START;
//SDR_IN('h6b);
//ADD_IN3('h010203);
//SDR_IN('h00);
//SDR_OUT('h0a);
//SDR_OUT('h0b);
//STOP;

////wel
//START;
//SDR_IN('h06);
//STOP;
//
//START;
//SDR_IN('h42);
//ADD_IN3('h010203);
//SDR_IN('h23);
//SDR_IN('h32);
//STOP;
//SDR_IN('h0a);
//SDR_IN('h0b);//no stop for cmd42
////
////wel
//START;
//SDR_IN('h06);
//STOP;
//START;
//SDR_IN('h44);
//ADD_IN3('h030a0d);
//STOP;


//START;
//SDR_IN('h48);
//ADD_IN3('h010203);
//SDR_IN('hcc);
//SDR_IN('hee);
//STOP;



//START;
//SDR_IN('h50);
//STOP;
//
//START;
//SDR_IN('h52);
//ADD_IN3('h010203);
//STOP;
//
//
//START;
//SDR_IN('h5a);
//ADD_IN3('h020203);
//SDR_OUT('h03);
//SDR_OUT('h43);
//SDR_OUT('h03);
//STOP;
//
//wel
//SDR_IN('h06);
//STOP;
//
//START;
//SDR_IN('h60);
//STOP;
//
////wel
//START;
//SDR_IN('h06);
//STOP;
//
//START;
//SDR_IN('hc7);
//STOP;

////wel
//START;
//SDR_IN('h66);
//STOP;
//
//START;
//SDR_IN('h99);
//STOP;

//START;
//SDR_IN('h90);
//ADD_IN3('h010203);
//SDR_OUT('h03);
//SDR_OUT('hB3);
//STOP;

//START;
//SDR_IN('h92);
//ADD_IN3('h010203);
//SDR_OUT('h03,2);
//SDR_OUT('hB3,2);
//STOP;

//START;
//SDR_IN('h94);
//ADD_IN3('h010203,2);
//SDR_IN('hB3,2);
//SDR_IN('hB3,2);
//SDR_OUT('hB3,2);
//SDR_OUT('hB3,2);
//STOP;
//
//START;
//SDR_IN('h9f);
//SDR_OUT('h11);
//SDR_OUT('h22);
//SDR_OUT('h33);
//STOP;

//START;
//SDR_IN('hab);
//ADD_IN3('h010203);
//SDR_OUT('h33);
//STOP;

//START;
//SDR_IN('he7);
//ADD_IN3('h010203,2);
//SDR_IN('h33,2);
//SDR_OUT('hBB,2);
//STOP;


//START;
//SDR_IN('heb);
//ADD_IN3('h010203,2);
//SDR_IN('h33,2);
//SDR_IN('h33,2);
//SDR_OUT('hBB,2);
//STOP;


//START;
//SDR_IN('hbb);
//ADD_IN3('h010203,1);
//SDR_IN('h33,1);
//SDR_OUT('hBB,1);
//SDR_OUT('hcc,1);
//STOP;


//mode = 1;
//spi_mode = 3;
//
START;
CMD_IN(`WRP);
SDR_IN(`P1_ADDR);
SDR_IN('hb1);
STOP;

mode=0;
START;
CMD_IN(`WRP);
SDR_IN(`P2_ADDR);
SDR_IN('ha1);//1wire 2wire 4wire
STOP;

mode = 1;
START;
CMD_IN(`RDP);
SDR_IN(`P1_ADDR);
DMY;
SDR_OUT('hb1);
STOP;

spi_mode=3;
START;
CMD_IN(`RDP);
SDR_IN(`P2_ADDR);
DMY;
SDR_OUT('ha1);
STOP;

START;
CMD_IN(`WPB);
ADD_IN3('h010203);
SDR_IN('haa);
SDR_IN('hbb);
SDR_IN('hcc);
SDR_IN('hdd);
STOP;

START;
CMD_IN(`RPB);
ADD_IN3('h010203);
SDR_OUT('haa);
SDR_OUT('hbb);
SDR_OUT('hcc);
STOP;

START;
CMD_IN('h06);
STOP;
#10us;
START;
CMD_IN(`RDP);
SDR_IN(`P3_ADDR);
DMY;
SDR_OUT('h02);
STOP;

START;
CMD_IN('h05);
SDR_OUT('h02);
SDR_OUT('h02);
SDR_OUT('h02);
STOP;





START;
CMD_IN('h04);
STOP;
#10us;
START;
CMD_IN(`WRP);
SDR_IN(`P3_ADDR);
DMY;
SDR_OUT('h00); 
STOP;

START;
CMD_IN('h01);
SDR_IN('hab);
SDR_IN('hac);
STOP;

START;
CMD_IN('h05);
SDR_OUT('hab);
SDR_OUT('hab);
SDR_OUT('hab);
STOP;

START;
CMD_IN('h05);
SDR_OUT('hac);
SDR_OUT('hac);
SDR_OUT('hac);
STOP;


$finish;
