+incdir+$mdl_path
+incdir+$inc_path
+incdir+$tb_path
+incdir+$mdl_path/cioo_interface/
+incdir+$mdl_path/cioo_flash_model/
+incdir+$UVM_HOME/src
+incdir+$UVM_HOME
//+incdir+$QUESTA_UVM_HOME/src
//+incdir+$QUESTA_UVM_HOME


//$inc_path/cioo_std_class.sv
//$UVM_HOME/src/uvm.sv
//$UVM_HOME/src/uvm_pkg.sv
//$UVM_HOME/src/dpi/uvm_dpi.cc
//$QUESTA_UVM_HOME/src/questa_uvm_pkg.sv

$mdl_path/cioo_interface/ad_if_top.sv
$mdl_path/chip_top.sv
$mdl_path/cioo_flash_model/cioo_flash_model.sv
//$mdl_path/cioo_flash_model/fpga_flash_model.v
$mdl_path/cioo_sram_model/cioo_sram_model.sv
$mdl_path/cioo_sram_model/sram_mem_core.sv


$mdl_path/cioo_analog_model/cioo_analog_top.sv
$mdl_path/cioo_analog_model/analog_power/cioo_power_top.sv
$mdl_path/cioo_analog_model/analog_power/cioo_power_flash.sv
$mdl_path/cioo_analog_model/analog_pad/cioo_pad_top.sv
$mdl_path/cioo_analog_model/analog_pad/pad_model_flash.sv

$mdl_path/cioo_analog_model/analog_osc/cioo_osc_model.sv

$rtl_path/defs.v
$rtl_path/DIGITAL_TOP.v
$rtl_path/FELOGIC.v
$rtl_path/FLH_CONTROL.v
$rtl_path/SAPI.v
$rtl_path/PAR.v
//$rtl_path/SRAM_BHV.v

$tb_path/TESTBENCH.sv
$tb_path/INTERFACE.sv
$tb_path/TRANSACTION_FLASH.sv
$tb_path/TRANSACTION_SCB.sv
$tb_path/DRIVER_FLASH.sv
$tb_path/MONITOR_FLASH.sv
$tb_path/SEQUENCER_FLASH.sv
//$tb_path/SEQUENCER_REG.sv
$tb_path/AGENT_FLASH.sv
$tb_path/SCOREBOARD.sv
//$tb_path/REG_MODEL_FLASH.sv
//$tb_path/SEQUENCER_ALL.sv
$tb_path/REFERENCE_FLASH.sv
$tb_path/ENVIRONMENT.sv
$tb_path/BASE_TEST.sv




//FLASH Library path
//-v $lib_path/sc9xz_ch018ull_base_g5p0.v
//-v $lib_path/sc9xz_ch018ull_base_g5p0_udp.v
//
////Flash Library path
//-v $lib_path/smic18m_neg.v

$tc_path/test_sram_wr.sv
