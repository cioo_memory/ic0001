`ifndef __ANALOG_OSC_HEADER__
    `define __ANALOG_OSC_HEADER__
    //5.osc
    `define tOSC_SWITCH_ON        6000
    `define tOSC_SWITCH_OFF       10

    `define tOSC_DELAY_1          65_000+`tOSC_SWITCH_ON    //64us
    `define tOSC_DELAY_2          81_000+`tOSC_SWITCH_ON    //80us

    `ifdef SIM_FAST
        `define OSC_FREQ 100_000_000
        `define OSC_1_FREQ 100_000_000
        `define OSC_2_FREQ 200_000_000
        `define OSC_3_FREQ 300_000_000
    `else
        `define OSC_FREQ 1_000_000
        `define OSC_1_FREQ 10_000_000
        `define OSC_2_FREQ 20_000_000
        `define OSC_3_FREQ 40_000_000
    `endif


`endif
