`ifndef __ANALOG_PAD_HEADER__
    `define __ANALOG_PAD_HEADER__
    //1.temperature protection
    `define OTW_CLR               16'd01_40
    `define OTW_SET               16'd01_55
    `define OTSD_CLR              16'd01_55

    `define OTSD_SET              16'd01_75
    
    `define TEMP_FLAG1            16'd01_05
    `define TEMP_FLAG2            16'd01_25
    `define TEMP_FLAG3            16'd01_35
    `define TEMP_FLAG4            16'd01_75

    //5.osc
    `define tOSC_SWITCH_ON        6000
    `define tOSC_SWITCH_OFF       10

    `define tOSC_DELAY_1          65_000+`tOSC_SWITCH_ON    //64us
    `define tOSC_DELAY_2          81_000+`tOSC_SWITCH_ON    //80us


    `ifdef PRODUCT_EEPROM
        `ifdef POSTMAX
        //MIN and TYP
            `define PAD_DLY_SCL     70
            `define PAD_DLY_SDA     65 
            `define PAD_DLY_A0      1
            `define PAD_DLY_A1      1
            `define PAD_DLY_A2      1
            `define PAD_DLY_WP      1
        `else
            `define PAD_DLY_SCL     56
            `define PAD_DLY_SDA     55
            `define PAD_DLY_A0      1
            `define PAD_DLY_A1      1
            `define PAD_DLY_A2      1
            `define PAD_DLY_WP      1
        `endif
    `endif

    `ifdef PRODUCT_FLASH
        `ifdef POSTMIN
            `define PAD_DLY_CS      1
            `define PAD_DLY_SCLK    1
            `define PAD_DLY_DI      1
            `define PAD_DLY_DO      1
            `define PAD_DLY_WP      1
            `define PAD_DLY_HOLD    1
        `else
            `define PAD_DLY_CS      1
            `define PAD_DLY_SCLK    1
            `define PAD_DLY_DI      1
            `define PAD_DLY_DO      1
            `define PAD_DLY_WP      1
            `define PAD_DLY_HOLD    1
        `endif
    `endif
    

`endif
