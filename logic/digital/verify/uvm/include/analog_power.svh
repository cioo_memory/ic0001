`ifndef __ANALOG_POWER_HEADER__
    `define __ANALOG_POWER_HEADER__

    `define PVDD_MIN

    //1.Power supplies
    `define tPOR_RELEASE            1000
    `define tPOR_ACTIVE             10

    `define tVCC_ON                 1300
    `define tVCC_OFF                10

    `define tCHP_ON               900000
    `define tAVDD_UVLO_DIS        500000

    `define tPOWER_ON               5000
    `define tPOWER_OFF              1000
    `define tVCC_LOCKOUT_ON         6000
    `define tVCC_LOCKOUT_OFF        800 
    `define tVCC_RDY_ON             7000
    `define tVCC_RDY_OFF            600

    //2.protection control
    `define tPD_E_L               16'd24000
    `define tPD_E_SD              16'd7000
    `define tVDS_PULSE            16'd56000
    `define vSNSOCP_TRIP          16'd02_00
`endif
