`ifndef __DEFINE_IF_HEADER__
    `define __DEFINE_IF_HEADER__

    //interface always be defined

    `define INTERFACE_IIC
    `define INTERFACE_SPI_4_WIRE

    `ifdef INTERFACE_SPI_4_WIRE
        `define INTERFACE_SPI
    `elsif INTERFACE_SPI_1_WIRE
        `define INTERFACE_SPI
    `endif

    //`define INTERFACE_IIC
    //`define INTERFACE_SPI_1_WIRE
    //`define INTERFACE_IIC_SPI_1_WIRE
    //`define INTERFACE_IIC_SPI_4_WIRE

    `define INTERFACE_IIC_EEPROM    3'b000
    `define INTERFACE_SPI_EEPROM    3'b001
    `define INTERFACE_ALL_EEPROM    3'b111

    `define INTERFACE_1_WIRE_SPI_FLASH    3'b000
    `define INTERFACE_2_WIRE_SPI_FLASH    3'b001
    `define INTERFACE_4_WIRE_SPI_FLASH    3'b010

`endif

