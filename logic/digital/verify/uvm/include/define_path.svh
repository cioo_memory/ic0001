`ifndef __PATH_HEADER__
    `define __PATH_HEADER__
    
    `define top TESTBENCH
    `define dut `top.dut
    `define analog_top `dut.gt_analog_top_inst
    `ifdef PRODUCT_EEPROM
        `define power `analog_top.gt_power_top.gt_power_eeprom
    `elsif PRODUCT_FLASH
        `define power `analog_top.gt_power_top.gt_power_flash
    `endif

    `define digital `dut.digital_top_inst

    `ifndef FPGA
        `define ee_model `dut.gt_eeprom_model
    `else
        `define ee_model `dut.eeprom_bhv_inst
    `endif
    

    //Testbench
    `define scb_ee env.agt_eeprom.scb_ee
    `define scb_flh env.agt_flash.scb_flh
    `define drv_iic_ee env.agt_eeprom.drv_ee.drv_iic_ee
    `define drv_spi_ee env.agt_eeprom.drv_ee.drv_spi_ee
    `define cfg env.agt_eeprom.drv_iic_ee.cfg
    `define spi env.agt_flash.drv_flh.drv_spi_flh
    `define mon_ee env.agt_eeprom.mon_ee

    `define drv_spi_flh env.agt_flash.drv_flh.drv_spi_flh
    //
    `ifdef PRODUCT_FLASH
        `define d2a_monitor `analog_top.d2a_monitor 
        `define a2d_driver `analog_top.a2d_driver 

        `ifndef FPGA
            `define flash_model `top.dut.gt_flash_model
            `define main_memory `top.dut.gt_flash_model.flash_array_digital
            `define otp1_memory `top.dut.gt_flash_model.flash_array_digital[`FLASH_ALL_WORD_SIZE-1:`FLASH_MAIN_WORD_SIZE]
            `define otp2_memory `top.dut.gt_flash_model.flash_otp2_digital
        `else
            `define flash_model `top.dut.gt_flash_model
            `define main_memory `flash_model.flash_mem_inst.ram_mem
            `define otp1_memory `top.dut.gt_flash_model.otp1_mem_inst.ram_mem
            `define otp2_memory `top.dut.gt_flash_model.otp2_mem_inst.ram_mem
        `endif

        `ifdef POSTFPGA
            `define flash_pst_array 
        `else
            `define flash_pst_array `flash_model.flash_pst_array
        `endif

    `endif


`endif
