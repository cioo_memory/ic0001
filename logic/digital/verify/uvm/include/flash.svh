`ifndef __FLASH_HEADER__
    `define __FLASH_HEADER__

    `define RUN_TIMES               1
    `define GT25Q40L 2'b00
    `define GT25Q20L 2'b01
    `define GT25Q10L 2'b10
    `define GT25Q05L 2'b11

    `define FLASH_ADDR_MAIN_WIDTH   19 
    `define FLASH_ADDR_OTP_WIDTH    10
    //`define FLASH_ADDR_WIDTH        19 
    `define FLASH_DATA_IN_WIDTH     32 
    `define FLASH_DATA_OUT_WIDTH    32 
    `define FLASH_DATA_F2L_WIDTH    32 
    `define FLASH_DATA_WIDTH        32 
    `define FLASH_OPT_WIDTH         32 
    `define FLASH_BIT_SEL_WIDTH     5  

    `define FLASH_SECTOR_SIZE       (4*1024)
    `define FLASH_32K_BLOCK_SIZE    (32*1024)
    `define FLASH_64K_BLOCK_SIZE    (64*1024)
    `define FLASH_CHIP_SIZE         (512*1024)

    `ifdef GT25Q40L
        `define FLASH_MAIN_SIZE          512*1024
    `elsif GT25Q20L
        `define FLASH_MAIN_SIZE          256*1024
    `elsif GT25Q10L
        `define FLASH_MAIN_SIZE          128*1024
    `else
        `define FLASH_MAIN_SIZE          64*1024
    `endif
    
    `define FLASH_OTP1_SIZE         2*1024
    `define FLASH_OTP1_WL_SIZE      512
      
    `define FLASH_OTP2_SIZE         96
    
    `define FLASH_SR1_ADDR 0 
    `define FLASH_SR2_ADDR 1 


    `define FLASH_MAIN_BYTE_SIZE    (`FLASH_MAIN_SIZE)
    `define FLASH_MAIN_WORD_SIZE    (`FLASH_MAIN_BYTE_SIZE/4)
    `define FLASH_OTP1_BYTE_SIZE    (`FLASH_OTP1_SIZE)
    `define FLASH_OTP1_WORD_SIZE    (`FLASH_OTP1_BYTE_SIZE/4)
    `define FLASH_OTP2_BYTE_SIZE    (`FLASH_OTP2_SIZE)
    `define FLASH_OTP2_WORD_SIZE    (`FLASH_OTP2_BYTE_SIZE/4)

    `define FLASH_ALL_BYTE_SIZE     (`FLASH_MAIN_BYTE_SIZE + `FLASH_OTP1_BYTE_SIZE)
    `define FLASH_ALL_WORD_SIZE     (`FLASH_MAIN_WORD_SIZE + `FLASH_OTP1_WORD_SIZE)

    `define FLASH_WORD_LINE_SIZE    512
    `define FLASH_WORD_LINE_NUM     (`FLASH_ALL_BYTE_SIZE/`FLASH_WORD_LINE_SIZE)


    `define FLASH_SECTOR_NUM        128
    `define FLASH_32K_BLOCK_NUM     16
    `define FLASH_64K_BLOCK_NUM     8
    `define FLASH_PAGE_NUM          2048

    `define ANA_INIT_VALUE          {32{7'h00}}
    //`define DIG_INIT_VALUE          {4{8'h00}}
    `define DIG_INIT_VALUE          {4{8'hff}}

    `define OTP2_ANA_INIT_VALUE          {32{7'h00}}
    //`define   OTP2_DIG_INIT_VALUE          {4{8'h00}}
    `define OTP2_DIG_INIT_VALUE          {4{8'hff}}

    //define the timing

    `ifndef SIM_FAST
        `define tV_SENSE_ON             90
        `define tV_SENSE_OFF            10
        `define tPUMP_01_ON             1000
        `define tPUMP_01_OFF            50
        `define tPUMP_02_ON             1000
        `define tPUMP_02_OFF            50
        `define tPUMP_03_ON             3000
        `define tPUMP_03_OFF            500
        `define tPUMP_04_ON             3000
        `define tPUMP_04_OFF            500
        `define tACC                    30
        `define tDATA_UPDATE_DLY        10
    `else
        `define tV_SENSE_ON             2
        `define tV_SENSE_OFF            1
        `define tPUMP_01_ON             1000
        `define tPUMP_01_OFF            50
        `define tPUMP_02_ON             1000
        `define tPUMP_02_OFF            50
        `define tPUMP_03_ON             3000
        `define tPUMP_03_OFF            500
        `define tPUMP_04_ON             3000
        `define tPUMP_04_OFF            500
        `define tACC                    6
        `define tDATA_UPDATE_DLY        3
    `endif

    //spec timing

    `define tVSS                    90
    `define tRDP                    9   //16 ----> 9
    `define tADRS                   1
    `define tADRH                   -1
    `define tRDDLY                  7
    `define tVPPRDOPS               1000

    `define tADRH_OEV               14
    `define tOEVP                   16

    //program verify + program
    `define tPSS                    25000
    `define tVPGRDY                 1000
    `define tVPS                    0
    `define tADR2PVS                -1
    `define tPVS                    700
    `define tPPSW                   190
    `define tPP0                    500
    `define tPP1                    1000
    `define tPP2                    2000
    `define tPP3                    3000
    `define tPP4                    4000
    `define tPP5                    6000
    `define tPP6                    8000
    `define tPP7                    10000
    `define tPPS                    700
    `define tPPH                    48      //typ 50
    `define tPH                     48      //typ 50
    `define tOPS                    48      //typ 50

    //erase verify
    `define tRPMPS                  10000
    `define tRDREGS                 2000    //4000 twbh = 2us

    `define tPS                     48  //typ = 50
    `define tPREGS                  48  //typ = 50


    `define tCLR                    2000
    `define tWBH                    2000

    //erase timing requirment
    `define tEPDLY                  1000
    `define tVPDLY                  50
    `define tVPPS                   20000
    `define tPOSREGH                6000
    `define tPOSREGH2               10000
    `define tRDREGH                 5000


    `define tEVS                    1
    `define tOPH                    1
    `define tVPPGS                  10
    `define tVPRDY                  10000
    `define tEPH                    20000
    `ifdef SIM_FAST
        `define tEPP1                   50_000
        `define tEPP2                   50_000
    `else
        `define tEPP1                   1000000
        `define tEPP2                   2000000
    `endif
    `ifdef SIM_FAST
        `define TEPW1_DFT 3'd0
        `define TEPW2_DFT 3'd0
        `define TPPW1_DFT 3'd0
        `define TPPW2_DFT 3'd0
        `define TSP1_DFT  3'd0
        `define TSP2_DFT  3'd0
    `else
        `define TEPW1_DFT 3'd1
        `define TEPW2_DFT 3'd3
        `define TPPW1_DFT 3'd1
        `define TPPW2_DFT 3'd2
        `define TSP1_DFT  3'd1
        `define TSP2_DFT  3'd1
    `endif
    `define tVOPH                   -1

    `define tXADRS                  2000 
    //OEV
    `define tOEVS1                  100 
    `define tOEVH                   -1
    `define tOEVP                   16

    `define tOEVS2_1                100 
    `define tOEVS2_2                25000

    //soft pgm
    `define tVPDRDY                 3000
    `define tVPDS                   4000
    `define tSPP                    1000
    `define tVPPDH                  50
    `define tSPH                    850
    `define tSPS                    48
    `define tPOSREGS                2000
    `define tPOSREGH3               50

    `define FLASH_RDV               12

    `define FLASH_OEV1_LOW          20
    `define FLASH_OEV1_HIGH         99

    `define FLASH_OEV2_LOW          0
    `define FLASH_OEV2_HIGH         99

    `ifdef NO_TIMEOUT
        `define FLASH_SP1_INC           10  //40 indicate the sp1 will always pass 
        `define FLASH_SP2_INC           3   //3 indicate the sp2 will always pass 
        //random dist setting
        `define ERASE_RANDOM_DIST       1   //0 indicate ev will always pass
        `define ERASE_TO_EV_DIST        7
        `define ERASE_TO_OEV1_DIST      1
        `define ERASE_TO_OEV2_DIST      1
    `elsif PGM_TIMEOUT
        `define FLASH_SP1_INC           40  //40 indicate the sp1 will always pass 
        `define FLASH_SP2_INC           3   //3 indicate the sp2 will always pass 
        //random dist setting
        `define ERASE_RANDOM_DIST       0   //0 indicate ev will always pass
        `define ERASE_TO_EV_DIST        7
        `define ERASE_TO_OEV1_DIST      1
        `define ERASE_TO_OEV2_DIST      1
    `elsif SP1_TIMEOUT
        `define FLASH_SP1_INC           0  //40 indicate the sp1 will always fail
        `define FLASH_SP2_INC           0  //3 indicate the sp2 will always fail
        //random dist setting
        `define ERASE_RANDOM_DIST       0  //0 indicate ev will always pass
        `define ERASE_TO_EV_DIST        0
        `define ERASE_TO_OEV1_DIST      1
        `define ERASE_TO_OEV2_DIST      0
    `elsif SP2_TIMEOUT
        `define FLASH_SP1_INC           0  //40 indicate the sp1 will always fail
        `define FLASH_SP2_INC           0  //3 indicate the sp2 will always fail
        //random dist setting
        `define ERASE_RANDOM_DIST       0   //0 indicate ev will always pass
        `define ERASE_TO_EV_DIST        0
        `define ERASE_TO_OEV1_DIST      0
        `define ERASE_TO_OEV2_DIST      1
    `elsif ERASE_TIMEOUT
        `define FLASH_SP1_INC           0  //40 indicate the sp1 will always fail
        `define FLASH_SP2_INC           0  //3 indicate the sp2 will always fail
        //random dist setting
        `define ERASE_RANDOM_DIST       1   //0 indicate ev will always pass
        `define ERASE_TO_EV_DIST        0
        `define ERASE_TO_OEV1_DIST      0
        `define ERASE_TO_OEV2_DIST      0
    `else
        `define FLASH_SP1_INC           10  //40 indicate the sp1 will always pass 
        `define FLASH_SP2_INC           3   //3 indicate the sp2 will always pass 
        //random dist setting
        `define ERASE_RANDOM_DIST       1   //0 indicate ev will always pass
        `define ERASE_TO_EV_DIST        7
        `define ERASE_TO_OEV1_DIST      1
        `define ERASE_TO_OEV2_DIST      1
    `endif

    `define SFDP_REG_ADDR           2'b00
    `define SECURITY_R1_ADDR        2'b01
    `define SECURITY_R2_ADDR        2'b10
    `define SECURITY_R3_ADDR        2'b11
    


`endif
