`ifndef __GLOBAL_CONFIG_HEADER__
    `define __GLOBAL_CONFIG_HEADER__    
    //1.Active level setting
    `define ACTIVE_LOW
    `ifdef ACTIVE_HIGH
        `define POSITIVE_RESET
            `define RESET_EDGE      posedge
            `define RESET_ENABLE    1'b1
            `define RESET_DISABLE   1'b0
        `define POSITIVE_MEMORY
            `define MEM_ENABLE      1'b1
            `define MEM_DISABLE     1'b0
    `elsif ACTIVE_LOW
        `define NEGATIVE_RESET
            `define RESET_EDGE      negedge
            `define RESET_ENABLE    1'b0
            `define RESET_DISABLE   1'b1
        `define NEGATIVE_MEMORY
            `define MEM_ENABLE      1'b0
            `define MEM_DISABLE     1'b1
    `endif

    //io select 
    `define IMPLEMENT_SPI
    `define IMPLEMENT_PWM
    `define IMPLEMENT_D2A

    //3.Report sensitivity setting 
    //----RPT_DEBUG  : All level display information will be printed, the debug level info is in the spi behavioral model .
    //----RPT_HIGH   : Print the driver level info include MEDIAN and LOW
    //----RPT_MEDIAN : Print the testcase/program level info include LOW
    //----RPT_LOW    : Only print the scoreboard level info or critical warning info in the testbench,include the error.
    `ifdef RPT_DEBUG
        `define RPT_HIGH
        `define RPT_MEDIAN
        `define RPT_LOW
    `elsif RPT_HIGH
        `define RPT_MEDIAN
        `define RPT_LOW
    `elsif RPT_MEDIAN
        `define RPT_LOW
    `endif

    //`define RUN_TIMES 16
    `define TB_TIMEOUT 40'd100_000_000_000

`endif
