
package gt_std_class;


class gt_display;

    task display_s(input bit en=1,input string x,input int y=-1,input int z=-1);
        if(en) begin
            if(y!=-1)                   $display(x,y);
            else if((y!=-1) & (z!=-1))  $display(x,y,z);
            else                        $display(x);
        end
    endtask

endclass



endpackage 
