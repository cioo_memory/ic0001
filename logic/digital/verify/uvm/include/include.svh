`include "define_product.svh"
`include "define_interface.svh"
`include "define_path.svh"
`include "sram.svh"

//`include "flash_setting.svh"
`include "flash.svh"
`include "global_config.svh"
`include "stddef.svh"
`include "spi.svh"

//analog include
`include "analog_power.svh"
`include "analog_pad.svh"
`include "analog_osc.svh"
