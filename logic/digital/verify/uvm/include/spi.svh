`ifndef __SPI_HEADER__
    `define __SPI_HEADER__

    `define SR0                         0
    `define SR1                         1

    //spi timing
    `define tSPI_READY                  10
    `define SPI_FREQ                    104
    `define tCLK                        (1000/(`SPI_FREQ*1.00))
    `define tCL                         (`tCLK/2)
    `define tCH                         (`tCLK -`tCL)
    //`define tCL                        5.615//(`tCLK/2)
    //`define tCH                        (`tCLK -`tCL)
    //`define tCH                         5.615//(`tCLK/2)
    //`define tCL                         (`tCLK -`tCH)
    `define tCHDX                       3
    `define tSLCH                       5
    `define tCHSL                       5
    `define tDVCH                       2
    `define tCHSH                       5
    `define tSHCH                       (`tCL+10) 
    `define tSHSL_MIN                   15
    `define tSHSL_MAX                   30
    `define tSHQZ                       6
    `define tCLQV                       6
    `define tHLCH                       0
    `define tCHHH                       5
    `define tHHCH                       5
    `define tCHHL                       5
    `define tHHQX                       6
    `define tHLQZ                       6
    `define tWHSL                       20
    `define tSHWL                       100
    `define tDP                         3000
    `define tRES                        8
    `define tSUS                        0
    `define tRST                        30000
    `define tW                          1000000
    `define tBP                         15000
    `define tPP                         400000
    `define tSE                         30000000
    `define tBE1                        120000000
    `define tBE2                        150000000
    `define tCE                         1000000000

    `define SPI_MODE_0                  0
    `define SPI_MODE_3                  1
    //spi command
    `define SPI_WREN                    8'h06
    `define SPI_WRDI                    8'h04
    `define SPI_WREN_VSR                8'h50
    `define SPI_RDSR_LB                 8'h05
    `define SPI_RDSR_HB                 8'h35
    `define SPI_WRSR                    8'h01
    `define SPI_READ                    8'h03
    `define SPI_FAST_READ               8'h0B
    `define SPI_DUAL_OUTPUT_FAST_READ   8'h3B
    `define SPI_DUAL_IO_FAST_READ       8'hBB
    `define SPI_QUAD_OUTPUT_FAST_READ   8'h6B
    `define SPI_QUAD_IO_FAST_READ       8'hEB
    `define SPI_SET_BURST_WITH_WRAP     8'h77
    `define SPI_PAGE_PROGRAM            8'h02
    `define SPI_QUAD_PAGE_PROGRAM       8'h32
    `define SPI_SECTOR_ERASE            8'h20
    `define SPI_32K_BLOCK_ERASE         8'h52
    `define SPI_64K_BLOCK_ERASE         8'hD8
    `define SPI_CHIP_ERASE_0            8'hC7
    `define SPI_CHIP_ERASE_1            8'h60
    `define SPI_ENABLE_RESET            8'h66
    `define SPI_RESET                   8'h99
    `define SPI_SET_BURST_WITH_WRAP     8'h77
    `define SPI_ESRY                    8'h70
    `define SPI_DSRY                    8'h80
    `define SPI_DP                      8'hB9
    `define SPI_DP_RELEASE              8'hAB
    `define SPI_RDI                     8'hAB
    `define SPI_REMS                    8'h90
    `define SPI_DIO_REMS                8'h92
    `define SPI_QIO_REMS                8'h94
    `define SPI_RDID                    8'h9F
    `define SPI_RD_UID                  8'h4B
    `define SPI_PES                     8'h75
    `define SPI_PER                     8'h7A
    `define SPI_ERASE_SEC_REG           8'h44
    `define SPI_PROG_SEC_REG            8'h42
    `define SPI_READ_SEC_REG            8'h48
    `define SPI_READ_SFDP               8'h5A

    // test command
    `define TM_OTP1_ERASE               8'h20
    `define TM_OTP1_PROGRAM             8'h02
    `define TM_OTP1_READ                8'h0a
    `define TM_OTP2_ERASE               8'h21
    `define TM_OTP2_PROGRAM             8'h12
    `define TM_OTP2_READ                8'h1a

    `define TM_SRAM_WRITE               8'hFD
    `define TM_SRAM_READ                8'hFC
 
    //`define STATUS_REG_WR   '{
    //    "REG_RO"  ,
    //    "REG_RW"  ,
    //    "REG_W1"  ,
    //    "REG_W1"  ,
    //    "REG_W1"  ,
    //    "REG_W1"  ,
    //    "REG_RW"  ,
    //    "REG_W1"  ,
    //    "REG_RW"  ,
    //    "REG_RW"  ,
    //    "REG_RW"  ,
    //    "REG_RW"  ,
    //    "REG_RW"  ,
    //    "REG_RW"  ,
    //    "REG_RO"  ,
    //    "REG_RO"  
    //}


`endif
