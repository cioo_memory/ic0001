`ifndef __SRAM_HEADER__
    `define __SRAM_HEADER__

    `define SRAM_ADDR_WIDTH     8
    `define SRAM_DATA_WIDTH     8

    `define SRAM_TIMING_CHECK   1
    `define SRAM_NTFY           1


    `define tTR1                1
    `define tTR2                10
    `define tTR3                10
    `define tTR4                20
    `define tTR5                10
    `define tTR6                20
    `define tTR7                50



`endif
