`ifndef __STDDEF_HEADER__
    `define __STDDEF_HEADER__
    `define LATCH               1'b1
    `define TOGGLE              1'b0
    `define HIGH                1'b1
    `define LOW                 1'b0
    `define ENABLE              1'b1
    `define DISABLE             1'b0
    `define ENABLE_N            1'b0
    `define DISABLE_N           1'b1
    `define READ                1'b1
    `define WRITE               1'b0
    `define LSB                 1'b0
    `define BYTE_DATA_W         8
    `define BYTE_MSB            7
    `define ByteDataBus         07:00
    `define WORD_DATA_W         16
    `define WORD_MSB            15
    `define WordDataBus         15:00
    `define DWORD_DATA_W        32
    `define DWORD_MSB           31
    `define DwordDataBus        31:00
    `define WORD_ADDR_W         30
    `define WORD_ADDR_MSB       29
    `define WordAddrBus         29:00
    `define BYTE_OFFSET_W       2
    `define ByteOffsetBus       01:00
    `define WordAddrLoc         31:02
    `define ByteOffsetLoc       01:00
    `define BYTE_OFFSET_WORD    2'b00    

    `define max(a,b) {(a) > (b) ? (a) : (b)}
    `define min(a,b) {(a) < (b) ? (a) : (b)}
    `define abs(a,b) {`max(a,b) - `min(a,b)}

    `define SYS_CLK_PERIOD      125

    `define tINVALID            `SYS_CLK_PERIOD*3
    `define tDLY                `SYS_CLK_PERIOD*5
    `define tDLY_STATE          `SYS_CLK_PERIOD     //state switch delay : condition to switch done

    `define tDLY_SLEEP_EXIT     15000
    `define tDLY_OPERATING_ENTER    20000
    //
    `define FINISH #2000 $finish

    //`define display_1(message) `ifdef RPT_MEDIAN $display(message)
    `define display_e(en,message,x) if(en) $display(message,x)

    `define INTERFACE_IIC_EEPROM    3'b000
    `define INTERFACE_SPI_EEPROM    3'b001
    `define INTERFACE_ALL_EEPROM    3'b111

    `define INTERFACE_1_WIRE_SPI_FLASH    3'b000
    `define INTERFACE_2_WIRE_SPI_FLASH    3'b001
    `define INTERFACE_4_WIRE_SPI_FLASH    3'b010



    //AGENT
    `define AGT_ACTIVE      1
    `define AGT_PASSIVE     0

`endif

