//****************************************************************************************************  
//---------------Copyright (c) 2019 CIOO@Gitee | wangboworks@126.com All rights reserved--------------
//**************************************************************************************************** 
//File Information
//**************************************************************************************************** 
//File Name      : tb_dump_config.sv 
//Project Name   : ic0001
//Description    : the testbench configure module : dump file.
//VC Address     : https://gitee.com/cioo_memory/ic0001
//License        : MuLanPSL-1.0
//**************************************************************************************************** 
//Version Information
//**************************************************************************************************** 
//Create Date    : 2019-10-25 14:50
//First Author   : FPGA1988
//Last Modify    : 2019-10-25 17:29
//Last Author    : FPGA1988
//**************************************************************************************************** 
//Change History(latest change first)
//yyyy.mm.dd - Author - Your log of change
//**************************************************************************************************** 
//2019.10.25 - FPGA1988 - The first version.
//*---------------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------------
//3 the wave dump
//------------------------------------------------------------------------------------------------
`ifdef FSDB_GEN
    initial begin
        $fsdbAutoSwitchDumpfile(500,"test.fsdb",100);
        $fsdbDumpflush;
        `ifndef FPGA
            $fsdbDumpvars(0,TESTBENCH.dut);
            $fsdbDumpvars(0,TESTBENCH.sys_if);
        `else
            $fsdbDumpvars(0,TESTBENCH.dut);
            `ifndef POSTFPGA
            $fsdbDumpvars(0,TESTBENCH.dut.digital_top_inst);
            `endif
        `endif
        //$fsdbDumpSVA;
    end
`endif

parameter VCD_DUMP_TSTART = 0;
parameter VCD_DUMP_TSTOP  = 0;

`ifdef VCD_GEN 
    initial begin
        $dumpfile("test.vcd");
        $dumpvars(0,TESTBENCH.dut.digital_top_inst);
        $dumpflush;
    end

    initial begin
        $display("Dump VCD wave....\n");
        #0;
        //$dumpoff;
        #VCD_DUMP_TSTART;
        $dumpon;
        //#(VCD_DUMP_TSTOP - VCD_DUMP_TSTART);
        //$dumpoff;
    end
`endif
