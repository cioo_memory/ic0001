//****************************************************************************************************  
//---------------Copyright (c) 2019 CIOO@Gitee | wangboworks@126.com All rights reserved--------------
//**************************************************************************************************** 
//File Information
//**************************************************************************************************** 
//File Name      : tb_hsim_config.sv 
//Project Name   : ic0001
//Description    : the testbench configure module : hsim file.
//VC Address     : https://gitee.com/cioo_memory/ic0001
//License        : MuLanPSL-1.0
//**************************************************************************************************** 
//Version Information
//**************************************************************************************************** 
//Create Date    : 2019-10-25 14:50
//First Author   : FPGA1988
//Last Modify    : 2019-10-25 17:29
//Last Author    : FPGA1988
//**************************************************************************************************** 
//Change History(latest change first)
//yyyy.mm.dd - Author - Your log of change
//**************************************************************************************************** 
//2019.10.25 - FPGA1988 - The first version.
//*---------------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------------
//4 generate hsim vector
//------------------------------------------------------------------------------------------------
parameter time_samp_start = 10000; 
parameter time_samp_end   = 10000000;  
reg sample_flag;

initial
begin
   sample_flag = 0 ;
   #(time_samp_start);	
   //wait(`digital.por_cfg_done);
   sample_flag = 1;
end   

integer fp_flash,fp_hsp;    
initial
    begin
    `ifdef hsim
        fp_flash = $fopen("./hsim.vec","w+");
        $fdisplay(fp_flash,"radix  1 1 1 1 1 1\n");  
        $fdisplay(fp_flash,"io     i i i i i i\n"); 
        $fdisplay(fp_flash,"vname  SCLK_PAD CS_PAD DI_PAD DO_PAD WP_PAD HOLD_PAD");
        $fdisplay(fp_flash,"period 1" );  
        $fdisplay(fp_flash,"TUNIT 1ns");
        $fdisplay(fp_flash,"vih simvih");
        $fdisplay(fp_flash,"vil simvil");
        $fdisplay(fp_flash,"delay tdelay");
        $fdisplay(fp_flash,"tskip");
        $fdisplay(fp_flash,"\n");
    `endif
end

reg         vect_clk        ;
reg         vect_clk_hsp    ;

initial vect_clk <= 0;
always 
     #1 vect_clk <= ~vect_clk;

`ifdef hsim
always @(vect_clk) begin
    if(sample_flag)	  
        $fdisplay(fp_flash,"%10d %b %b %b %b %b %b",$time,sys_if.sclk,sys_if.cs_n,sys_if.io0_si_i,sys_if.io1_so_i,sys_if.io2_wp_n_i,sys_if.io3_hold_n_i);
end
`endif
