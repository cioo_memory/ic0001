//****************************************************************************************************  
//---------------Copyright (c) 2019 CIOO@Gitee | wangboworks@126.com All rights reserved--------------
//**************************************************************************************************** 
//File Information
//**************************************************************************************************** 
//File Name      : tb_sdf_config.sv 
//Project Name   : ic0001
//Description    : the testbench configure module : sdf.
//VC Address     : https://gitee.com/cioo_memory/ic0001
//License        : MuLanPSL-1.0
//**************************************************************************************************** 
//Version Information
//**************************************************************************************************** 
//Create Date    : 2019-10-25 14:50
//First Author   : FPGA1988
//Last Modify    : 2019-10-25 17:29
//Last Author    : FPGA1988
//**************************************************************************************************** 
//Change History(latest change first)
//yyyy.mm.dd - Author - Your log of change
//**************************************************************************************************** 
//2019.10.25 - FPGA1988 - The first version.
//*---------------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------------
//2 the sdf file annotation
//------------------------------------------------------------------------------------------------

`ifdef POSTMAX
    initial begin
        $sdf_annotate(`SDF_MAX_FILE,dut.digital_top_inst,,,"MAXIMUM");
    end
`elsif POSTTYP
    initial begin
        $sdf_annotate(`SDF_TYP_FILE,dut.digital_top_inst,,,"TYPICAL");
    end
`elsif POSTMIN
    initial begin
        $sdf_annotate(`SDF_MIN_FILE,dut.digital_top_inst,,,"MINIMUM");
    end
`elsif POSTFPGA
    initial begin
        $sdf_annotate(`FPGA_SDF_FILE,dut,,,"MAXIMUM");
    end
`endif
