//****************************************************************************************************
//---------------Copyright (c) 2019 CIOO@Gitee | wangboworks@126.com All rights reserved--------------
//****************************************************************************************************
//File Information
//****************************************************************************************************
//File Name      : Environment.v
//Project Name   : ic0001
//Description    : the chip behavioral model : chip top.
//VC Address     : https://gitee.com/cioo_memory/ic0001
//License        : MuLanPSL-1.0
//****************************************************************************************************
//Version Information
//****************************************************************************************************
//Create Date    : 2019-10-14 09:10
//First Author   : FPGA1988
//Last Modify    : 2019-10-14 16:30
//Last Author    : FPGA1988
//****************************************************************************************************
//Change History(latest change first)
//yyyy.mm.dd - Author - Your log of change
//****************************************************************************************************
//2019.10.14 - FPGA1988 - Initial verison : create basic hierarchy.
//----------------------------------------------------------------------------------------------------
`include "include.svh"

module chip_top(
    sys_if sys_if
);


    //************************************************************************************************
    // 1.Sub module instantiation
    //************************************************************************************************
    //------------------------------------------------------------------------------------------------
    // 1.1 the analog between digital interface instance
    //------------------------------------------------------------------------------------------------
    ad_if ad_if(.*);
    //------------------------------------------------------------------------------------------------
    // 1.2 the analog top : pad + power
    //------------------------------------------------------------------------------------------------
    cioo_analog_top cioo_analog_top_inst(
        .sys_if             (sys_if                         ),//xx  System interface
        .ad_if              (ad_if                          ) //xx  Analog between Digital IO
    );

    //------------------------------------------------------------------------------------------------
    // 1.3 the digital top
    //------------------------------------------------------------------------------------------------
    DIGITAL_TOP digital_top_inst(
        .osc_clk                (ad_if.osc_clk              ),//01  In
        .osc_en                 (ad_if.osc_en               ),//01  Out
        .por_rst_n              (ad_if.por_rst_n            ),//1   In
        .por_cfg_done           (ad_if.por_cfg_done         ),//01  Out
        .pad_sclk_in            (ad_if.pad_sclk_in          ),//01  In
        .pad_cs_in              (ad_if.pad_cs_in            ),//01  In
        .pad_si_in              (ad_if.pad_si_in            ),//01  In
        .pad_so_in              (ad_if.pad_so_in            ),//01  In
        .pad_wp_in              (ad_if.pad_wp_in            ),//01  In
        .pad_hold_in            (ad_if.pad_hold_in          ),//01  In
        .pad_out_en             (ad_if.pad_out_en           ),//01  Out
        .pad_dual_en            (ad_if.pad_dual_en          ),//01  Out
        .pad_quad_en            (ad_if.pad_quad_en          ),//01  Out
        .pad_si_out             (ad_if.pad_si_out           ),//01  Out
        .pad_so_out             (ad_if.pad_so_out           ),//01  Out
        .pad_wp_out             (ad_if.pad_wp_out           ),//01  Out
        .pad_hold_out           (ad_if.pad_hold_out         ),//01  Out

        .flh_pump_01_en         (ad_if.flh_pump_01_en       ),//01  Out
        .flh_pump_02_en         (ad_if.flh_pump_02_en       ),//01  Out
        .flh_pump_03_en         (ad_if.flh_pump_03_en       ),//01  Out
        .flh_pump_04_en         (ad_if.flh_pump_04_en       ),//01  Out
        .flh_pump_01_rdy        (ad_if.flh_pump_01_rdy      ),//01  In
        .flh_pump_02_rdy        (ad_if.flh_pump_02_rdy      ),//01  In
        .flh_pump_03_rdy        (ad_if.flh_pump_03_rdy      ),//01  In
        .flh_pump_04_rdy        (ad_if.flh_pump_04_rdy      ),//01  In

        .flh_sab_en             (ad_if.flh_sab_en           ),//01  Out
        .flh_rd_clk             (ad_if.flh_rd_clk           ),//01  Out
        .flh_sa_en              (ad_if.flh_sa_en            ),//01  Out
        .flh_data_in            (ad_if.flh_data_in          ),//32  Out
        .flh_data_out           (ad_if.flh_data_out         ),//32  In
        .flh_addr               (ad_if.flh_addr             ),//19  Out
        .flh_pgm_start          (ad_if.flh_pgm_start        ),//01  Out
        .flh_pgm_en             (ad_if.flh_pgm_en           ),//01  Out
        .flh_pgm_pulse          (ad_if.flh_pgm_pulse        ),//01  Out
        .flh_pv_en              (ad_if.flh_pv_en            ),//01  Out
        .flh_pgm_st             (ad_if.flh_pgm_st           ),//02  In
        .flh_ers_start          (ad_if.flh_ers_start        ),//01  Out
        .flh_ers_en             (ad_if.flh_ers_en           ),//01  Out
        .flh_ers_pulse          (ad_if.flh_ers_pulse        ),//01  Out
        .flh_ev1_en             (ad_if.flh_ev1_en           ),//01  Out
        .flh_ev2_en             (ad_if.flh_ev2_en           ),//01  Out
        .flh_ers_st             (ad_if.flh_ers_st           ),//02  In

        .flh_ers_tune1_en       (ad_if.flh_ers_tune1_en     ),//01  Out
        .flh_ers_tune2_en       (ad_if.flh_ers_tune2_en     ),//01  Out
        .flh_ers_tune1_pulse    (ad_if.flh_ers_tune1_pulse  ),//01  Out
        .flh_ers_tune2_pulse    (ad_if.flh_ers_tune2_pulse  ),//01  Out
        .flh_blk_ers_en         (ad_if.flh_blk_ers_en       ),//01  Out
        .flh_sec_ers_en         (ad_if.flh_sec_ers_en       ),//01  Out
        .flh_ers_64k_en         (ad_if.flh_ers_64k_en       ),//01  Out
        .flh_busy               (ad_if.flh_busy             ),//01  Out
        .sram_clk               (ad_if.sram_clk             ),//01  Out
        .sram_addr              (ad_if.sram_addr            ),//01  Out
        .sram_wr_en             (ad_if.sram_wr_en           ),//01  Out
        .sram_rd_en             (ad_if.sram_rd_en           ),//01  Out
        .sram_data_in           (ad_if.sram_data_in         ),//08  Out
        .sram_data_out          (ad_if.sram_data_out        ),//08  In
        .dpd_mode_en            (ad_if.dpd_mode_en          ),//01  Out
        .stb_mode_en            (ad_if.stb_mode_en          ),//01  Out
        .test_mode_en           (ad_if.test_mode_en         ),//01  Out
        .tm_flag_01             (                               ),//01  Out
        .tm_flag_02             (                               ),//01  Out
        .tm_flag_03             (                               ),//01  Out
        .tm_flag_04             (                               ),//01  Out
        .tm_flag_05             (                               ),//01  Out
        .tm_flag_06             (                               ) //01  Out

    );
    //------------------------------------------------------------------------------------------------
    // 1.4 the cioo flash model
    //------------------------------------------------------------------------------------------------
    cioo_flash_model cioo_flash_model(
        .flh_pump_01_en         (ad_if.flh_pump_01_en       ),//01  Out
        .flh_pump_02_en         (ad_if.flh_pump_02_en       ),//01  Out
        .flh_pump_03_en         (ad_if.flh_pump_03_en       ),//01  Out
        .flh_pump_04_en         (ad_if.flh_pump_04_en       ),//01  Out
        .flh_pump_01_rdy        (ad_if.flh_pump_01_rdy      ),//01  In
        .flh_pump_02_rdy        (ad_if.flh_pump_02_rdy      ),//01  In
        .flh_pump_03_rdy        (ad_if.flh_pump_03_rdy      ),//01  In
        .flh_pump_04_rdy        (ad_if.flh_pump_04_rdy      ),//01  In

        .flh_sab_en             (ad_if.flh_sab_en           ),//01  Out
        .flh_rd_clk             (ad_if.flh_rd_clk           ),//01  Out
        .flh_sa_en              (ad_if.flh_sa_en            ),//01  Out
        .flh_data_in            (ad_if.flh_data_in          ),//32  Out
        .flh_data_out           (ad_if.flh_data_out         ),//32  In
        .flh_addr               (ad_if.flh_addr             ),//19  Out
        .flh_pgm_start          (ad_if.flh_pgm_start        ),//01  Out
        .flh_pgm_en             (ad_if.flh_pgm_en           ),//01  Out
        .flh_pgm_pulse          (ad_if.flh_pgm_pulse        ),//01  Out
        .flh_pv_en              (ad_if.flh_pv_en            ),//01  Out
        .flh_pgm_st             (ad_if.flh_pgm_st           ),//02  In
        .flh_ers_start          (ad_if.flh_ers_start        ),//01  Out
        .flh_ers_en             (ad_if.flh_ers_en           ),//01  Out
        .flh_ers_pulse          (ad_if.flh_ers_pulse        ),//01  Out
        .flh_ev1_en             (ad_if.flh_ev1_en           ),//01  Out
        .flh_ev2_en             (ad_if.flh_ev2_en           ),//01  Out
        .flh_ers_st             (ad_if.flh_ers_st           ),//02  In

        .flh_ers_tune1_en       (ad_if.flh_ers_tune1_en     ),//01  Out
        .flh_ers_tune2_en       (ad_if.flh_ers_tune2_en     ),//01  Out
        .flh_ers_tune1_pulse    (ad_if.flh_ers_tune1_pulse  ),//01  Out
        .flh_ers_tune2_pulse    (ad_if.flh_ers_tune2_pulse  ),//01  Out
        .flh_blk_ers_en         (ad_if.flh_blk_ers_en       ),//01  Out
        .flh_sec_ers_en         (ad_if.flh_sec_ers_en       ),//01  Out
        .flh_ers_64k_en         (ad_if.flh_ers_64k_en       ),//01  Out
        .flh_busy               (ad_if.flh_busy             ) //01  In
    );

    //------------------------------------------------------------------------------------------------
    // 1.5 the sram model
    //------------------------------------------------------------------------------------------------
    cioo_sram_model cioo_sram_model(
        .sram_clk           (ad_if.sram_clk                 ),//01  In
        .sram_addr          (ad_if.sram_addr                ),//01  In
        .sram_wr_en         (ad_if.sram_wr_en               ),//01  In
        .sram_rd_en         (ad_if.sram_rd_en               ),//01  In
        .sram_data_in       (ad_if.sram_data_in             ),//08  In
        .sram_data_out      (ad_if.sram_data_out            ) //08  Out
    );


endmodule
