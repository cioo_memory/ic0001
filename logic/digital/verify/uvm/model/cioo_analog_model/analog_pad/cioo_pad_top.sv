//****************************************************************************************************  
//---------------Copyright (c) 2019 CIOO@Gitee | wangboworks@126.com All rights reserved--------------
//**************************************************************************************************** 
//File Information
//**************************************************************************************************** 
//File Name      : cioo_pad_model.sv 
//Project Name   : ic0001
//Description    : the top of pad model.
//VC Address     : https://gitee.com/cioo_memory/ic0001
//License        : MuLanPSL-1.0
//**************************************************************************************************** 
//Version Information
//**************************************************************************************************** 
//Create Date    : 2019-02-25 10:50
//First Author   : FPGA1988
//Last Modify    : 2019-08-07 15:30
//Last Author    : FPGA1988
//**************************************************************************************************** 
//Change History(latest change first)
//yyyy.mm.dd - Author - Your log of change
//**************************************************************************************************** 
//2019.08.07 - FPGA1988 - Interface update.
//2019.02.25 - FPGA1988 - The first version.
//*---------------------------------------------------------------------------------------------------
//File Include : system header file
`include "include.svh"

module cioo_pad_top(
    sys_if          sys_if  ,
    ad_if           ad_if   
);

    `ifdef PRODUCT_EEPROM
        pad_model_eeprom pad_model_eeprom(
            .iic_if         (sys_if.iic_if ), 
            .ad_if          (ad_if  ) 
        );
    `elsif PRODUCT_FLASH
        pad_model_flash pad_model_flash(
            .spi_if_dut     (sys_if.spi_if_dut ), 
            .ad_if          (ad_if  )
        ); 
    `endif

endmodule    
//****************************************************************************************************
//End of Module
//****************************************************************************************************
