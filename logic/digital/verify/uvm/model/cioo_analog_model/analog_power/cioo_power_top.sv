//****************************************************************************************************  
//*---------------Copyright (c) 2017 Giantec Digital Logic Group. All rights reserved-----------------
//
//                   --              It to be defined               --
//                   --                    ...                      --
//                   --                    ...                      --
//                   --                    ...                      --
//**************************************************************************************************** 
//File Information
//**************************************************************************************************** 
//File Name      : gt_power_top.sv 
//Project Type   : All
//Description    : the power model.
//VC Address     : None[Version Control System = SOS]
//License        : None
//**************************************************************************************************** 
//Version Information
//**************************************************************************************************** 
//Create Date    : 2017-01-20 10:20
//First Author   : FPGA1988
//Last Modify    : 2018-09-25 14:30
//Last Author    : FPGA1988
//**************************************************************************************************** 
//Change History(latest change first)
//yyyy.mm.dd - Author - Your log of change
//**************************************************************************************************** 
//2018.09.25 - FPGA1988 - Add the power check logic : stb and dpd.
//2018.09.12 - FPGA1988 - Add the auto trim logic.
//2018.09.12 - FPGA1988 - Add the auto_trim_done and related ports.
//2018.06.19 - FPGA1988 - Add the bgr behavior.
//2018.06.19 - FPGA1988 - The first version.
//*---------------------------------------------------------------------------------------------------
//File Include : system header file
`include "include.svh"

module cioo_power_top(
    sys_if sys_if,
    ad_if ad_if
);


`ifdef PRODUCT_EEPROM
    cioo_power_eeprom gt_power_eeprom(.ad_if(ad_if));
`elsif PRODUCT_FLASH
    cioo_power_flash gt_power_flash(.ad_if(ad_if));
`endif

endmodule    
//****************************************************************************************************
//End of Module
//****************************************************************************************************
