//****************************************************************************************************  
//---------------Copyright (c) 2019 CIOO@Gitee | wangboworks@126.com All rights reserved--------------
//**************************************************************************************************** 
//File Information
//**************************************************************************************************** 
//File Name      : flash_bhv_model.sv
//Project Name   : ic0001
//Description    : FLASH behavioral model.
//VC Address     : https://gitee.com/cioo_memory/ic0001
//License        : MuLanPSL-1.0
//**************************************************************************************************** 
//Version Information
//**************************************************************************************************** 
//Create Date    : 2019-10-12 14:10
//First Author   : FPGA1988
//Last Modify    : 2019-11-19 17:00
//Last Author    : FPGA1988
//**************************************************************************************************** 
//Change History(latest change first)
//yyyy.mm.dd - Author - Your log of change
//**************************************************************************************************** 
//2019.11.19 - FPGA1988 - Basic read + pgm + erase logic are added.
//2019.10.24 - FPGA1988 - First version.
//---------------------------------------------------------------------------------------

`include "include.svh"
`include "flash_config.sv"

module cioo_flash_model(
    input   logic                   flh_pump_01_en          ,     
    input   logic                   flh_pump_02_en          , 
    input   logic                   flh_pump_03_en          , 
    input   logic                   flh_pump_04_en          , 
    output  logic                   flh_pump_01_rdy         , 
    output  logic                   flh_pump_02_rdy         , 
    output  logic                   flh_pump_03_rdy         , 
    output  logic                   flh_pump_04_rdy         , 
    input   logic                   flh_sab_en              , 
    input   logic                   flh_rd_clk              , 
    input   logic                   flh_sa_en               , 
    input   logic   [31:00]         flh_data_in             , 
    output  logic   [31:00]         flh_data_out            , 
    input   logic   [23:00]         flh_addr                , 
    input   logic                   flh_pgm_start           , 
    input   logic                   flh_pgm_en              , 
    input   logic                   flh_pgm_pulse           , 
    input   logic                   flh_pv_en               , 
    output  logic   [01:00]         flh_pgm_st              , 
    input   logic                   flh_ers_start           , 
    input   logic                   flh_ers_en              , 
    input   logic                   flh_ers_pulse           , 
    input   logic                   flh_ev1_en              , 
    input   logic                   flh_ev2_en              , 
    output  logic   [01:00]         flh_ers_st              , 
    input   logic                   flh_ers_tune1_en        , 
    input   logic                   flh_ers_tune2_en        , 
    input   logic                   flh_ers_tune1_pulse     ,
    input   logic                   flh_ers_tune2_pulse     ,
    input   logic                   flh_blk_ers_en          , 
    input   logic                   flh_sec_ers_en          , 
    input   logic                   flh_ers_64k_en          , 
    input   logic                   flh_busy           

);

    // ************************************************************************************
    // 1.parameter and constant definition
    // ************************************************************************************
    parameter tP1RDY = 3000;
    parameter tP2RDY = 3000;
    parameter tP3RDY = 3000;
    parameter tP4RDY = 3000;
    parameter tPDIS  = 50;


    // ************************************************************************************
    // 2.registers and wire declaration
    // ************************************************************************************

    // ------------------------------------------------------------------------------------
    // 2.1 The memory define
    // ------------------------------------------------------------------------------------
    bit     [31:0]          flh_array_dig   [`FLASH_ALL_WORD_SIZE-1:0];//512K = 512*1024 Bytes = 512K/4 Words

    // ------------------------------------------------------------------------------------
    // 2.2 The power and enable signal
    // ------------------------------------------------------------------------------------
    wire    #(tP1RDY,tPDIS) flh_pump_01_delay   ;
    wire    #(tP2RDY,tPDIS) flh_pump_02_delay   ;
    wire    #(tP3RDY,tPDIS) flh_pump_03_delay   ;
    wire    #(tP4RDY,tPDIS) flh_pump_04_delay   ;

    logic   [21:00]         flh_word_addr       ;
    bit     [31:00]         read_buf_01         ;
    bit     [31:00]         read_buf_02         ;
    bit     [31:00]         pv_buf_01           ;
    bit     [01:00]         pv_buf_02           ;
    bit     [31:00]         ev_buf_01           ;
    bit     [01:00]         ev_buf_02           ;

    // ************************************************************************************
    // 3.main code
    // ************************************************************************************
    // ------------------------------------------------------------------------------------
    // 3.1 The pump switch
    // ------------------------------------------------------------------------------------
    assign flh_pump_01_delay = flh_pump_01_en;
    assign flh_pump_02_delay = flh_pump_02_en;
    assign flh_pump_03_delay = flh_pump_03_en;
    assign flh_pump_04_delay = flh_pump_04_en;

    assign flh_pump_01_rdy = flh_pump_01_delay;
    assign flh_pump_02_rdy = flh_pump_02_delay;
    assign flh_pump_03_rdy = flh_pump_03_delay;
    assign flh_pump_04_rdy = flh_pump_04_delay;


    assign flh_word_addr = flh_addr[21:00];



    // ------------------------------------------------------------------------------------
    // 3.1 Flash read
    // ------------------------------------------------------------------------------------
    always @(posedge flh_rd_clk) begin
        if(flh_sab_en & flh_pump_01_rdy)
            read_buf_01 <= flh_array_dig[flh_word_addr];
    end
    always @(negedge flh_sa_en) begin
        if(flh_sab_en & flh_pump_01_rdy)
            read_buf_02 <= read_buf_01;
    end

    assign flh_data_out = read_buf_02;

    // ------------------------------------------------------------------------------------
    // 3.3 Program verify
    // ------------------------------------------------------------------------------------
    always @(posedge flh_rd_clk) begin
        if(flh_sab_en & flh_pump_01_rdy & flh_pv_en)
            pv_buf_01 <= flh_data_in ^ flh_array_dig[flh_word_addr];
    end

    always @(posedge flh_sa_en) begin
        if(flh_sab_en & flh_pump_01_rdy & flh_pv_en) begin
            if(&pv_buf_01)
                pv_buf_02 <= 2'b11; //
            else if(|pv_buf_01)
                pv_buf_02 <= 2'b00; //
            else begin
                randcase
                    1 : pv_buf_02 <= 2'b01;
                    1 : pv_buf_02 <= 2'b10;
                endcase
            end
        end
    end

    always @(negedge flh_sa_en) begin
        if(flh_sab_en & flh_pump_01_rdy & flh_pv_en)
            flh_pgm_st <= pv_buf_02;
    end


    // ------------------------------------------------------------------------------------
    // 3.4 Program and erase
    // ------------------------------------------------------------------------------------
    always @(flh_pgm_pulse,flh_ers_pulse)
        if(flh_pgm_start & flh_pump_02_rdy & flh_pgm_en)
            flh_array_dig[flh_word_addr]  <= flh_data_in;
        else if(flh_ers_start & flh_pump_04_rdy  & flh_ers_en)
            flh_array_dig[flh_word_addr]  <= 32'hffffffff;

    // ------------------------------------------------------------------------------------
    // 3.5 Erase verify
    // ------------------------------------------------------------------------------------
    always @(posedge flh_rd_clk) begin
        if(flh_sab_en & flh_pump_03_rdy & (flh_ev1_en | flh_ev2_en))
            ev_buf_01 <= 32'hffff_ffff;
    end

    always @(posedge flh_sa_en) begin
        if(flh_sab_en & flh_pump_03_rdy & (flh_ev1_en | flh_ev2_en)) begin
            if(&ev_buf_01)
                ev_buf_02 <= 2'b11; //
            else if(|ev_buf_01)
                ev_buf_02 <= 2'b00; //
            else begin
                randcase
                    1 : ev_buf_02 <= 2'b01;
                    1 : ev_buf_02 <= 2'b10;
                endcase
            end
        end
    end

    always @(negedge flh_sa_en) begin
        if(flh_sab_en & flh_pump_03_rdy & (flh_ev1_en | flh_ev2_en))
            flh_ers_st <= ev_buf_02;
    end

endmodule

// ************************************************************************************
// End of Module
// ************************************************************************************
