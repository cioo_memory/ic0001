    class flash_config;
        rand bit [5:0]  pst;
        constraint pst_dist {
            `ifndef PGM_TIMEOUT
                pst dist {1 := 10000,[1:8] :/ 900,[9:32] :/ 30,[33:63] :/ 0};
            `else
                pst dist {[33:63] :/ 100};
            `endif
        }

        /*
            erase_pulse = 0 ----> erase_pulse_1
            erase_pulse = 1 ----> erase_pulse_2

        */
        rand bit       erase_pulse;
        rand bit [7:0] erase_to_0;
        rand bit [7:0] erase_to_ev;
        rand bit [7:0] erase_random;

        constraint erase_level_dist {
            //1.1 0 
            erase_to_0[7] == 0;
            if(erase_pulse == 0)
                erase_to_0[6:0] dist {[1:30] :/ 20,[31:99] :/ 80};
            else
                erase_to_0[6:0] dist {[1:30] :/ 20,[31:99] :/ 80};
            //1.1 ev
            erase_to_ev[7:0] == 8'h80;
            //1.4 random 
            `ifdef ERASE_TIMEOUT
                erase_random[7:0] dist {1 := 5000,[2:9] :/ 50};
            `else
                if(erase_pulse == 0)
                    erase_random[7:0] dist {[0:99] :/ 50,[100:199] :/ 50};
                else
                    erase_random[7:0] dist {[0:99] :/ 50,[100:199] :/ 50};
            `endif
        }

    endclass
