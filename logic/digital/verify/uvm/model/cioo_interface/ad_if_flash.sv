//****************************************************************************************************  
//---------------Copyright (c) 2019 CIOO@Gitee | wangboworks@126.com All rights reserved--------------
//**************************************************************************************************** 
//File Information
//**************************************************************************************************** 
//File Name      : ad_if_flash.sv 
//Project Name   : ic0001
//Description    : the flash interface.
//VC Address     : https://gitee.com/cioo_memory/ic0001
//License        : MuLanPSL-1.0
//**************************************************************************************************** 
//Version Information
//**************************************************************************************************** 
//Create Date    : 2019-10-14 09:10
//First Author   : FPGA1988
//Last Modify    : 2019-10-14 16:30
//Last Author    : FPGA1988
//**************************************************************************************************** 
//Change History(latest change first)
//yyyy.mm.dd - Author - Your log of change
//**************************************************************************************************** 
//2019.10.14 - FPGA1988 - Initial version.
//----------------------------------------------------------------------------------------------------

//File Include : testbench include

//************************************************************************************************
// 1.Interface
//************************************************************************************************
    logic                                   osc_clk                 ;
    logic                                   osc_en                  ;
    bit                                     por_rst_n               ;
    logic                                   por_cfg_done            ;
    logic                                   test_mode_en            ;
    logic                                   dpd_mode_en             ;
    logic                                   stb_mode_en             ;
    
    logic                                   pad_sclk_in             ;
    logic                                   pad_cs_in               ;
    logic                                   pad_si_in               ;
    logic                                   pad_so_in               ;
    logic                                   pad_wp_in               ;
    logic                                   pad_hold_in             ;
    logic                                   pad_out_en              ;
    wire                                    pad_dual_en             ;
    logic                                   pad_quad_en             ;
    wire                                    pad_si_out              ;
    wire                                    pad_so_out              ;
    logic                                   pad_wp_out              ;
    logic                                   pad_hold_out            ;

    logic                                   flh_pump_01_en          ;
    logic                                   flh_pump_02_en          ;
    logic                                   flh_pump_03_en          ;
    logic                                   flh_pump_04_en          ;
    bit                                     flh_pump_01_rdy         ;
    bit                                     flh_pump_02_rdy         ;
    bit                                     flh_pump_03_rdy         ;
    bit                                     flh_pump_04_rdy         ;
    logic                                   flh_sab_en              ;
    logic                                   flh_rd_clk              ;
    logic                                   flh_sa_en               ;
    logic   [`FLASH_ADDR_MAIN_WIDTH-1:00]   flh_addr                ;
    logic   [`FLASH_DATA_IN_WIDTH-1:00]     flh_data_in             ;
    logic   [`FLASH_DATA_OUT_WIDTH-1:00]    flh_data_out            ;
    logic                                   flh_pgm_start           ;
    logic                                   flh_pgm_en              ;
    logic                                   flh_pgm_pulse           ;
    logic                                   flh_pv_en               ;
    bit     [01:00]                         flh_pgm_st              ;
    logic                                   flh_ers_start           ;
    logic                                   flh_ers_en              ;
    logic                                   flh_ers_pulse           ;
    logic                                   flh_ev1_en              ;
    logic                                   flh_ev2_en              ;
    bit     [01:00]                         flh_ers_st              ;
    logic                                   flh_ers_tune1_en        ;
    logic                                   flh_ers_tune2_en        ;
    logic                                   flh_ers_tune1_pulse     ;
    logic                                   flh_ers_tune2_pulse     ;
    logic                                   flh_blk_ers_en          ;
    logic                                   flh_sec_ers_en          ;
    logic                                   flh_ers_64k_en          ;
    logic                                   flh_busy                ;



    
    logic                                   sram_clk                ;//sram clock,fmax : wr_clk = 111MHz rd_clk = 25MHz
    logic   [`SRAM_ADDR_WIDTH-1:00]         sram_addr               ;//sram address
    logic                                   sram_wr_en              ;//sram write enable,1 active
    logic                                   sram_rd_en              ;//sram read enable,1 active
    logic   [`SRAM_DATA_WIDTH-1:00]         sram_data_in            ;//sram data input
    logic   [`SRAM_DATA_WIDTH-1:00]         sram_data_out           ;//sram data output

    //interface : between pad and digital
    modport dbp( 
        input   por_cfg_done            ,
        output  pad_sclk_in             ,
        output  pad_cs_in               ,
        output  pad_si_in               ,
        output  pad_so_in               ,
        output  pad_wp_in               ,
        output  pad_hold_in             ,
        input   pad_out_en              ,
        input   pad_dual_en             ,
        input   pad_quad_en             ,
        input   pad_si_out              ,
        input   pad_so_out              ,
        input   pad_wp_out              ,
        input   pad_hold_out             

    );
    //interface : between analog and digital : bgr + osc + por + pump
    modport dba( 
        input   por_cfg_done            ,
        output  osc_clk                 ,
        input   osc_en                  ,
        output  por_rst_n               , 
        input   flh_busy                ,
        input   test_mode_en            ,
        input   dpd_mode_en             ,
        input   stb_mode_en              
    );
    //interface : between flash and digital
    modport dbf( 
        input   flh_pump_01_en          ,
        input   flh_pump_02_en          ,
        input   flh_pump_03_en          ,
        input   flh_pump_04_en          ,
        output  flh_pump_01_rdy         ,
        output  flh_pump_02_rdy         ,
        output  flh_pump_03_rdy         ,
        output  flh_pump_04_rdy         ,
        input   flh_sab_en              ,
        input   flh_rd_clk              ,
        input   flh_sa_en               ,
        input   flh_addr                ,
        input   flh_data_in             ,
        output  flh_data_out            ,
        input   flh_pgm_start           ,
        input   flh_pgm_en              ,
        input   flh_pgm_pulse           ,
        input   flh_pv_en               ,
        output  flh_pgm_st              ,
        input   flh_ers_start           ,
        input   flh_ers_en              ,
        input   flh_ers_pulse           ,
        input   flh_ev1_en              ,
        input   flh_ev2_en              ,
        output  flh_ers_st              ,
        input   flh_ers_tune1_en        ,
        input   flh_ers_tune2_en        ,
        input   flh_ers_tune1_pulse     ,
        input   flh_ers_tune2_pulse     ,
        input   flh_blk_ers_en          ,
        input   flh_sec_ers_en          ,
        input   flh_ers_64k_en          ,
        input   flh_busy            
    );
    //interface : between sram and digital
    modport dbs( 
        input   sram_clk                ,
        input   sram_wr_en              ,
        input   sram_rd_en              ,
        input   sram_addr               ,
        input   sram_data_in            ,
        output  sram_data_out                              
    );


//****************************************************************************************************
//End of Interface
//****************************************************************************************************
