//****************************************************************************************************  
//---------------Copyright (c) 2019 CIOO@Gitee | wangboworks@126.com All rights reserved--------------
//**************************************************************************************************** 
//File Information
//**************************************************************************************************** 
//File Name      : cioo_sram_model.sv
//Project Name   : ic0001
//Description    : sram behavioral model.
//VC Address     : https://gitee.com/cioo_memory/ic0001
//License        : MuLanPSL-1.0
//**************************************************************************************************** 
//Version Information
//**************************************************************************************************** 
//Create Date    : 2019-10-12 14:10
//First Author   : FPGA1988
//Last Modify    : 2019-10-12 16:30
//Last Author    : FPGA1988
//**************************************************************************************************** 
//Change History(latest change first)
//yyyy.mm.dd - Author - Your log of change
//**************************************************************************************************** 
//2019.10.24 - FPGA1988 - First version.
//---------------------------------------------------------------------------------------
`include "include.svh"
module cioo_sram_model #(
    parameter   SRAM_ADDR_WIDTH     = 8                        ,
    parameter   SRAM_BYTE_SIZE      = 2**SRAM_ADDR_WIDTH       ,
    parameter   SRAM_BIT_WIDTH      = 3                        ,
    parameter   SRAM_BYTE_WIDTH     = 2**SRAM_BIT_WIDTH        ,
    parameter   SRAM_DATA_WIDTH     = 8                         
)(
    input   wire                                    sram_clk        ,//sram clock,fmax : wr_clk = 111MHz rd_clk = 25MHz
    input   wire    [SRAM_ADDR_WIDTH-1:00]          sram_addr       ,//sram address
    input   wire                                    sram_wr_en       ,//sram write enable,1 active
    input   wire                                    sram_rd_en       ,//sram read enable,1 active
    input   wire    [SRAM_DATA_WIDTH-1:00]          sram_data_in      ,//sram data inout
    output  wire    [SRAM_DATA_WIDTH-1:00]          sram_data_out       //sram data inout
);

    // ************************************************************************************
    // 1.parameter and constant definition
    // ************************************************************************************

    // ************************************************************************************
    // 2.registers and wire declaration
    // ************************************************************************************
    
    // ------------------------------------------------------------------------------------
    // 2.1 The clock and enable signal
    // ------------------------------------------------------------------------------------
    
    wire                                sram_mem_a_clk          ;
    wire                                sram_mem_a_wren         ;
    wire    [SRAM_ADDR_WIDTH-1:0]       sram_mem_a_addr         ;
    wire    [07:00]                     sram_mem_a_din          ;
    wire    [07:00]                     sram_mem_a_dout         ;

    wire                                sram_true_write         ;
    wire                                sram_true_read          ;
    wire                                tc_sram_wr_clk          ;
    wire                                tc_sram_rd_clk          ;
        
    // ************************************************************************************
    // 3.main code
    // ************************************************************************************

    assign sram_true_write  = sram_wr_en & (~sram_rd_en);
    assign sram_true_read   = sram_rd_en & (~sram_wr_en);

    assign #2.0 sram_mem_a_clk   = sram_clk  ;
    assign sram_mem_a_addr  = sram_addr ;
    assign sram_mem_a_din   = sram_data_in;
    assign #`tTR3 sram_data_out       = sram_mem_a_dout;
    assign sram_mem_a_wren  = sram_true_write ? 1'b1 : (sram_true_read : 1'b0 : 1'bx);
    // ------------------------------------------------------------------------------------
    // 3.3 The signal register
    // ------------------------------------------------------------------------------------  
    // ------------------------------------------------------------------------------------
    // 3.4 The Timing Check Logic
    // ------------------------------------------------------------------------------------ 
    `ifndef FPGA

    always @(*) begin
        if(sram_wr_en & sram_rd_en)
            begin
                $display("SRAM Timing Error :  write enable and read enable can not be 1 at the same time.");
                #100ns;
                $finish(2);
            end
    end 

    //property sram_p01
    //    @(posedge sram_clk)

    //endproperty

    //always @(sram_clk) begin
    //    sram_a01 : assert (sram_wr_en & sram_rd_en == 1'b0) 
    //    else $error("SRAM Err : write enable and read enable can not be 1 at the same time. ");
    //end
    
    assign tc_sram_wr_clk = sram_wr_en & sram_mem_a_clk;
    assign tc_sram_rd_clk = sram_rd_en & sram_mem_a_clk;

    `ifndef SIM_FAST
    specify
        specparam tTW1 =  0.1 ;
        specparam tTW2 =  0.1 ;
        specparam tTW3 =  20  ; 
        specparam tTW4 =  3.4 ;  
        specparam tTW5 =  4.5 ;  
        specparam tTW6 =  3.5 ;  
        specparam tTW7 =  3.5 ;  
        specparam tTW8 =  20  ; 

        specparam tTR1 =  1   ;
        specparam tTR2 =  10  ; 
        specparam tTR3 =  10  ; 
        specparam tTR4 =  20  ; 
        specparam tTR5 =  10  ; 
        specparam tTR6 =  20  ; 
        specparam tTR7 =  50  ; 
        //1.setup and hold : write
        $setup(sram_data_in,posedge tc_sram_wr_clk,tTW1);
        $setup(sram_addr,posedge tc_sram_wr_clk,tTW2);
        $setup(sram_wr_en,posedge tc_sram_wr_clk,tTW3);
        $hold(negedge tc_sram_wr_clk,sram_data_in,tTW6);
        $hold(negedge tc_sram_wr_clk,sram_addr,tTW7);
        $hold(negedge tc_sram_wr_clk,sram_wr_en,tTW8-20);
        //2.setup and hold : read
        $setup(sram_data_out,posedge tc_sram_rd_clk,tTR1);
        $setup(sram_rd_en,posedge tc_sram_rd_clk,tTR2);
        $hold(negedge tc_sram_rd_clk,sram_addr,tTR5);
        $hold(negedge tc_sram_rd_clk,sram_rd_en,tTR7);
        //3.width 
        $width(posedge tc_sram_wr_clk,tTW4);
        $width(posedge tc_sram_rd_clk,tTR4);

    endspecify
    `endif

    `endif
    
    
    // ************************************************************************************
    // 4.sub-module instantiation
    // ************************************************************************************

    // ------------------------------------------------------------------------------------
    // 4.1 The flash buffer memory module
    // ------------------------------------------------------------------------------------  

    sram_mem_core #(
        .RAM_WIDTH      (SRAM_BYTE_WIDTH    ),
        .RAM_ADDR_BITS  (SRAM_ADDR_WIDTH    )
    )
    sram_mem_inst(
        .mem_a_clk              (sram_mem_a_clk     ),//1   In
        .mem_a_wren             (sram_mem_a_wren    ),//1   In
        .mem_a_addr             (sram_mem_a_addr    ),//7   In
        .mem_a_din              (sram_mem_a_din     ),//8   In
        .mem_a_dout             (sram_mem_a_dout    ),//8   In
        .mem_b_clk              (                   ),//1   In
        .mem_b_wren             (                   ),//1   In
        .mem_b_addr             (                   ),//7   In
        .mem_b_din              (                   ),//8   In
        .mem_b_dout             (                   ) //8   In
    );


endmodule

// ************************************************************************************
// End of Module
// ************************************************************************************
