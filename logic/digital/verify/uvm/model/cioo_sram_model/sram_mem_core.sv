//****************************************************************************************************  
//---------------Copyright (c) 2019 CIOO@Gitee | wangboworks@126.com All rights reserved--------------
//**************************************************************************************************** 
//File Information
//**************************************************************************************************** 
//File Name      : sram_mem_core.sv
//Project Name   : ic0001
//Description    : memory core.
//VC Address     : https://gitee.com/cioo_memory/ic0001
//License        : MuLanPSL-1.0
//**************************************************************************************************** 
//Version Information
//**************************************************************************************************** 
//Create Date    : 2019-10-12 14:10
//First Author   : FPGA1988
//Last Modify    : 2019-10-12 16:30
//Last Author    : FPGA1988
//**************************************************************************************************** 
//Change History(latest change first)
//yyyy.mm.dd - Author - Your log of change
//**************************************************************************************************** 
//2019.10.24 - FPGA1988 - First version.
//---------------------------------------------------------------------------------------
module sram_mem_core #(
    parameter   RAM_WIDTH       = 8    ,   
    parameter   RAM_ADDR_BITS   = 8    
)(
    mem_a_clk           ,//1    In
    mem_a_wren          ,//1    In
    mem_a_addr          ,//14   In
    mem_a_din           ,//38   In
    mem_a_dout          ,//38   Out
    mem_b_clk           ,//1    In
    mem_b_wren          ,//1    In
    mem_b_addr          ,//14   In
    mem_b_din           ,//38   In
    mem_b_dout          //38    Out
);

    // ************************************************************************************
    // 1.input and output declaration
    // ************************************************************************************
    input                       mem_a_clk   ;//fpga clock signal,20MHz
    input                       mem_a_wren  ;//eeprom extclk enable
    input   [RAM_WIDTH-1:0]     mem_a_din   ;//ee option
    output  [RAM_WIDTH-1:0]     mem_a_dout  ;//data from logic to ee
    input   [RAM_ADDR_BITS-1:0] mem_a_addr  ;//ecc from logic to ee

    input                       mem_b_clk   ;//fpga clock signal,20MHz
    input                       mem_b_wren  ;//eeprom extclk enable
    input   [RAM_WIDTH-1:0]     mem_b_din   ;//ee option
    output  [RAM_WIDTH-1:0]     mem_b_dout  ;//data from logic to ee
    input   [RAM_ADDR_BITS-1:0] mem_b_addr  ;//ecc from logic to ee 
    
    // ************************************************************************************
    // 2.parameter and constant definition
    // ************************************************************************************

    
    // ************************************************************************************
    // 3.registers and wire declaration
    // ************************************************************************************
    
    
    wire                        mem_a_enable                            ;//
    wire                        mem_b_enable                            ;//
    
//  (* RAM_STYLE="{AUTO | BLOCK |  BLOCK_POWER1 | BLOCK_POWER2}" *)
    (* RAM_STYLE="BLOCK" *)
    reg     [RAM_WIDTH-1:0]     ram_mem [(2**RAM_ADDR_BITS)-1:0]	    ;//

    reg     [RAM_WIDTH-1:0]     mem_a_dout = 0;
    reg     [RAM_WIDTH-1:0]     mem_b_dout = 0;
    // ************************************************************************************
    // 4.main code
    // ************************************************************************************

    // ------------------------------------------------------------------------------------
    // 4.1 The memory initial logic
    // ------------------------------------------------------------------------------------  

    //  The following code is only necessary if you wish to initialize the RAM 
    //  contents via an external file (use $readmemb for binary data)
//	initial
//		$readmemh("main_mem_init.coe",ram_mem,0,2**RAM_ADDR_BITS-1);
    //`define SIM 1     
	`ifdef RTLSIM
    integer i;
    initial begin
        for(i=0;i<2**RAM_ADDR_BITS;i=i+1)
            ram_mem[i] = $urandom_range(8'hff);
    end
    `endif
	//`else

	//initial
	//	$readmemh("main_mem_init.coe",ram_mem,0,2**RAM_ADDR_BITS-1);
	//`endif       

    // ------------------------------------------------------------------------------------
    // 4.2 The main memory read and write logic
    // ------------------------------------------------------------------------------------  
	assign mem_a_enable = 1'b1;
	assign mem_b_enable = 1'b0;

    always @(posedge mem_a_clk)
        if (mem_a_enable) begin
            if (mem_a_wren)
                ram_mem[mem_a_addr] <= mem_a_din;
            mem_a_dout  <= ram_mem[mem_a_addr];
        end
        
    always @(posedge mem_b_clk)
        if (mem_b_enable) begin
            if (mem_b_wren)
                ram_mem[mem_b_addr] <= mem_b_din;
            mem_b_dout  <= ram_mem[mem_b_addr];
        end
        
    // ************************************************************************************
    // 5.sub-module instantiation
    // ************************************************************************************


endmodule

// ************************************************************************************
// End of Module
// ************************************************************************************
