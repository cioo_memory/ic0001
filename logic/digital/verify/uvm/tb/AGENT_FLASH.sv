//****************************************************************************************************  
//---------------Copyright (c) 2019 CIOO@Gitee | wangboworks@126.com All rights reserved--------------
//**************************************************************************************************** 
//File Information
//**************************************************************************************************** 
//File Name      : AGENT_FLASH.v 
//Project Name   : ic0001
//Description    : the uvm agent, which is derived from uvm_agent.
//VC Address     : https://gitee.com/cioo_memory/ic0001
//License        : MuLanPSL-1.0
//**************************************************************************************************** 
//Version Information
//**************************************************************************************************** 
//Create Date    : 2019-10-12 14:10
//First Author   : FPGA1988
//Last Modify    : 2019-10-12 16:30
//Last Author    : FPGA1988
//**************************************************************************************************** 
//Change History(latest change first)
//yyyy.mm.dd - Author - Your log of change
//**************************************************************************************************** 
//2019.10.12 - FPGA1988 - Add the basic architecture and add build phase to create the uvm tree.
//----------------------------------------------------------------------------------------------------
//`timescale 1ns/1ps
`ifndef __AGENT_FLASH__
`define __AGENT_FLASH__
// ************************************************************************************
// 1.Class define
// ************************************************************************************
class AGENT_FLASH extends uvm_agent;
// ------------------------------------------------------------------------------------
// 1.1  instance define
// ------------------------------------------------------------------------------------  
    SEQUENCER_FLASH   sqr;
    DRIVER_FLASH      drv;
    MONITOR_FLASH     mon;

    //one ap : agt_i for drv and agt_o for mon 
    uvm_analysis_port #(TRANSACTION_FLASH) drv_ap;
    uvm_analysis_port #(TRANSACTION_SCB) mon_ap;

// ------------------------------------------------------------------------------------
// 1.2  extern function and task define
// ------------------------------------------------------------------------------------  

    function new(string name,uvm_component parent);
        super.new(name,parent);
        `uvm_info("AGENT_FLASH",$sformatf("The %s has been created",name),UVM_HIGH);
    endfunction


    extern virtual function void build_phase(uvm_phase phase);
    extern function void connect_phase(uvm_phase phase);
// ------------------------------------------------------------------------------------
// 1.3  factory register
// ------------------------------------------------------------------------------------  
    `uvm_component_utils(AGENT_FLASH)

endclass

function void AGENT_FLASH::build_phase(uvm_phase phase);
    super.build_phase(phase);
    //1.the driver may be unnecessary,default is_active = UVM_ACTIVE
    if(is_active == UVM_ACTIVE) begin
        sqr = SEQUENCER_FLASH::type_id::create("sqr",this);
        drv = DRIVER_FLASH::type_id::create("drv",this);
    end
    //2.only PASSSIVE agent define instance the mon(ACTIVE can implement it)
    if(is_active == UVM_PASSIVE)
        mon = MONITOR_FLASH::type_id::create("mon",this);
    `uvm_info("AGENT_FLASH","build phase is start",UVM_HIGH)

    //2.connect objection new
    //ap = new("ap",this); 

endfunction

function void AGENT_FLASH::connect_phase(uvm_phase phase);
    super.connect_phase(phase);
    if(is_active == UVM_ACTIVE)
        drv_ap = drv.ap;
    if(is_active == UVM_PASSIVE)
        mon_ap = mon.ap;
    if(is_active == UVM_ACTIVE) begin
        drv.seq_item_port.connect(sqr.seq_item_export);
    end
endfunction
`endif
