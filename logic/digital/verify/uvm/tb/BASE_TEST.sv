//****************************************************************************************************  
//---------------Copyright (c) 2019 CIOO@Gitee | wangboworks@126.com All rights reserved--------------
//**************************************************************************************************** 
//File Information
//**************************************************************************************************** 
//File Name      : BASE_TEST.v 
//Project Name   : ic0001
//Description    : the uvm base test, which is derived from uvm_test.
//VC Address     : https://gitee.com/cioo_memory/ic0001
//License        : MuLanPSL-1.0
//**************************************************************************************************** 
//Version Information
//**************************************************************************************************** 
//Create Date    : 2019-10-12 14:10
//First Author   : FPGA1988
//Last Modify    : 2019-10-12 16:30
//Last Author    : FPGA1988
//**************************************************************************************************** 
//Change History(latest change first)
//yyyy.mm.dd - Author - Your log of change
//**************************************************************************************************** 
//2019.10.12 - FPGA1988 - Add the basic architecture and add build phase to create the uvm tree.
//----------------------------------------------------------------------------------------------------
// ************************************************************************************
// 1.Class define
// ************************************************************************************
`ifndef __BASE_TEST__
`define __BASE_TEST__
class BASE_TEST extends uvm_test;
    ENVIRONMENT             env         ;
    //SEQUENCER_ALL           sqr_all     ;
    //SEQUENCER_REG           sqr_reg     ;
    //REG_MODEL_FLASH         reg_mdl_flh ;

    function new(string name = "my_test",uvm_component parent = null);
        super.new(name,parent);
        `uvm_info("BASE_TEST",$sformatf("The %s has been created",name),UVM_DEBUG);
    endfunction

    extern virtual function void build_phase(uvm_phase phase);
    extern virtual function void connect_phase(uvm_phase phase);
    //extern virtual function void report_phase(uvm_phase phase);
    
    //register the factory
    `uvm_component_utils(BASE_TEST)

endclass

function void BASE_TEST::build_phase(uvm_phase phase);
    super.build_phase(phase);
    env = ENVIRONMENT::type_id::create("env",this);
    //sqr_all = SEQUENCER_ALL::type_id::create("sqr_all",this);
    //sqr_reg = SEQUENCER_REG::type_id::create("sqr_reg",this);

    //reg model
    //reg_mdl_flh = REG_MODEL_FLASH::type_id::create("reg_mdl_flh",this);
    //reg_mdl_flh.configure(null,"");
    //reg_mdl_flh.build();
    //reg_mdl_flh.lock_model();
    //reg_mdl_flh.reset();
    //reg_mdl_flh.set_hdl_path_root("uvm_test_top.reg_mdl_flh");

    //env.reg_mdl_flh = this.reg_mdl_flh;
    //$display("path = %s.",reg_mdl_flh.has_hdl_path());

    `uvm_info("BASE_TEST","build phase is start",UVM_HIGH);
endfunction

function void BASE_TEST::connect_phase(uvm_phase phase);
    super.connect_phase(phase);
    //sqr_all.sqr_flh = env.agt_flh_i.sqr;
    //sqr_all.reg_mdl_flh = this.reg_mdl_flh;
    

    //reg_mdl_flh.default_map.set_auto_predict(1);
    `uvm_info("BASE_TEST","connect phase is start",UVM_HIGH);
endfunction

`endif
