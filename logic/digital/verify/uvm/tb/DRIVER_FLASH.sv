//****************************************************************************************************  
//---------------Copyright (c) 2019 CIOO@Gitee | wangboworks@126.com All rights reserved--------------
//**************************************************************************************************** 
//File Information
//**************************************************************************************************** 
//File Name      : DRIVER_FLASH.v 
//Project Name   : ic0001
//Description    : the flash driver, which is derived from uvm_driver.
//VC Address     : https://gitee.com/cioo_memory/ic0001
//License        : MuLanPSL-1.0
//**************************************************************************************************** 
//Version Information
//**************************************************************************************************** 
//Create Date    : 2019-10-12 14:10
//First Author   : FPGA1988
//Last Modify    : 2019-11-06 15:30
//Last Author    : FPGA1988
//**************************************************************************************************** 
//Change History(latest change first)
//yyyy.mm.dd - Author - Your log of change
//**************************************************************************************************** 
//2019.11.06 - FPGA1988 - Add the one-wire write and read task.
//2019.10.12 - FPGA1988 - Add the basic architecture and add build phase to create the uvm tree.
//----------------------------------------------------------------------------------------------------
`ifndef __DRIVER_FLASH__
`define __DRIVER_FLASH__

//`include "include.svh"

class DRIVER_FLASH extends uvm_driver #(TRANSACTION_FLASH);

    virtual sys_if sys_vif;
    uvm_analysis_port #(TRANSACTION_FLASH) ap;
    TRANSACTION_FLASH trans_flh;
    
    function new(string name = "DRIVER_FLASH",uvm_component parent);
    super.new(name,parent);
    set_report_verbosity_level(UVM_HIGH);
    `uvm_info("DRIVER_FLASH",$sformatf("The %s has been created",name),UVM_HIGH);
    endfunction

    extern function void build_phase(uvm_phase phase);
    extern task main_phase(uvm_phase phase);
    extern task send_packet(TRANSACTION_FLASH trans_flh);

    //flash task
    extern task flh_single_cmd(TRANSACTION_FLASH trans_flh);
    extern task flh_1_wire_write(TRANSACTION_FLASH trans_flh);
    extern task flh_1_wire_read(TRANSACTION_FLASH trans_flh);


    //spi basic task
    extern task spi_start(input bit spi_mode);
    extern task spi_sclk_gen(input logic en,input real tCLK = `tCLK,input real tCL = `tCL);
    extern task spi_stop(input bit spi_mode);
    extern task spi_send_cmd(input logic [07:00] data,input real tCL = `tCL,input int tDVCH = `tDVCH,input int tCHDX = `tCHDX,input logic test_en = 0);
    extern task spi_send_1_wire(input bit [07:00] data[$],input [02:00] bit_cnt,input real tCL = `tCL,input int tDVCH = `tDVCH,input int tCHDX = `tCHDX);
    extern task spi_recv_1_wire(input bit [15:00] recv_len,input bit [2:0] bit_num = 0,ref logic [07:00] data[$]);

    `uvm_component_utils(DRIVER_FLASH)
endclass


function void DRIVER_FLASH::build_phase(uvm_phase phase);
    super.build_phase(phase);
    //get the interface 
    if(!uvm_config_db#(virtual sys_if)::get(this,"","transfer_obj_01",sys_vif))
        `uvm_fatal("DRIVER_FLASH","virtual interface must be set for sys_vif.");
    ap = new("ap",this);
    `uvm_info("DRIVER_FLASH","build phase is start",UVM_HIGH)

endfunction

task DRIVER_FLASH::main_phase(uvm_phase phase);
    trans_flh = new();
    `uvm_info("DRIVER_FLASH","driver main phase start.",UVM_HIGH)
    sys_vif.spi_if.io2_wp_n_i = 1'b1;
    sys_vif.spi_if.io3_hold_n_i = 1'b1;
    while(1) begin
        seq_item_port.get_next_item(trans_flh);
        ap.write(trans_flh);
        send_packet(trans_flh);
        seq_item_port.item_done();
    end

endtask

task DRIVER_FLASH::send_packet(TRANSACTION_FLASH trans_flh);
    `uvm_info("DRIVER_FLASH",$sformatf("FLASH Command : %h.",trans_flh.cmd),UVM_HIGH)
    case(trans_flh.cmd)
        `SPI_WREN,`SPI_WRDI,`SPI_WREN_VSR,`SPI_CHIP_ERASE_0,`SPI_CHIP_ERASE_1,
        `SPI_ENABLE_RESET,`SPI_RESET,`SPI_PES,`SPI_PER,
        `SPI_ESRY,`SPI_DSRY,`SPI_DP : flh_1_wire_write(trans_flh);           
        `SPI_RDSR_LB,`SPI_RDSR_HB   : flh_1_wire_read(trans_flh);
        `TM_SRAM_WRITE              : flh_1_wire_write(trans_flh);
        `TM_SRAM_READ               : flh_1_wire_read(trans_flh);
        //`SPI_WRSR                 :
        //`SPI_READ                 :
        //`SPI_FAST_READ            : 
        //`SPI_DUAL_OUTPUT_FAST_READ:     
        //`SPI_DUAL_IO_FAST_READ    :     
        //`SPI_QUAD_OUTPUT_FAST_READ:     
        //`SPI_QUAD_IO_FAST_READ    :     
        //`SPI_SET_BURST_WITH_WRAP  :     
        //`SPI_PAGE_PROGRAM         : 
        //`SPI_QUAD_PAGE_PROGRAM    :     
        //`SPI_SECTOR_ERASE         : 
        //`SPI_32K_BLOCK_ERASE      :     
        //`SPI_64K_BLOCK_ERASE      :     
        //`SPI_SET_BURST_WITH_WRAP  :     
        //`SPI_DP_RELEASE           : 
        //`SPI_RDI                  :
        //`SPI_REMS                 :
        //`SPI_DIO_REMS             : 
        //`SPI_QIO_REMS             : 
        //`SPI_RDID                 :
        //`SPI_RD_UID               :
        //`SPI_ERASE_SEC_REG        : 
        //`SPI_PROG_SEC_REG         : 
        //`SPI_READ_SEC_REG         : 
        //`SPI_READ_SFDP            : 
    default:;
    endcase

endtask

task DRIVER_FLASH::flh_1_wire_read(TRANSACTION_FLASH trans_flh);
    `uvm_info("DRIVER_FLASH",$sformatf("SPI send single command : 0x%h.",trans_flh.cmd),UVM_HIGH)
    spi_start(trans_flh.spi_mode);
    fork begin : spi_reg_fork
        fork
            spi_sclk_gen(1'b1);
        join_none
        spi_send_cmd(trans_flh.cmd);
        case(trans_flh.cmd)
            `TM_SRAM_READ : 
                spi_send_1_wire(trans_flh.addr_08_queue,trans_flh.bit_num);
            default:;
        endcase
        spi_recv_1_wire(trans_flh.rd_len,trans_flh.bit_num,trans_flh.rdata_08_queue);
        spi_sclk_gen(1'b0);
        disable fork;
        end
    join
    spi_stop(trans_flh.spi_mode);
    //ap.write(trans_flh);
endtask

task DRIVER_FLASH::flh_1_wire_write(TRANSACTION_FLASH trans_flh);
    `uvm_info("DRIVER_FLASH",$sformatf("SPI send single command : 0x%h.",trans_flh.cmd),UVM_HIGH)
    spi_start(trans_flh.spi_mode);
    fork begin : spi_reg_fork
        fork
            spi_sclk_gen(1'b1);
        join_none
        spi_send_cmd(trans_flh.cmd);
        //address cycle
        case(trans_flh.cmd)
            `TM_SRAM_WRITE :
                spi_send_1_wire(trans_flh.addr_08_queue,trans_flh.bit_num);
            default:;
        endcase
        //data cycle
        spi_send_1_wire(trans_flh.wdata_08_queue,trans_flh.bit_num);
        spi_sclk_gen(1'b0);
        disable fork;
        end
    join
    spi_stop(trans_flh.spi_mode);
    //ap.write(trans_flh);
endtask

task DRIVER_FLASH::spi_send_1_wire(input bit [07:00] data[$],input [02:00] bit_cnt,input real tCL = `tCL,input int tDVCH = `tDVCH,input int tCHDX = `tCHDX);
    bit             bit_data[$] ;
    int             bit_num     ;
    //bit num 1:
    /*
        bit_cnt     bit_num
        0           8
        1           1
        .           .
        7           7
    */
    bit_num = {~(|bit_cnt),bit_cnt};
    //bit num 2 = bit_num_1 + (byte_num-1) * 8
    bit_num += (data.size()-1)*8;
    $display("data_size = %d.",data.size());
    foreach(data[i]) begin
        `ifdef RPT_DEBUG
            $display("  [%0t] ==[SPI DRIVER INFO]==  /* send spi */ spi send byte = [%0h] ",$realtime,data[i]);
        `endif
        for(int j=7;j>=0;j--) begin
            bit_data.push_back(data[i][j]);
            bit_num--;
            if(bit_num == 0) begin
                break;
            end
        end
        if(bit_num == 0) begin
            break;
        end
    end

    foreach(bit_data[i]) begin
        if(i == 0) begin
            sys_vif.spi_if.io0_si_i     = bit_data[i]; 
            @(posedge sys_vif.spi_if.sclk);
        end
        else begin
            @(negedge sys_vif.spi_if.sclk)
            #(tCL-tDVCH);
            sys_vif.spi_if.io0_si_i     = bit_data[i];
            @(posedge sys_vif.spi_if.sclk);
            #tCHDX;
            sys_vif.spi_if.io0_si_i     = 1'bx;
        end
        `ifdef RPT_DEBUG
            $display("    [%0t] ==[SPI DRIVER INFO]==  /* send spi */ spi send bit %0dth[%0b] ",$realtime,i,bit_data[i]);
        `endif
    end
    sys_vif.spi_if.io0_si_i     = 1'b1;
endtask

task DRIVER_FLASH::spi_start(input bit spi_mode);
    `ifdef RPT_DEBUG
    $display("[%0t] ==[SPI DRIVER INFO]==  /* send spi */ send start ",$realtime);
    `endif
    //sys_vif.spi_if.sclk = spi_mode;
    sys_vif.spi_if.sclk = $urandom_range(1);
    sys_vif.spi_if.cs_n = `DISABLE_N;
    #`tCHSL;
    sys_vif.spi_if.cs_n = `ENABLE_N;
    #(`tSHCH-`tCL);
endtask

task DRIVER_FLASH::spi_sclk_gen(input logic en,input real tCLK = `tCLK,input real tCL = `tCL);
    int l_period    ;
    int clk_duty    ;
    //$display("tCL = %d.tCLK = %d.",tCL,tCLK);
    //clk_duty    = $urandom_range(51,49);
    l_period = tCLK / 2;
    while(en) begin
    if(en) begin
        #tCL;
        sys_vif.spi_if.sclk = ~sys_vif.spi_if.sclk;
        #(tCLK - tCL);
        sys_vif.spi_if.sclk = ~sys_vif.spi_if.sclk;
    end
    else
        sys_vif.spi_if.sclk = 1'b0;
    end
endtask

task DRIVER_FLASH::spi_stop(input bit spi_mode);
    `uvm_info("DRIVER_FLASH","send stop",UVM_HIGH);
    sys_vif.spi_if.cs_n = `ENABLE_N;
    sys_vif.spi_if.sclk = spi_mode;
    #(`tCHSH);
    sys_vif.spi_if.cs_n = `DISABLE_N;
    #(`tSHCH);
    //sys_vif.spi_if.sclk = $urandom_range(1);
    sys_vif.spi_if.cs_n = `DISABLE_N;
endtask

task DRIVER_FLASH::spi_send_cmd(input logic [07:00] data,input real tCL = `tCL,input int tDVCH = `tDVCH,input int tCHDX = `tCHDX, input logic test_en = 0);
    `ifdef RPT_DEBUG
    $display("[%0t] ==[SPI DRIVER INFO]==  /* send spi */ send cmd : cmd = 0x%h ",$realtime,data);
    `endif
    for(int i=7;i>=0;i--) begin
    if(i == 7) begin
        if (test_en)
        sys_vif.spi_if.io1_so_i = data[i];
        else
        sys_vif.spi_if.io0_si_i = data[i];
        @(posedge sys_vif.spi_if.sclk);
    end
    else begin
        @(negedge sys_vif.spi_if.sclk)
        #(tCL-tDVCH);
        if (test_en)
        sys_vif.spi_if.io1_so_i = data[i];
        else
        sys_vif.spi_if.io0_si_i = data[i];
        @(posedge sys_vif.spi_if.sclk);
        #tCHDX;
        if (test_en)
        //sys_vif.spi_if.io1_so_i = 1'bx;
        sys_vif.spi_if.io1_so_i = sys_vif.spi_if.io1_so_i;
        else 
        sys_vif.spi_if.io0_si_i = 1'bx;
    end
    `ifdef RPT_DEBUG
        $display("    [%0t] ==[SPI DRIVER INFO]==  /* send spi */ spi send bit %0dth[%0b] ",$realtime,i,data[i]);
    `endif
    end
    //@(posedge sys_vif.spi_if.sclk);
    `ifdef RPT_DEBUG
    $display("  [%0t] ==[SPI DRIVER INFO]==  /* send spi */ spi send byte = [%0h] ",$realtime,data);
    `endif
endtask


task DRIVER_FLASH::spi_recv_1_wire(input bit [15:00] recv_len,input bit [2:0] bit_num = 0,ref logic [07:00] data[$]);
    //all receive bit = recv_len * 8 + bit_num
    logic   [07:00]     byte_data   ;
    repeat(recv_len) begin
        for(int i=7;i>=0;i--) begin
            @(posedge sys_vif.spi_if.sclk);
            byte_data[i] = sys_vif.spi_if.io1_so_o;
            `uvm_info("DRIVER_FLASH",$sformatf("spi receive bit = [%0h].",byte_data[i]),UVM_DEBUG);
        end
        `uvm_info("DRIVER_FLASH",$sformatf("spi receive a byte data = [%0h].",byte_data),UVM_HIGH)
        data.push_back(byte_data);
    end
    if(bit_num != 0)
        for(int i=7;i>=7-bit_num;i--) begin
            @(posedge sys_vif.spi_if.sclk);
            byte_data[i] = sys_vif.spi_if.io1_so_o;
            `uvm_info("DRIVER_FLASH",$sformatf("spi receive bit = [%0h].",byte_data[i]),UVM_HIGH)
        end
        data.push_back(byte_data);
endtask

`endif
