//****************************************************************************************************  
//---------------Copyright (c) 2019 CIOO@Gitee | wangboworks@126.com All rights reserved--------------
//**************************************************************************************************** 
//File Information
//**************************************************************************************************** 
//File Name      : Environment.v 
//Project Name   : ic0001
//Description    : the uvm environment, which is derived from uvm_env.
//VC Address     : https://gitee.com/cioo_memory/ic0001
//License        : MuLanPSL-1.0
//**************************************************************************************************** 
//Version Information
//**************************************************************************************************** 
//Create Date    : 2019-10-12 14:10
//First Author   : FPGA1988
//Last Modify    : 2019-10-12 16:30
//Last Author    : FPGA1988
//**************************************************************************************************** 
//Change History(latest change first)
//yyyy.mm.dd - Author - Your log of change
//**************************************************************************************************** 
//2019.08.13 - FPGA1988 - Add the basic architecture and add build phase to create the uvm tree.
//----------------------------------------------------------------------------------------------------
//`timescale 1ns/1ps
`ifndef __ENVIRONMENT__
    `define __ENVIRONMENT__
/*
    //derived from uvm env[component]
    //env -> agent * n + scb + ref_mdl_flh + reg_mdl
*/
// ************************************************************************************
// 1.Class define
// ************************************************************************************
class ENVIRONMENT extends uvm_env;

// ------------------------------------------------------------------------------------
// 1.1  instance define
// ------------------------------------------------------------------------------------  
    //agent_i is active mode, agent_o is passive mode : set in the env
    AGENT_FLASH             agt_flh_i   ;
    AGENT_FLASH             agt_flh_o   ;
    SCOREBOARD              scb         ;
    REFERENCE_FLASH         ref_mdl_flh ;
    //REG_MODEL_FLASH         reg_mdl_flh ;


    uvm_tlm_analysis_fifo #(TRANSACTION_FLASH) drv_ref_fifo;
    uvm_tlm_analysis_fifo #(TRANSACTION_SCB) ref_scb_fifo;
    uvm_tlm_analysis_fifo #(TRANSACTION_SCB) mon_scb_fifo;

// ------------------------------------------------------------------------------------
// 1.2  extern function and task define
// ------------------------------------------------------------------------------------  
    extern function new(string name = "env",uvm_component parent);
    //why the build_phase use the virtual 
    extern virtual function void build_phase(uvm_phase phase);
    extern function void connect_phase(uvm_phase phase);
    //extern task main_phase(uvm_phase phase);
    
// ------------------------------------------------------------------------------------
// 1.3  factory register
// ------------------------------------------------------------------------------------  
    `uvm_component_utils(ENVIRONMENT)

endclass
// ************************************************************************************
// 2.Externed function and task implement
// ************************************************************************************
function ENVIRONMENT::new(string name = "env",uvm_component parent);
    super.new(name,parent);
    `uvm_info("ENVIRONMENT",$sformatf("The %s has been created",name),UVM_HIGH);
endfunction
   
// ------------------------------------------------------------------------------------
// 2.2  build phase : create uvm tree
// ------------------------------------------------------------------------------------  
function void ENVIRONMENT::build_phase(uvm_phase phase);
    super.build_phase(phase);
    //---- 2.2.1 new component
    //if you want to use xx::type_id::create("name",this) to create the instance, the instance must be register to factory
    agt_flh_i = AGENT_FLASH::type_id::create("agt_flh_i",this);
    agt_flh_o = AGENT_FLASH::type_id::create("agt_flh_o",this);
    //agent mode set : is_active = UVM_ACTIVE/UVM_PASSIVE
    agt_flh_i.is_active = UVM_ACTIVE;
    agt_flh_o.is_active = UVM_PASSIVE;

    ref_mdl_flh = REFERENCE_FLASH::type_id::create("ref_mdl_flh",this);
    scb     = SCOREBOARD::type_id::create("scb",this);
    //reg_mdl = REGISTER::type_id::create("reg_mdl",this);

    //---- 2.2.2 new port/fifo
    drv_ref_fifo = new("drv_ref_fifo",this);
    ref_scb_fifo = new("ref_scb_fifo",this);
    mon_scb_fifo = new("mon_scb_fifo",this);



    `uvm_info("ENVIRONMENT","build phase is start",UVM_HIGH);
endfunction

function void ENVIRONMENT::connect_phase(uvm_phase phase);
    super.connect_phase(phase);

    //1.drv -> ref
    //1.1 ap -> analysis_export
    agt_flh_i.drv_ap.connect(drv_ref_fifo.analysis_export);
    //1.2 blocking_get_port -> blocking_get_export
    ref_mdl_flh.port.connect(drv_ref_fifo.blocking_get_export);

    //2.mon -> scb
    //1.1 ap -> analysis_export
    agt_flh_o.mon_ap.connect(mon_scb_fifo.analysis_export);
    //1.2 blocking_get_port -> blocking_get_export
    scb.mon_port.connect(mon_scb_fifo.blocking_get_export);

    //3.ref -> scb
    //1.1 ap -> analysis_export
    ref_mdl_flh.ap.connect(ref_scb_fifo.analysis_export);
    //1.2 blocking_get_port -> blocking_get_export
    scb.ref_port.connect(ref_scb_fifo.blocking_get_export);



    //ref_mdl_flh.reg_mdl_flh = this.reg_mdl_flh;

endfunction

`endif
