//****************************************************************************************************  
//---------------Copyright (c) 2019 CIOO@Gitee | wangboworks@126.com All rights reserved--------------
//**************************************************************************************************** 
//File Information
//**************************************************************************************************** 
//File Name      : Interface.sv 
//Project Name   : ic0001
//Description    : the testbench Interface
//VC Address     : https://gitee.com/cioo_memory/ic0001
//License        : MuLanPSL-1.0
//**************************************************************************************************** 
//Version Information
//**************************************************************************************************** 
//Create Date    : 2019-10-12 15:30
//First Author   : FPGA1988
//Last Modify    : 2019-10-12 16:00
//Last Author    : FPGA1988
//**************************************************************************************************** 
//Change History(latest change first)
//yyyy.mm.dd - Author - Your log of change
//**************************************************************************************************** 
//2019.10.12 - FPGA1988 - The first version.
//*---------------------------------------------------------------------------------------------------
//File Include : system header file
//`include "include.svh"

//File Include : testbench include

`ifndef __INTERFACE__
`define __INTERFACE__
interface sys_if();
    //************************************************************************************************
    // 1.All I/O
    //************************************************************************************************
    //------------------------------------------------------------------------------------------------
    //1.1 power interface
    //------------------------------------------------------------------------------------------------
    logic                   vss             ;
    logic                   vcc             ;
    
    //------------------------------------------------------------------------------------------------
    //1.2 ASIC top interface
    //------------------------------------------------------------------------------------------------

    bit                     cs_n        ;
    bit                     sclk        ;
    logic                   io0_si_i    ;
    logic                   io1_so_i    ;
    logic                   io2_wp_n_i  ;
    logic                   io3_hold_n_i;
    logic                   io0_si_o    ;
    logic                   io1_so_o    ;
    logic                   io2_wp_n_o  ;
    logic                   io3_hold_n_o;
    logic                   cs_n_hv     ;

    //------------------------------------------------------------------------------------------------
    //1.3 FPGA interface
    //------------------------------------------------------------------------------------------------
    `ifdef FPGA
    logic                   fpga_clk        ;
    logic                   fpga_rst_n      ;
    logic                   soft_rst_n      ;
    wire                    so              ;
    wire                    wp_n            ;
    wire                    si              ;
    wire                    hold_n          ;
    `endif
    //************************************************************************************************
    // 2.Modport
    //************************************************************************************************
    `ifdef FPGA
    modport cr_if(
        output fpga_clk     ,
        output fpga_rst_n   ,
        output soft_rst_n
    );
    `endif
    modport spi_if_dut(
        input cs_n         ,        
        input sclk         ,
        input io0_si_i     ,
        input io1_so_i     ,
        input io2_wp_n_i   ,
        input io3_hold_n_i ,
        output  io0_si_o     ,
        output  io1_so_o     ,
        output  io2_wp_n_o   ,
        output  io3_hold_n_o ,
        input cs_n_hv     
    );
    modport spi_if(
        output cs_n         ,        
        output sclk         ,
        output io0_si_i     ,
        output io1_so_i     ,
        output io2_wp_n_i   ,
        output io3_hold_n_i ,
        input  io0_si_o     ,
        input  io1_so_o     ,
        input  io2_wp_n_o   ,
        input  io3_hold_n_o ,
        output cs_n_hv     
    );

endinterface

`endif
