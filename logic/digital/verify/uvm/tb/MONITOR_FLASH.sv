//****************************************************************************************************  
//---------------Copyright (c) 2019 CIOO@Gitee | wangboworks@126.com All rights reserved--------------
//**************************************************************************************************** 
//File Information
//**************************************************************************************************** 
//File Name      : BASE_TEST.v 
//Project Name   : ic0001
//Description    : the uvm base test, which is derived from uvm_test.
//VC Address     : https://gitee.com/cioo_memory/ic0001
//License        : MuLanPSL-1.0
//**************************************************************************************************** 
//Version Information
//**************************************************************************************************** 
//Create Date    : 2019-10-12 14:10
//First Author   : FPGA1988
//Last Modify    : 2019-10-12 16:30
//Last Author    : FPGA1988
//**************************************************************************************************** 
//Change History(latest change first)
//yyyy.mm.dd - Author - Your log of change
//**************************************************************************************************** 
//2019.10.12 - FPGA1988 - Add the basic architecture and add build phase to create the uvm tree.
//----------------------------------------------------------------------------------------------------
`ifndef __MONITOR_FLASH__
`define __MONITOR_FLASH__
class MONITOR_FLASH extends uvm_monitor;
    
    virtual sys_if sys_vif; 
    uvm_analysis_port #(TRANSACTION_SCB) ap;
    event start_event,stop_event;

    function new(string name="MONITOR_FLASH",uvm_component parent=null);
        super.new(name,parent);
        `uvm_info("MONITOR_FLASH",$sformatf("The %s has been created",name),UVM_HIGH);
    endfunction

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        if(!uvm_config_db#(virtual sys_if)::get(this,"","transfer_obj_01",sys_vif))
            `uvm_fatal("DRIVER_FLASH","virtual interface must be set for sys_vif.");
        ap = new("ap",this);
        set_report_verbosity_level(UVM_HIGH);
        `uvm_info("MONITOR_FLASH","build phase is start",UVM_HIGH);
    endfunction

    extern task main_phase(uvm_phase phase);

    extern task collect_packet(TRANSACTION_SCB trans_scb);
    //extern task generate_packet(TRANSACTION_SCB trans_scb);
    extern task spi_get_cmd(TRANSACTION_SCB trans_scb);
    extern task spi_get_stop();
    extern task spi_get_start();
    extern task spi_get_byte(TRANSACTION_SCB trans_scb);
    extern task spi_get_byte_1_wire(TRANSACTION_SCB trans_scb);
    //extern task spi_get_byte_2_wire(TRANSACTION_SCB trans_scb);
    //extern task spi_get_byte_4_wire(TRANSACTION_SCB trans_scb);

    `uvm_component_utils(MONITOR_FLASH)

endclass

task MONITOR_FLASH::main_phase(uvm_phase phase);
    TRANSACTION_SCB trans_scb;
    trans_scb = new;
    while(1) begin
        collect_packet(trans_scb);
        if(trans_scb.cmd == `SPI_RDSR_HB | trans_scb.cmd == `SPI_RDSR_LB) begin
            //$display("-----------------------------------------");
            //trans_scb.print();
            ap.write(trans_scb);
        end
    end
endtask


task MONITOR_FLASH::collect_packet(TRANSACTION_SCB trans_scb);
    bit         start_en;
    bit [07:00] cmd;
    `uvm_info("MONITOR_FLASH","Collect a packet start.",UVM_HIGH)
    while(1) begin
        fork
            fork : threads_detect
                while(1) begin
                    spi_get_start();
                    spi_get_stop();
                end
            join
            begin : threads_data
                //wait(start_event.triggered());
                @ start_event;
                spi_get_cmd(trans_scb);
                //$display("start event");
                spi_get_byte(trans_scb);
            end
            begin : threads_stop
                //wait(stop_event.triggered());
                @ stop_event;
                //$display("stop event");
                disable threads_detect;
                disable threads_data;
                disable threads_stop;
            end
        join
        break;
    end
    #10;
    //generate_packet(trans_scb);
    `uvm_info("MONITOR_FLASH","Collect a packet,packet information : ",UVM_HIGH)
    trans_scb.print;
    `uvm_info("MONITOR_FLASH","Collect a packet done.",UVM_HIGH)

endtask


task MONITOR_FLASH::spi_get_start();
    //`uvm_info("MONITOR_FLASH","Detect a start pulse start.",UVM_HIGH)
    @(negedge sys_vif.spi_if.cs_n);
    `uvm_info("MONITOR_FLASH","Detect a start pulse done.",UVM_HIGH)
    -> start_event;
endtask

task MONITOR_FLASH::spi_get_cmd(TRANSACTION_SCB trans_scb);
    logic [07:00] data;
    for(int i=7;i>=0;i--) begin
        @(posedge sys_vif.spi_if.sclk);
            data[i] = sys_vif.spi_if.io0_si_i;
    end
    trans_scb.cmd = data;
    `uvm_info("MONITOR_FLASH",$sformatf("Receive a cmd : [%0h].",data),UVM_HIGH)
endtask

task MONITOR_FLASH::spi_get_byte(TRANSACTION_SCB trans_scb);
    bit [07:00] data;
    case(trans_scb.cmd)
        `SPI_RDSR_LB,`SPI_RDSR_HB : 
            spi_get_byte_1_wire(trans_scb);
        default:;
    endcase
    `uvm_info("MONITOR_FLASH","Receive byte data done.",UVM_HIGH)
endtask

task MONITOR_FLASH::spi_get_byte_1_wire(TRANSACTION_SCB trans_scb);
    logic   [07:00]     byte_data   ;
    bit     [03:00]     i           ;
    i = 0;
    fork
        while(1) begin
            @(posedge sys_vif.spi_if.sclk);
            byte_data[i] = sys_vif.spi_if.io1_so_o;
            i++;
            `uvm_info("DRIVER_FLASH",$sformatf("spi receive bit = [%0h].",byte_data[i]),UVM_HIGH)
        end
        if(i == 'h8) begin
            i = 0;
            trans_scb.rdata_08_queue.push_back(byte_data);
            `uvm_info("DRIVER_FLASH",$sformatf("spi receive a one wire byte data = [%0h].",byte_data),UVM_HIGH)
        end
    join
endtask

task MONITOR_FLASH::spi_get_stop();
    //`uvm_info("MONITOR_FLASH","Detect a stop pulse start.",UVM_HIGH)
    @(posedge sys_vif.spi_if.cs_n);
    `uvm_info("MONITOR_FLASH","Detect a stop pulse done.",UVM_HIGH)
    -> stop_event;
endtask

/*
task MONITOR_FLASH::generate_packet(TRANSACTION_SCB trans_scb);
    bit [07:00] cmd;
    `uvm_info("MONITOR_FLASH","Generate a packet start.",UVM_MEDIUM)
    cmd = trans_scb.read_data_queue.pop_front();
    if(cmd == `NCMD_EE_READ) begin
        trans_scb.cmd = `NCMD_EE_READ;
        trans_scb.read_len = trans_scb.read_data_queue.size();
    end
    else if(cmd == `NCMD_EE_WRITE) begin
        trans_scb.cmd     = `NCMD_EE_WRITE;
        trans_scb.addr_16[15:08] = trans_scb.read_data_queue.pop_front();
        trans_scb.addr_16[07:00] = trans_scb.read_data_queue.pop_front();
        trans_scb.read_data_queue.delete();
    end
    else begin
        trans_scb.read_data_queue.delete();
    end
    #1ns;
    `uvm_info("MONITOR_FLASH","Generate a packet done.",UVM_MEDIUM)

endtask
*/

`endif

