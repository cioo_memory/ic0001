//****************************************************************************************************  
//---------------Copyright (c) 2019 CIOO@Gitee | wangboworks@126.com All rights reserved--------------
//**************************************************************************************************** 
//File Information
//**************************************************************************************************** 
//File Name      : REFERENCE_FLASH.v 
//Project Name   : ic0001
//Description    : the uvm flash reference model, which is derived from uvm_component.
//VC Address     : https://gitee.com/cioo_memory/ic0001
//License        : MuLanPSL-1.0
//**************************************************************************************************** 
//Version Information
//**************************************************************************************************** 
//Create Date    : 2019-10-12 14:10
//First Author   : FPGA1988
//Last Modify    : 2019-10-12 16:30
//Last Author    : FPGA1988
//**************************************************************************************************** 
//Change History(latest change first)
//yyyy.mm.dd - Author - Your log of change
//**************************************************************************************************** 
//2019.10.12 - FPGA1988 - Add the basic architecture and add build phase to create the uvm tree.
//----------------------------------------------------------------------------------------------------
//`timescale 1ns/1ps
`ifndef __REFERENCE_FLASH__
`define __REFERENCE_FLASH__
`include "include.svh"
class REFERENCE_FLASH extends uvm_component;
   
    //REG_MODEL_FLASH reg_mdl_flh;
    typedef struct {
        bit sus1    ;
        bit cmp     ;
        bit lb3     ;
        bit lb2     ;
        bit lb1     ;
        bit sus2    ;
        bit qe      ;
        bit srp1    ;
        bit srp0    ;
        bit bp4     ;
        bit bp3     ;
        bit bp2     ;
        bit bp1     ;
        bit bp0     ;
        bit wel     ;
        bit wip     ;
    } status_register_s;

    bit vsr_wel ;

    status_register_s sr;

    bit [07:00] sram_mem_all   [bit[`SRAM_ADDR_WIDTH-1:0]],sram_addr_idx;//SRAM assoc all array
    bit [07:00] flash_mem_main [bit[`FLASH_ADDR_MAIN_WIDTH-1:0]],flh_addr_main_idx;//FLASH assoc main array
    bit [07:00] flash_mem_otp  [bit[`FLASH_ADDR_OTP_WIDTH-1:0]],flh_addr_otp_idx;//FLASH assoc otp array

    //1.for analysisi fifo between drv
    uvm_blocking_get_port #(TRANSACTION_FLASH) port;
    //2.transaction transport use ap between scb
    uvm_analysis_port #(TRANSACTION_SCB) ap;
    virtual sys_if sys_vif;

    extern function new(string name,uvm_component parent);
    extern function void build_phase(uvm_phase phase);
    extern virtual task main_phase(uvm_phase phase);

    extern task sr_write(TRANSACTION_FLASH trans_flh);
    extern task sr_read(TRANSACTION_FLASH trans_flh,TRANSACTION_SCB trans_scb);
    extern task sram_write(TRANSACTION_FLASH trans_flh);
    extern task sram_read(TRANSACTION_FLASH trans_flh,TRANSACTION_SCB trans_scb);
    /*
    extern task write_byte_main_array(TRANSACTION_FLASH trans_flh);
    extern task read_byte_main_array(TRANSACTION_FLASH trans_flh);
    */
    `uvm_component_utils(REFERENCE_FLASH)

endclass

function REFERENCE_FLASH::new(string name,uvm_component parent);
    super.new(name,parent);
endfunction

function void REFERENCE_FLASH::build_phase(uvm_phase phase);
    super.build_phase(phase);

    if(!uvm_config_db#(virtual sys_if)::get(this,"","transfer_obj_01",sys_vif))
        `uvm_fatal("DRIVER_FLASH","virtual interface must be set for sys_vif.");
    //1.connect interface new
    port = new("port",this);
    ap   = new("ap",this); 

    set_report_verbosity_level(UVM_HIGH);
    `uvm_info("REFERENCE_FLASH","build phase is start",UVM_HIGH)


endfunction

task REFERENCE_FLASH::main_phase(uvm_phase phase);
    TRANSACTION_FLASH trans_flh;
    TRANSACTION_SCB trans_scb;
    trans_scb = new;
    super.main_phase(phase);
    while(1) begin
        port.get(trans_flh);
        //only the variable which be register by uvm_field in the transaction can use the copy/print/compare directly.
        `uvm_info("REFERENCE_FLASH","get a transaction.",UVM_LOW)
        trans_flh.print();
        case(trans_flh.cmd)
            `SPI_WREN,`SPI_WRDI,`SPI_WRSR   : sr_write(trans_flh);
            `SPI_RDSR_LB,`SPI_RDSR_HB       : sr_read(trans_flh,trans_scb);
            `TM_SRAM_WRITE                  : sram_write(trans_flh);
            `TM_SRAM_READ                   : sram_read(trans_flh,trans_scb);
            //`NCMD_EE_READ  : read_byte_main_array(trans_flh);
            default:;
        endcase
        
        //any cmd will clera the vsr_wel but wren_vsr
        if(trans_flh.cmd == `SPI_WREN_VSR)
            vsr_wel = 1'b1;
        else
            vsr_wel = 1'b0;
        //if(ee_main_array.first(addr_index)) begin
        //    do
        //        `uvm_info("REFERENCE_FLASH",$sformatf("eeprom main array : addr[%0h] = ee_main_array[%h].",addr_index,ee_main_array[addr_index]),UVM_LOW)
        //    while(ee_main_array.next(addr_index));
        //end
    end
endtask

task REFERENCE_FLASH::sr_write(TRANSACTION_FLASH trans_flh);
    bit otp_pgm_en;
    case(trans_flh.cmd)
        `SPI_WREN : begin
            if(trans_flh.wdata_08_queue.size() == 0)
                sr.wel = 1'b1;
        end
        `SPI_WRDI : begin
            if((trans_flh.wdata_08_queue.size() == 0) && (trans_flh.bit_num == 0))
                sr.wel = 1'b0;
        end
        `SPI_WRSR : begin
            if(vsr_wel | sr.wel) begin
                if((trans_flh.wdata_08_queue.size() == 1) && (trans_flh.bit_num == 0)) begin
                    `uvm_info("REFERENCE_FLASH",$sformatf("status register b7-b0 udpate : [%0h] ----> [%0h].",
                                {sr.srp0,sr.bp4,sr.bp3,sr.bp2,sr.bp1,sr.bp0,sr.wel,sr.wip},
                                {trans_flh.wdata_08_queue[0][7:2],1'b0,1'b0}),UVM_HIGH)
                    sr.srp0 = trans_flh.wdata_08_queue[0][7];  //07  
                    sr.bp4  = trans_flh.wdata_08_queue[0][6];  //06
                    sr.bp3  = trans_flh.wdata_08_queue[0][5];  //05
                    sr.bp2  = trans_flh.wdata_08_queue[0][4];  //04
                    sr.bp1  = trans_flh.wdata_08_queue[0][3];  //03
                    sr.bp0  = trans_flh.wdata_08_queue[0][2];  //02
                    //sr.wel  = trans_flh.wdata_08_queue[0][1];  //01
                    //sr.wip  = trans_flh.wdata_08_queue[0][0];  //00
                    if(sr.wel)
                        flash_mem_otp[`FLASH_SR1_ADDR] = trans_flh.wdata_08_queue[0];
                    otp_pgm_en = 1'b1;
                end
                else if((trans_flh.wdata_08_queue.size() == 2) && (trans_flh.bit_num == 0)) begin
                    `uvm_info("REFERENCE_FLASH",$sformatf("status register b7-b0 udpate : [%0h] ----> [%0h].",
                                {sr.sus1,sr.cmp,sr.lb3,sr.lb2,sr.lb1,sr.sus2,sr.qe,sr.srp1},
                                {sr.sus1,trans_flh.wdata_08_queue[1][6:3],sr.sus2,trans_flh.wdata_08_queue[1][1:0]}),UVM_HIGH)
                    //sr.sus1 = trans_flh.wdata_08_queue[1][7];  //15
                    sr.cmp  = trans_flh.wdata_08_queue[1][6];  //14  
                    sr.lb3  = trans_flh.wdata_08_queue[1][5];  //13
                    sr.lb2  = trans_flh.wdata_08_queue[1][4];  //12
                    sr.lb1  = trans_flh.wdata_08_queue[1][3];  //11
                    //sr.sus2 = trans_flh.wdata_08_queue[1][2];  //10
                    sr.qe   = trans_flh.wdata_08_queue[1][1];  //09
                    sr.srp1 = trans_flh.wdata_08_queue[1][0];  //08
                    if(sr.wel)
                        flash_mem_otp[`FLASH_SR2_ADDR] = trans_flh.wdata_08_queue[1];
                    otp_pgm_en = 1'b1;
                end
                else
                    otp_pgm_en = 1'b0;
            end
            if(otp_pgm_en & sr.wel) begin
                //wait the posedge of cs
                @(posedge sys_vif.spi_if.cs_n);
                fork
                    sr.wip  = 1'b1;  //00
                    #trans_flh.tW;
                    sr.wip  = 1'b0;  //00
                    sr.wel  = 1'b0;  //00
                join_none
            end
        end
        default:;
    endcase
            
    `uvm_info("REFERENCE_FLASH",$sformatf("REFERENCE_FLASH task [sr_write] start : cmd = [%0h].",trans_flh.cmd),UVM_HIGH)
     
endtask
task REFERENCE_FLASH::sr_read(TRANSACTION_FLASH trans_flh,TRANSACTION_SCB trans_scb);
    bit otp_pgm_en;
    case(trans_flh.cmd)
        `SPI_RDSR_LB : begin
            repeat(trans_flh.rd_len)
                trans_scb.rdata_08_queue.push_back({sr.srp0,
                        sr.bp4,
                        sr.bp3,
                        sr.bp2,
                        sr.bp1,
                        sr.bp0,
                        sr.wel,
                        sr.wip});
        end
        `SPI_RDSR_HB : begin
            repeat(trans_flh.rd_len)
                trans_scb.rdata_08_queue.push_back({sr.sus1,
                        sr.cmp,
                        sr.lb3,
                        sr.lb2,
                        sr.lb1,
                        sr.sus2,
                        sr.qe,
                        sr.srp1});
        end
        default:;
    endcase
    trans_scb.cmd = trans_flh.cmd;
    ap.write(trans_scb);
            
    `uvm_info("REFERENCE_FLASH",$sformatf("REFERENCE_FLASH task [sr_read] start : cmd = [%0h].",trans_flh.cmd),UVM_HIGH)
     
endtask


task REFERENCE_FLASH::sram_write(TRANSACTION_FLASH trans_flh);
    int sram_write_len;
    sram_addr_idx = trans_flh.addr_08_queue[$];
    sram_write_len = trans_flh.wdata_08_queue.size();
    $display("sram_write_len = %d.",sram_write_len);
    repeat(sram_write_len) begin
        sram_mem_all[sram_addr_idx] = trans_flh.wdata_08_queue.pop_front();
        `uvm_info("REFERENCE_FLASH",$sformatf("sram write : addr[%0h] = [%0h].",sram_addr_idx,sram_mem_all[sram_addr_idx]),UVM_HIGH)
        sram_addr_idx++;
    end
    `uvm_info("REFERENCE_FLASH",$sformatf("Task [sram_write] start : cmd = [%0h].",trans_flh.cmd),UVM_HIGH)
     
endtask
task REFERENCE_FLASH::sram_read(TRANSACTION_FLASH trans_flh,TRANSACTION_SCB trans_scb);
    sram_addr_idx = trans_flh.addr_08_queue[$];
    trans_scb.cmd = trans_flh.cmd;
    repeat(trans_flh.rd_len) begin
        trans_scb.rdata_08_queue.push_back(sram_mem_all[sram_addr_idx]);
        `uvm_info("REFERENCE_FLASH",$sformatf("sram read : addr[%0h] = [%0h].",sram_addr_idx,sram_mem_all[sram_addr_idx]),UVM_HIGH)
        sram_addr_idx++;
    end
    ap.write(trans_scb);
    `uvm_info("REFERENCE_FLASH",$sformatf("REFERENCE_FLASH task [sram_read] start : cmd = [%0h].",trans_flh.cmd),UVM_HIGH)
     
endtask
/*
task REFERENCE_FLASH::write_byte_main_array(TRANSACTION_FLASH trans_flh);
    bit [15:00] write_len;
    bit [07:00] data_temp;
    bit [07:00] data_queue_temp[$];
    bit [`EE_ADDR_WIDTH-1:0]    addr_temp;
    write_len = trans_flh.wdata_08_queue.size();
    byte_addr = trans_flh.addr_16[(`EE_PAGE_WIDTH)-1:0];
    page_addr = trans_flh.addr_16[`EE_PAGE_WIDTH];
    line_addr = trans_flh.addr_16[(`EE_ADDR_WIDTH)-1:(`EE_PAGE_WIDTH)+1];
    $display("width info : %0d,%0d.",`EE_ADDR_WIDTH,`EE_PAGE_WIDTH);
    `uvm_info("REFERENCE_FLASH",$sformatf("REFERENCE_FLASH task [write_byte_main_array] start : addr = [%0h],data len = [%0h].",trans_flh.addr_16,write_len),UVM_LOW)
    addr_temp = trans_flh.addr_16;
    repeat(write_len) begin
        data_temp = trans_flh.wdata_08_queue.pop_front();
        ee_main_array[addr_temp] = data_temp;
        `uvm_info("REFERENCE_FLASH",$sformatf("FLASH Data Update : addr[%0h] --> write_data[%0h] |.",addr_temp,data_temp),UVM_HIGH)
        addr_temp[`EE_PAGE_WIDTH-1:0]++;
    end
     
endtask

task REFERENCE_FLASH::read_byte_main_array(TRANSACTION_FLASH trans_flh);
    bit [`EE_ADDR_WIDTH-1:00] addr_temp;
    bit [07:00] data_temp;
    bit [07:00] data_queue_temp[$];
    `uvm_info("REFERENCE_FLASH",$sformatf("REFERENCE_FLASH task [read_byte_main_array] start : addr = [%0h],data len = [%0h].",trans_flh.addr_16,trans_flh.read_len),UVM_LOW)
    addr_temp = trans_flh.addr_16;
    repeat(trans_flh.read_len) begin
        data_temp = ee_main_array[addr_temp];
        trans_flh.read_data_queue.push_back(data_temp);
        `uvm_info("REFERENCE_FLASH",$sformatf("FLASH read data : addr[%0h] <---- read_data[%h] |.",addr_temp,data_temp),UVM_HIGH)
        addr_temp++;
    end
    trans_flh.print();
    ap.write(trans_flh);
     
endtask
*/

`endif
