//****************************************************************************************************  
//---------------Copyright (c) 2019 CIOO@Gitee | wangboworks@126.com All rights reserved--------------
//**************************************************************************************************** 
//File Information
//**************************************************************************************************** 
//File Name      : REG_ADAPTER.sv 
//Project Name   : ic0001
//Description    : the adapter of reg&mem modle : bus2reg and reg2bus.
//VC Address     : https://gitee.com/cioo_memory/ic0001
//License        : MuLanPSL-1.0
//**************************************************************************************************** 
//Version Information
//**************************************************************************************************** 
//Create Date    : 2019-11-06 14:10
//First Author   : FPGA1988
//Last Modify    : 2019-11-06 16:30
//Last Author    : FPGA1988
//**************************************************************************************************** 
//Change History(latest change first)
//yyyy.mm.dd - Author - Your log of change
//**************************************************************************************************** 
//2019.11.06 - FPGA1988 - Add the basic reg info and add build function.
//----------------------------------------------------------------------------------------------------
//`timescale 1ns/1ps
`ifndef __MEM_ADAPTER_FLASH__
`define __MEM_ADAPTER_FLASH__
class flash_adapter extends uvm_reg_adapter;
    //1. register define

    //2.function 
    function new(string name = "flash_adapter");
        //super.new(name,16,UVM_CVR_ALL);
        super.new(name,16,UVM_NO_COVERAGE);
    endfunction



    function uvm_sequence_item reg2bus(const ref uvm_reg_bus_op rw);
        flash_bus_trans fb_trans = flash_bus_trans::type_id::create("fb_trans");
        fb_trans.cmd = (rw.kind == UVM_WRITE) ? `WRITE : `READ;
        fb_trans.addr = rw.addr;
        fb_trans.wdata = rw.data;
        return fb_trans;
    endfunction
    
    //digital to reg modle 
    function uvm_sequence_item bus2reg(const ref uvm_reg_bus_op rw);
        flash_bus_trans fb_trans;
        if(!$cast(t,bus_item)) begin
            `uvm_fatal("NOT_FLASH_BUS_TYPE","Provided bus_item is not of the correct type!");
            return;
        end
        //1.

        
        rw.kind = (fb_trans.cmd == `WRITE) ? UVM_WRITE : UVM_READ;
        rw.addr = fb_trans.addr;
        rw.data = (fb_trans.cmd == `WRITE) ? fb_trans.wdata : fb_trans.rdata;
        rw.status = UVM_IS_OK;

    endfunction


    `uvm_object_utils(flash_adapter)
endclass

`endif
