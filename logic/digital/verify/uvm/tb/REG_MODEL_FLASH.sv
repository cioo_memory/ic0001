//****************************************************************************************************  
//---------------Copyright (c) 2019 CIOO@Gitee | wangboworks@126.com All rights reserved--------------
//**************************************************************************************************** 
//File Information
//**************************************************************************************************** 
//File Name      : REG_MODEL_FLASH.sv 
//Project Name   : ic0001
//Description    : the uvm flash memory and register model.
//VC Address     : https://gitee.com/cioo_memory/ic0001
//License        : MuLanPSL-1.0
//**************************************************************************************************** 
//Version Information
//**************************************************************************************************** 
//Create Date    : 2019-11-06 14:10
//First Author   : FPGA1988
//Last Modify    : 2019-11-06 16:30
//Last Author    : FPGA1988
//**************************************************************************************************** 
//Change History(latest change first)
//yyyy.mm.dd - Author - Your log of change
//**************************************************************************************************** 
//2019.11.06 - FPGA1988 - Add the basic reg info and add build function.
//----------------------------------------------------------------------------------------------------
//`timescale 1ns/1ps
`ifndef __REG_MODEL_FLASH__
`define __REG_MODEL_FLASH__
class status_reg extends uvm_reg;
    //1. register define
    rand uvm_reg_field sus1 ;
    rand uvm_reg_field cmp  ; 
    rand uvm_reg_field lb3  ;
    rand uvm_reg_field lb2  ;
    rand uvm_reg_field lb1  ;
    rand uvm_reg_field sus2 ;
    rand uvm_reg_field qe   ;
    rand uvm_reg_field srp1 ;

    rand uvm_reg_field srp0 ;
    rand uvm_reg_field bp4  ;
    rand uvm_reg_field bp3  ;
    rand uvm_reg_field bp2  ;
    rand uvm_reg_field bp1  ;
    rand uvm_reg_field bp0  ;
    rand uvm_reg_field wel  ;
    rand uvm_reg_field wip  ;

    //2.function 
    function new(string name = "status_reg");
        //super.new(name,16,UVM_CVR_ALL);
        super.new(name,16,UVM_NO_COVERAGE);
    endfunction

    virtual function void build();
        //1.create
        sus1 = uvm_reg_field::type_id::create("sus1");
        cmp  = uvm_reg_field::type_id::create("cmp");
        lb3  = uvm_reg_field::type_id::create("lb3");
        lb2  = uvm_reg_field::type_id::create("lb2");
        lb1  = uvm_reg_field::type_id::create("lb1");
        sus2 = uvm_reg_field::type_id::create("sus2");
        qe   = uvm_reg_field::type_id::create("qe");
        srp1 = uvm_reg_field::type_id::create("srp1");
        srp0 = uvm_reg_field::type_id::create("srp0");
        bp4  = uvm_reg_field::type_id::create("bp4");
        bp3  = uvm_reg_field::type_id::create("bp3");
        bp2  = uvm_reg_field::type_id::create("bp2");
        bp1  = uvm_reg_field::type_id::create("bp1");
        bp0  = uvm_reg_field::type_id::create("bp0");
        wel  = uvm_reg_field::type_id::create("wel");
        wip  = uvm_reg_field::type_id::create("wip");

        //2.config
        sus1.configure(this,1,15,"RW",1,0,1,1,0);
        cmp.configure (this,1,14,"RW",1,0,1,1,0);
        lb3.configure (this,1,13,"RW",1,0,1,1,0);
        lb2.configure (this,1,12,"RW",1,0,1,1,0);
        lb1.configure (this,1,11,"RW",1,0,1,1,0);
        sus2.configure(this,1,10,"RW",1,0,1,1,0);
        qe.configure  (this,1,9,"RW",1,0,1,1,0);
        srp1.configure(this,1,8,"RW",1,0,1,1,0);
        srp0.configure(this,1,7,"RW",1,0,1,1,0);
        bp4.configure (this,1,6,"RW",1,0,1,1,0);
        bp3.configure (this,1,5,"RW",1,0,1,1,0);
        bp2.configure (this,1,4,"RW",1,0,1,1,0);
        bp1.configure (this,1,3,"RW",1,0,1,1,0);
        bp0.configure (this,1,2,"RW",1,0,1,1,0);
        wel.configure (this,1,1,"RW",1,0,1,1,0);
        wip.configure (this,1,0,"RW",1,0,1,1,0);

        `uvm_info("REG_MODEL_FLASH","status register has been built.",UVM_NONE)
    endfunction

    `uvm_object_utils(status_reg)
endclass
//memory
class mem_512k_8b extends uvm_mem;
    function new(string name="mem_512k_8b");
        super.new(name,524288,8,"RW",UVM_NO_COVERAGE);
    endfunction
    `uvm_object_utils(mem_512k_8b)
endclass : mem_512k_8b
/*
class mem_flash_otp extends uvm_mem;
    function new(string name="mem_flash_otp");
        super.new(name,512*1024,8);
    endfunction
    `uvm_object_utils(mem_flash_otp)
endclass
*/
class mem_256_8b extends uvm_mem;
    function new(string name="mem_256_8b");
        super.new(name,256,8,"RW",UVM_NO_COVERAGE);
    endfunction
    `uvm_object_utils(mem_256_8b)
endclass : mem_256_8b

class REG_MODEL_FLASH extends uvm_reg_block;
    
    //1. register define
    rand status_reg  flash_reg_status;
    rand mem_512k_8b flash_mem_main;
    rand mem_256_8b  sram_mem_all;

    function new(string name = "REG_MODEL_FLASH",uvm_component parent);
        //super.new(name,build_coverage(UVM_NO_COVERAGE));
        super.new(name,UVM_NO_COVERAGE);
        `uvm_info("DRIVER_FLASH",$sformatf("The %s has been created",name),UVM_LOW);
    endfunction

    virtual function void build();
        default_map = create_map("default_map",0,1,UVM_BIG_ENDIAN,0);
        //default_map = create_map();

        //access sr by backdoor
        flash_reg_status = status_reg::type_id::create("flash_reg_status");
        flash_reg_status.configure(this,null,"flash_reg_status");
        flash_reg_status.build();
        flash_reg_status.add_hdl_path('{'{"flash_reg_status",-1,-1}});
        $display("~~~~~~~~");
        $display(flash_reg_status.has_hdl_path());
        $display("~~~~~~~~");
        //| r1.add_hdl_path('{ '{"r1", -1, -1} });

        //access memory by frontdoor
        flash_mem_main = mem_512k_8b::type_id::create("flash_mem_main");
        flash_mem_main.configure(this,"");
        //front door address
        //default_map.add_mem(flash_mem_main,'h0,"RW");
        //back door
        //flash_mem_main.add_hdl_path();

        sram_mem_all = mem_256_8b::type_id::create("sram_mem_all");
        sram_mem_all.configure(this,"");
        //front door address
        //default_map.add_mem(sram_mem_all,'h0,"RW");
        //back door
        //sram_mem_all.add_hdl_path();

    endfunction


    `uvm_component_utils(REG_MODEL_FLASH)

endclass

class status_reg_backdoor extends uvm_reg_backdoor;

    virtual task read(uvm_reg_item rw);
        do_pre_read(rw);
        rw.value[0] = `REG_TOP_PATH.flash_reg_status;
        rw.status = UVM_IS_OK;

    endtask

    //virtual task write



endclass

`endif
