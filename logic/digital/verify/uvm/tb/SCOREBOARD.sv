//****************************************************************************************************  
//---------------Copyright (c) 2019 CIOO@Gitee | wangboworks@126.com All rights reserved--------------
//**************************************************************************************************** 
//File Information
//**************************************************************************************************** 
//File Name      : SCOREBOARD.v 
//Project Name   : ic0001
//Description    : the uvm scoreboard, which is derived from uvm_scoreboard.
//VC Address     : https://gitee.com/cioo_memory/ic0001
//License        : MuLanPSL-1.0
//**************************************************************************************************** 
//Version Information
//**************************************************************************************************** 
//Create Date    : 2019-10-12 14:10
//First Author   : FPGA1988
//Last Modify    : 2019-11-29 16:30
//Last Author    : FPGA1988
//**************************************************************************************************** 
//Change History(latest change first)
//yyyy.mm.dd - Author - Your log of change
//**************************************************************************************************** 
//2019.11.29 - FPGA1988 - Add final phase.
//2019.11.29 - FPGA1988 - Add the pass and fail char to print.
//2019.10.12 - FPGA1988 - Add the basic architecture and add build phase to create the uvm tree.
//----------------------------------------------------------------------------------------------------
`ifndef __SCOREBOARD__
`define __SCOREBOARD__
class SCOREBOARD extends uvm_scoreboard;
    TRANSACTION_SCB expect_queue[$];
    uvm_blocking_get_port #(TRANSACTION_SCB) ref_port;  //expect
    uvm_blocking_get_port #(TRANSACTION_SCB) mon_port;  //actual


    bit result;
    string pass_char,fail_char;



    extern function new(string name,uvm_component parent = null);
    extern virtual function void build_phase(uvm_phase phase);
    extern virtual task main_phase(uvm_phase phase);
    extern virtual function void report_phase(uvm_phase phase);
    `uvm_component_utils(SCOREBOARD)

endclass

function SCOREBOARD::new(string name,uvm_component parent = null);
    super.new(name,parent);
    `uvm_info("SCOREBOARD",$sformatf("The %s has been created",name),UVM_HIGH);

endfunction

function void SCOREBOARD::build_phase(uvm_phase phase);
    super.build_phase(phase);

    //1.connect interface new
    ref_port = new("ref_port",this);
    mon_port = new("mon_port",this);
    set_report_verbosity_level(UVM_HIGH);
    pass_char = "                                                                                   \n\
                                                                                                    \n\
//************************************************************************************************//\n\
//                   _____   _       ___  ___      _____      ___   _____   _____                 //\n\
//                  /  ___/ | |     /   |/   |    |  _  \    /   | /  ___/ /  ___/                //\n\
//                  | |___  | |    / /|   /| |    | |_| |   / /| | | |___  | |___                 //\n\
//                  |___  \ | |   / / |__/ | |    |  ___/  / /_| | |___  | |___  |                //\n\
//                   ___| | | |  / /       | |    | |     / /  | |  ___| |  ___| |                //\n\
//                  /_____/ |_| /_/        |_|    |_|    /_/   |_| /_____/ /_____/                //\n\
//                                                                                                //\n\
//************************************************************************************************//\n\
                                                                                                    \n";

    fail_char = "                                                                                   \n\
                                                                                                    \n\
//************************************************************************************************//\n\
//                   _____   _       ___  ___      _____      ___   _   _                         //\n\
//                  /  ___/ | |     /   |/   |    |  ___|    /   | | | | |                        //\n\
//                  | |___  | |    / /|   /| |    | |__     / /| | | | | |                        //\n\
//                  |___  | | |   / / |__/ | |    |  __|   / /_| | | | | |                        //\n\
//                   ___| | | |  / /       | |    | |     / /  | | | | | |___                     //\n\
//                  /_____/ |_| /_/        |_|    |_|    /_/   |_| |_| |_____|                    //\n\
//                                                                                                //\n\
//************************************************************************************************//\n\
                                                                                                    \n";
    `uvm_info("SCOREBOARD","build phase is start",UVM_HIGH);

endfunction


task SCOREBOARD::main_phase(uvm_phase phase);
    TRANSACTION_SCB get_expect;
    TRANSACTION_SCB get_actual;
    TRANSACTION_SCB tmp_tran;

    //get a expect transaction from exp_port and push it back to queue.
    while(1) begin
        ref_port.get(get_expect);
        mon_port.get(get_actual);
        `uvm_info("SCOREBOARD","expect transaction context : ",UVM_HIGH);
        get_expect.print();
        `uvm_info("SCOREBOARD","actual transaction context : ",UVM_HIGH);
        get_actual.print();
        result = get_actual.compare(get_expect);
        if(result) begin
            `uvm_info("SCOREBOARD","Compare successfully.",UVM_LOW);
        end
        else begin
            `uvm_error("SCOREBOARD",$sformatf("%s",fail_char));
        end
    end

endtask

function void SCOREBOARD::report_phase(uvm_phase phase);
    super.report_phase(phase);
    if(result) begin
        `uvm_error("SCOREBOARD",$sformatf("%s",pass_char));
    end
    else begin
        `uvm_info("SCOREBOARD","\n\nError : Get nothing from dut!!!\n",UVM_LOW);
        `uvm_error("SCOREBOARD",$sformatf("%s",fail_char));
    end

endfunction
`endif
