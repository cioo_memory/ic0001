//****************************************************************************************************  
//---------------Copyright (c) 2019 CIOO@Gitee | wangboworks@126.com All rights reserved--------------
//**************************************************************************************************** 
//File Information
//**************************************************************************************************** 
//File Name      : SEQUENCER_FLASH.v 
//Project Name   : ic0001
//Description    : the uvm sequencer, which is derived from uvm_sequencer.
//VC Address     : https://gitee.com/cioo_memory/ic0001
//License        : MuLanPSL-1.0
//**************************************************************************************************** 
//Version Information
//**************************************************************************************************** 
//Create Date    : 2019-10-12 14:10
//First Author   : FPGA1988
//Last Modify    : 2019-10-12 15:00
//Last Author    : FPGA1988
//**************************************************************************************************** 
//Change History(latest change first)
//yyyy.mm.dd - Author - Your log of change
//**************************************************************************************************** 
//2019.10.12 - FPGA1988 - Add the basic architecture and add build phase to create the uvm tree.
//----------------------------------------------------------------------------------------------------
//`timescale 1ns/1ps
`ifndef __SEQUENCER_FLASH__
    `define __SEQUENCER_FLASH__
// ************************************************************************************
// 1.Class define
// ************************************************************************************

class SEQUENCER_FLASH extends uvm_sequencer #(TRANSACTION_FLASH);
    
    function new(string name,uvm_component parent);
        super.new(name,parent);
        `uvm_info("SEQUENCER_FLASH",$sformatf("The %s has been created",name),UVM_HIGH);
    endfunction

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        `uvm_info("SEQUENCER_FLASH","build phase is start",UVM_HIGH);
    endfunction

    `uvm_component_utils(SEQUENCER_FLASH)

endclass

`endif

