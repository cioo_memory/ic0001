//****************************************************************************************************  
//---------------Copyright (c) 2019 CIOO@Gitee | wangboworks@126.com All rights reserved--------------
//**************************************************************************************************** 
//File Information
//**************************************************************************************************** 
//File Name      : TRANSACTION_FLASH.v 
//Project Name   : ic0001
//Description    : the flash transaction, which is derived from uvm_sequence_item.
//VC Address     : https://gitee.com/cioo_memory/ic0001
//License        : MuLanPSL-1.0
//**************************************************************************************************** 
//Version Information
//**************************************************************************************************** 
//Create Date    : 2019-10-12 14:10
//First Author   : FPGA1988
//Last Modify    : 2019-10-12 16:30
//Last Author    : FPGA1988
//**************************************************************************************************** 
//Change History(latest change first)
//yyyy.mm.dd - Author - Your log of change
//**************************************************************************************************** 
//2019.10.12 - FPGA1988 - Add some variable in the transaction.
//----------------------------------------------------------------------------------------------------
//`timescale 1ns/1ps
`ifndef __TRANSACTION_FLASH__
`define __TRANSACTION_FLASH__
class TRANSACTION_FLASH extends uvm_sequence_item;

    rand    bit     [07:00]         cmd                 ;
    rand    bit     [07:00]         addr_08_queue[$]    ;
    rand    bit     [15:00]         addr_16             ;
    rand    bit     [23:00]         addr_24             ;
    rand    byte                    wdata_08            ;
    rand    byte                    rdata_08            ;
    rand    bit     [07:00]         wdata_08_queue[$]   ;
    rand    logic   [07:00]         rdata_08_queue[$]   ;
    rand    bit     [15:00]         rd_len              ;//shortint default is signed 16bit

    rand    bit                     chip_erase_type     ;
    typedef enum    {NO,YES}        bool_enum           ;
    rand    bool_enum               bool                ;

    rand    bit                     spi_mode            ;
    rand    bit     [02:00]         bit_num             ;


    //timing
    rand    int                     tW                  ;



    `uvm_object_utils_begin(TRANSACTION_FLASH)
        `uvm_field_int(cmd,UVM_ALL_ON)
        `uvm_field_enum(bool_enum,bool,UVM_ALL_ON)
        `uvm_field_int(addr_16,UVM_ALL_ON)
        `uvm_field_int(addr_24,UVM_ALL_ON)
        `uvm_field_int(wdata_08,UVM_ALL_ON)
        `uvm_field_int(rdata_08,UVM_ALL_ON)
        `uvm_field_int(rd_len,UVM_ALL_ON)
        `uvm_field_int(spi_mode,UVM_ALL_ON)
        `uvm_field_int(bit_num,UVM_ALL_ON)
        //queue field automation
        `uvm_field_queue_int(addr_08_queue,UVM_ALL_ON)
        `uvm_field_queue_int(wdata_08_queue,UVM_ALL_ON)
        `uvm_field_queue_int(rdata_08_queue,UVM_ALL_ON)

    `uvm_object_utils_end

    function void pre_randomize();
    endfunction

    constraint addr_con{
        addr_08_queue.size() == 3;
    }

    constraint data_con{
        wdata_08 >= 0;
    }

    constraint tW_con{
        tW inside {[1_000_000:20_000_000]};
    }
    
    constraint data_len_con{
        wdata_08_queue.size() inside {[0:512]};
        //wdata len in the normal mode
        cmd == `SPI_WREN            -> wdata_08_queue.size() == 0;
        cmd == `SPI_WRDI            -> wdata_08_queue.size() == 0;
        cmd == `SPI_WREN_VSR        -> wdata_08_queue.size() == 0;
        cmd == `SPI_CHIP_ERASE_0    -> wdata_08_queue.size() == 0;
        cmd == `SPI_CHIP_ERASE_1    -> wdata_08_queue.size() == 0;
        cmd == `SPI_ENABLE_RESET    -> wdata_08_queue.size() == 0;
        cmd == `SPI_RESET           -> wdata_08_queue.size() == 0;
        cmd == `SPI_PES             -> wdata_08_queue.size() == 0;
        cmd == `SPI_PER             -> wdata_08_queue.size() == 0;
        cmd == `SPI_ESRY            -> wdata_08_queue.size() == 0;
        cmd == `SPI_DSRY            -> wdata_08_queue.size() == 0;
        cmd == `SPI_DP              -> wdata_08_queue.size() == 0;
        cmd == `SPI_WRSR            -> wdata_08_queue.size() inside {[1:2]};
        //wdata len in the test mode
        cmd == `TM_SRAM_WRITE       -> wdata_08_queue.size() inside {[0:512]};
        //rdata len
        cmd == `SPI_RDSR_LB -> rd_len dist {1:/60,[2:8]:/20,[9:32]:/20};
        cmd == `SPI_RDSR_HB -> rd_len dist {1:/60,[2:8]:/20,[9:32]:/20};
        //bit_num dist {0:/70,[1:7]:/30};
        bit_num == 0;
    }

    constraint spi_mode_con{
        spi_mode == 0;    
    }

    function new(string name="FLASH_TRANS");
	    super.new(name);
    endfunction


endclass
`endif
