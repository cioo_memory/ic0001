//****************************************************************************************************  
//---------------Copyright (c) 2019 CIOO@Gitee | wangboworks@126.com All rights reserved--------------
//**************************************************************************************************** 
//File Information
//**************************************************************************************************** 
//File Name      : TRANSACTION_SCB.v 
//Project Name   : ic0001
//Description    : the transaction for compare.
//VC Address     : https://gitee.com/cioo_memory/ic0001
//License        : MuLanPSL-1.0
//**************************************************************************************************** 
//Version Information
//**************************************************************************************************** 
//Create Date    : 2019-11-29 14:10
//First Author   : FPGA1988
//Last Modify    : 2019-11-29 16:30
//Last Author    : FPGA1988
//**************************************************************************************************** 
//Change History(latest change first)
//yyyy.mm.dd - Author - Your log of change
//**************************************************************************************************** 
//2019.11.29 - FPGA1988 - Add some variable in the transaction.
//----------------------------------------------------------------------------------------------------
//`timescale 1ns/1ps
`ifndef __TRANSACTION_SCB__
`define __TRANSACTION_SCB__
class TRANSACTION_SCB extends uvm_sequence_item;

    rand    bit     [07:00]         cmd                 ;
    rand    logic   [07:00]         rdata_08_queue[$]   ;
    rand    bit     [02:00]         bit_num             ;

    `uvm_object_utils_begin(TRANSACTION_SCB)
        `uvm_field_int(cmd,UVM_ALL_ON)
        `uvm_field_int(bit_num,UVM_ALL_ON)
        `uvm_field_queue_int(rdata_08_queue,UVM_ALL_ON)

    `uvm_object_utils_end

    function void pre_randomize();
    endfunction

    constraint data_len_con{
        bit_num == 0;
    }

    function new(string name="SCB_TRANS");
	    super.new(name);
    endfunction


endclass
`endif
