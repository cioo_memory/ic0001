class case0_sequence extends uvm_sequence #(TRANSACTION_FLASH);
    TRANSACTION_FLASH trans;

    function new(string name = "case0_sequence");
        super.new(name);
    endfunction

    virtual task body();
        $display("task body run");
        if(starting_phase != null)
            starting_phase.raise_objection(this);
        repeat(2) begin
            `uvm_do_with(trans,{trans.bool== NO;})
        end
        if(starting_phase != null)
            starting_phase.drop_objection(this);
    endtask

    `uvm_object_utils(case0_sequence)

endclass

//the root is test_top(base test)
class my_case0 extends BASE_TEST;

    function new(string name = "my_case0",uvm_component parent = null);
        super.new(name,parent);
        `uvm_info("SEQUENCE",$sformatf("The %s has been created",name),UVM_HIGH);
    endfunction

    extern virtual function void build_phase(uvm_phase phase);

    `uvm_component_utils(my_case0)

endclass

function void my_case0::build_phase(uvm_phase phase);
    super.build_phase(phase);

    `uvm_info("SEQUENCE","build phase is start",UVM_HIGH)
    //set the default sequence to current sequence_name
    uvm_config_db#(uvm_object_wrapper)::set(this,
        "env.agt_i.sqr.main_phase",
        "default_sequence",
        case0_sequence::type_id::get());

endfunction
