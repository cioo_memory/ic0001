class case_sequence extends uvm_sequence #(TRANSACTION_FLASH);
    TRANSACTION_FLASH trans;

    function new(string name = "case_sequence");
        super.new(name);
    endfunction

    virtual task body();
        $display("task body run");
        if(starting_phase != null)
            starting_phase.raise_objection(this);
        repeat(1) begin
            #10us;
            `uvm_do_with(trans,{trans.cmd == `SPI_WREN;trans.spi_mode == 0;trans.addr_08_queue[2] == 8'h40;})
            #10us;
            `uvm_do_with(trans,{trans.cmd == `SPI_WRSR;trans.spi_mode == 0;})
            #10ms;
            `uvm_do_with(trans,{trans.cmd == `SPI_RDSR_LB;trans.spi_mode == 0;trans.rd_len == 10;})
            #10us;
            `uvm_do_with(trans,{trans.cmd == `SPI_RDSR_HB;trans.spi_mode == 0;trans.rd_len == 10;})
        end
        if(starting_phase != null)
            starting_phase.drop_objection(this);
    endtask

    `uvm_object_utils(case_sequence)

endclass

//the root is test_top(base test)
class spi_sr_wr extends BASE_TEST;

    function new(string name = "spi_sr_wr",uvm_component parent = null);
        super.new(name,parent);
        `uvm_info("SEQUENCE",$sformatf("The %s has been created",name),UVM_HIGH);
    endfunction

    extern virtual function void build_phase(uvm_phase phase);

    `uvm_component_utils(spi_sr_wr)

endclass

function void spi_sr_wr::build_phase(uvm_phase phase);
    super.build_phase(phase);

    `uvm_info("SEQUENCE","build phase is start",UVM_HIGH)
    //set the default sequence to current sequence_name
    uvm_config_db#(uvm_object_wrapper)::set(this,
        "env.agt_flh_i.sqr.main_phase",
        "default_sequence",
        case_sequence::type_id::get());

endfunction
