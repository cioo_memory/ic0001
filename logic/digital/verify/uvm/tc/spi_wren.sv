class case_sequence extends uvm_sequence #(TRANSACTION_FLASH);
    TRANSACTION_FLASH trans;

    function new(string name = "case_sequence");
        super.new(name);
    endfunction

    virtual task body();
        $display("task body run");
        if(starting_phase != null)
            starting_phase.raise_objection(this);
        repeat(0) begin
            #10us;
            `uvm_do_with(trans,{trans.cmd == `SPI_WREN;trans.spi_mode == 0;})
            //reg_mdl_flh.flash_reg_status.poke(status,1);
            #10us;
            `uvm_do_with(trans,{trans.cmd == `SPI_WREN;trans.spi_mode == 0;});
        end
        if(starting_phase != null)
            starting_phase.drop_objection(this);
    endtask

    `uvm_object_utils(case_sequence)

endclass



class status_reg_cfg extends uvm_sequence;

   `uvm_object_utils(status_reg_cfg)
   `uvm_declare_p_sequencer(SEQUENCER_ALL)
   
   function  new(string name= "status_reg_cfg");
      super.new(name);
   endfunction 
   
   virtual task body();
      uvm_status_e   status;
      uvm_reg_data_t value;
      bit   [15:00]     status_reg_val;
      if(starting_phase != null) 
         starting_phase.raise_objection(this);

      p_sequencer.reg_mdl_flh.flash_reg_status.poke(status,'hff);
      p_sequencer.reg_mdl_flh.flash_reg_status.peek(status, value);
      status_reg_val = value[15:0];
      `uvm_info("status_reg_cfg", $sformatf("after poke,status register's value(BACKDOOR) is %0h",status_reg_val), UVM_LOW)
      if(starting_phase != null) 
         starting_phase.drop_objection(this);
   endtask

endclass

class case_vsequence extends uvm_sequence;

   `uvm_object_utils(case_vsequence)
   `uvm_declare_p_sequencer(SEQUENCER_ALL)
   
   function  new(string name= "case_vsequence");
      super.new(name);
   endfunction 
   
   virtual task body();
      case_sequence main_sequence;
      uvm_status_e   status;
      uvm_reg_data_t value;
      bit   [15:00]     status_reg_val;
      if(starting_phase != null) 
         starting_phase.raise_objection(this);
      #10000;
      main_sequence = case_sequence::type_id::create("main_sequence");
      main_sequence.start(p_sequencer.sqr_flh);

      p_sequencer.reg_mdl_flh.flash_reg_status.poke(status,'hff);
      p_sequencer.reg_mdl_flh.flash_reg_status.peek(status, value);
      status_reg_val = value[15:0];
      `uvm_info("status_reg_cfg", $sformatf("after poke,status register's value(BACKDOOR) is %0h",status_reg_val), UVM_LOW)
      
      if(starting_phase != null) 
         starting_phase.drop_objection(this);
   endtask

endclass

//the root is test_top(base test)
class spi_wren extends BASE_TEST;

    function new(string name = "spi_wren",uvm_component parent = null);
        super.new(name,parent);
        `uvm_info("SEQUENCE",$sformatf("The %s has been created",name),UVM_HIGH);
    endfunction

    extern virtual function void build_phase(uvm_phase phase);

    `uvm_component_utils(spi_wren)

endclass

function void spi_wren::build_phase(uvm_phase phase);
    super.build_phase(phase);

    `uvm_info("SEQUENCE","build phase is start",UVM_HIGH)
    //set the default sequence to current sequence_name
    
    //configure phase + cfg_sequence
    uvm_config_db#(uvm_object_wrapper)::set(this,
        "sqr_all.configure_phase",
        "default_sequence",
        status_reg_cfg::type_id::get());

    uvm_config_db#(uvm_object_wrapper)::set(this,
        //"env.agt_flh_i.sqr.main_phase",
        "sqr_all.main_phase",
        "default_sequence",
        case_vsequence::type_id::get());

endfunction

